using DataStructures
using BenchmarkTools
using StaticArrays
using Profile
using PProf
using Bumper

function parse_data()
    data = Vector{Vector{Int}}()
    open(joinpath(dirname(@__FILE__), "data/day09.txt")) do f
        while !eof(f)
            line = parse.(Int, collect(readline(f)))
            push!(data, line)
        end
    end
    line = data[1]
    half_size = length(line) ÷ 2
    files = SizedVector{half_size + 1,Int}(line[1:2:end])
    spaces = SizedVector{half_size,Int}(line[2:2:end])

    return files, spaces
end

function solve_problem(data)
    files, spaces = data
    # files = SizedVector{half_size + 1,Int}(line[1:2:end])
    # spaces = SizedVector{half_size,Int}(line[2:2:end])
    # spaces = line[2:2:end]
    @inbounds @no_escape begin
        fills = @alloc(Int, length(spaces))
        # fill!(fills, 0)
        # fills = zeros(Int, length(spaces))
        fills .= 0

        # files = view(line, 1:2:length(line))
        # spaces = view(line, 2:2:length(line))
        # fills = zeros(Int, length(spaces))

        score = 0
        for file_i in length(files):-1:1
            file = files[file_i]
            found = false

            for space_i in 1:(file_i - 1)
                space = spaces[space_i]
                fill = fills[space_i]
                if space - fill >= file
                    # if space - fill >= file
                    found = true
                    # println("found it at $space_i")
                    # The file fits in the space
                    # space starts at all the files + spaces that come before it plus the fill of the current space
                    # start_in_expanded = files[1]
                    start_in_expanded = files[1]
                    for j in 1:(space_i - 1)
                        start_in_expanded += spaces[j]
                        start_in_expanded += files[j + 1]
                    end
                    start_in_expanded += fill
                    end_in_expanded = start_in_expanded + file - 1

                    # This adds some score
                    for expanded_i in start_in_expanded:1:end_in_expanded
                        score += expanded_i * (file_i - 1)
                    end

                    # this it fills the current space
                    # println(score)
                    fills[space_i] += file
                    break
                end
                # start_in_expanded += space
                # start_in_expanded += files[space_i + 1]
            end
            if !found
                # Stil gets score
                start_in_expanded = 0
                for j in 2:file_i
                    start_in_expanded += files[j - 1] + spaces[j - 1]
                    # start_in_expanded += spaces[j - 1]
                end
                end_in_expanded = start_in_expanded + file - 1
                for expanded_i in start_in_expanded:1:end_in_expanded
                    score += (expanded_i) * (file_i - 1)
                end
            end
        end
    end
    return score
end

function main()
    data = parse_data()
    sol = solve_problem(data)

    println(sol)

    @btime solve_problem($data) seconds = 1

    # Profile.clear()
    # Profile.@profile for _ in 1:100
    #     solve_problem(data)
    # end

    # PProf.pprof()
end

main()