using DataStructures

function parse_data()
    data = []
    open(joinpath(dirname(@__FILE__), "data/day01.txt")) do f
        while !eof(f)
            line = readline(f)
            line = split(line, "   ")
            line = parse.(Int, line)
            push!(data, line)
        end
    end
    return data
end

function solve_problem(data)
    list1 = getindex.(data, 1)
    list2 = getindex.(data, 2)
    sort!(list1)
    sort!(list2)
    sol = 0
    for el1 in list1
        counts = count(el2 -> el2 == el1, list2)
        sol += el1 * counts
    end

    return sol
end

data = parse_data()
sol = solve_problem(data)
@btime solve_problem($data) seconds = 1