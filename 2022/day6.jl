using BenchmarkTools

function parse_data()
    open(joinpath(dirname(@__FILE__), "data/day6.txt")) do f
        while !eof(f)
            line = readline(f)
            return line
        end
    end
end

@inbounds function solve_problem(data)
    marker_length = 14
    len = length(data)
    i = 1
    while i <= len
        complete = true
        for (j, item) in enumerate(reverse(view(data, i:i+marker_length - 2)))
            if occursin(item, view(data, i + marker_length - j:i + marker_length - 1))
                i += marker_length - j - 1
                complete = false
                break
            end
        end
        if complete
            return i + marker_length - 1
        end
        i += 1
    end
end


function main()
    data = parse_data()
    sol = solve_problem(data)
#     println(sol)
end


# main()
@benchmark main()
