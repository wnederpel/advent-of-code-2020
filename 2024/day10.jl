using DataStructures

function parse_data()
    grid = Vector{Vector{Int}}()
    heads = Vector{Vector{Int}}()
    open(joinpath(dirname(@__FILE__), "data/day10.txt")) do f
        row = 1
        while !eof(f)
            line = parse.(Int, collect(readline(f)))
            for (col, val) in enumerate(line)
                if val == 0
                    push!(heads, [row, col])
                end
            end
            push!(grid, line)
            row += 1
        end
    end
    return grid, heads
end

function solve_problem(data)
    println(data)
    grid, heads = data
    neighs = [[-1, 0], [1, 0], [0, -1], [0, 1]]
    score = 0
    reached = 0
    for head in heads
        can_reach = Set{Vector{Int}}()
        visited = Set{Vector{Int}}()
        to_process_stack = Stack{Vector{Int}}()
        push!(to_process_stack, head)
        while !isempty(to_process_stack)
            process = pop!(to_process_stack)
            # println("process $process")
            # if process in visited
            # println("already visited!")
            # continue
            # end

            push!(visited, process)

            cur_val = grid[process[1]][process[2]]

            if cur_val == 9
                # push!(can_reach, process)
                reached += 1
                continue
            end

            for n in neighs
                n_node = process + n
                if 1 <= n_node[1] <= length(grid) && 1 <= n_node[2] <= length(grid[1])
                    new_val = grid[n_node[1]][n_node[2]]
                    if new_val == cur_val + 1
                        # println("am at val $cur_val, go to $new_val at $n_node")
                        push!(to_process_stack, n_node)
                    end
                else
                    # println("node $n_node not in range")
                end
            end
        end
        # println(length(can_reach))
        # score += length(can_reach)
        # println(reached)
    end
    return reached
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
end

main()