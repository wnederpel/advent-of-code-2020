using DataStructures

function parse_data()
    data = []
    start = [-1, -1]
    open(joinpath(dirname(@__FILE__), "data/day06.txt")) do f
        row = 0
        while !eof(f)
            row += 1
            line = collect(readline(f))
            push!(data, line)
            col = findfirst(item -> item == '^', line)
            if col !== nothing
                start = [col, row]
            end
        end
    end
    return data, start
end

function solve_problem(data)
    data, start = data
    dir = [0, -1]
    visited, loop = run(start, dir, data)
    println(length(visited))
    println(loop)

    res = Set()
    for vis in visited
        if vis == start
            continue
        end
        vis_loc, vis_dirs = vis
        # for vis_dir in vis_dirs
        # next_vis = vis_loc + vis_dir
        # if 1 <= next_vis[1] <= length(data) && 1 <= next_vis[2] <= length(data[1])
        # next_token = data[vis_loc[2]][vis_loc[1]]
        # if next_token != '#'
        data[vis_loc[2]][vis_loc[1]] = '#'

        _, loop = run(start, [0, -1], data)
        if loop
            push!(res, vis_loc)
        end

        data[vis_loc[2]][vis_loc[1]] = '.'
        # end
        # end
        # end
    end
    println(res)
    return length(res)
end

function run(start, dir, data)
    start_col, start_row = start
    visited = DefaultDict(() -> Set())
    up = [0, -1]
    down = [0, 1]
    right = [1, 0]
    left = [-1, 0]
    # push!(visited[(start_col, start_row)], up)
    turns = Dict(up => right, right => down, down => left, left => up)
    next = start
    next_col, next_row = next
    while 1 <= next_row <= length(data) && 1 <= next_col <= length(data[1])
        if dir in visited[start]
            return visited, true
        end
        # println("visiting $start in $dir")
        push!(visited[start], dir)
        next = start + dir
        next_col, next_row = next

        if !(1 <= next_row <= length(data) && 1 <= next_col <= length(data[1]))
            break
        end
        if data[next_row][next_col] == '#'
            dir = turns[dir]
            continue
        else
            # actually go to the next place
            start_col, start_row = start

            # # The the right turn of the current dir is already visited, we can create a loop
            # for visited_dir in visited[(start_col, start_row)]
            #     if visited_dir == turns[dir]
            #         # Try to placed a blocker after start, so at next, if it was not yet visited
            #         if visited[(next_col, next_row)] == Set()
            #             println((next_col, next_row))
            #             push!(options, (next_col, next_row))
            #         end
            #     end
            # end
            # # Also add visiteds in the backwards direction until a # for loop detection
            # back_next_col, back_next_row = start
            # lines_to_explore = Stack{Tuple}()
            # push!(lines_to_explore, (start, -dir))
            # while !isempty(lines_to_explore)
            #     # println(lines_to_explore)
            #     (back_next_col, back_next_row), back_dir = pop!(lines_to_explore)
            #     # println(!isempty(lines_to_explore))
            #     # println()
            #     # println("exploring $((back_next_col, back_next_row)) in back dir $(back_dir)")

            #     while 1 <= back_next_row <= length(data) &&
            #               1 <= back_next_col <= length(data[1]) &&
            #               data[back_next_row][back_next_col] != '#'
            #         # println("visiting $((back_next_col, back_next_row))")
            #         if -back_dir in visited[(back_next_col, back_next_row)]
            #             # println(
            #             #     "already explored $((back_next_col, back_next_row)) in back dir $(back_dir)",
            #             # )
            #             break
            #         else
            #         end
            #         push!(visited[(back_next_col, back_next_row)], -back_dir)

            #         # If to the right of the back dir is a #, add left to lines to lines_to_explore
            #         right_back_dir = turns[back_dir]
            #         right_spot = [back_next_col, back_next_row] + right_back_dir
            #         right_back_next_col, right_back_next_row = right_spot

            #         if 1 <= right_back_next_row <= length(data) &&
            #             1 <= right_back_next_col <= length(data[1]) &&
            #             data[right_back_next_row][right_back_next_col] == '#'
            #             # println(
            #             #     "I should epxlore $((back_next_col, back_next_row)) in back_dir $(-right_back_dir)",
            #             # )
            #             push!(lines_to_explore, ((back_next_col, back_next_row), -right_back_dir))
            #         end

            #         # If to the left of the back dir is a #, add right to lines to lines_to_explore
            #         left_back_dir = turns[-back_dir]
            #         left_spot = [back_next_col, back_next_row] + left_back_dir
            #         left_back_next_col, left_back_next_row = left_spot

            #         if 1 <= left_back_next_row <= length(data) &&
            #             1 <= left_back_next_col <= length(data[1]) &&
            #             data[left_back_next_row][left_back_next_col] == '#'
            #             # println(
            #             #     "I should epxlore $((back_next_col, back_next_row)) in back_dir $(-left_back_dir)",
            #             # )
            #             push!(lines_to_explore, ((back_next_col, back_next_row), -left_back_dir))
            #         end

            #         back_next_col, back_next_row = [back_next_col, back_next_row] + back_dir
            #     end
            #     # println(!isempty(lines_to_explore))
            # end

            start = next
        end
    end
    return visited, false
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
    @btime solve_problem($data) seconds = 1
end

main()