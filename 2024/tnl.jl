
bf_instructions = ['>', '<', '+', '-', '.', '[', ']', ',', '_', '_']
letters = ['T', 'U', 'E', 'I', '_', 'C', 'H', 'N', 'O', 'L']

input = "TCTNTNTNTTNTHETCTCTTCTNTCTU_NOCECELNHUN_NEENTETUUN_ONETNTTNIETIU"

translations = Dict()
# a dict with 7! = 5040 entries

function generate_permutations(el_to_go, elements; start="")
    if el_to_go == 0
        return start
    end

    sols = []
    for i in 1:length(elements)
        new_start = start * elements[i]
        new_elements = filter(char -> char != elements[i], elements)
        sol = generate_permutations(el_to_go - 1, new_elements; start=new_start)
        sols = [sols; sol]
    end
    return sols
end

function bf_eval(string, input)
    input_index = 1
    loop_count = 0
    cells = zeros(Int, 30_000)
    pointer = 1
    index = 0
    res = ""
    while index < length(string)
        loop_count += 1
        if loop_count > 30_000
            return res
        end
        index += 1
        char = string[index]
        if char == '_'
            continue
        end
        if char == '>'
            pointer += 1
        elseif char == '<'
            pointer -= 1
        elseif char == '+'
            cells[pointer] += 1
        elseif char == '-'
            cells[pointer] -= 1
        elseif char == '.'
            # println(cells[pointer])
            # print(String(UInt8.([cells[pointer]])))
            res *= String(UInt8.([cells[pointer]]))
        elseif char == ','
            cells[pointer] = Int(input[input_index])
            input_index += 1
        elseif char == '['
            if cells[pointer] == 0
                # Skip to matching ]
                count_left = 0
                count_right = 0
                new_index = index + 1
                while string[new_index] != ']' || count_left != count_right - 1
                    new_index += 1
                    if string[new_index] == '['
                        count_left += 1
                    elseif string[new_index] == ']'
                        count_right += 1
                    end
                end
                index = new_index + 1
            end
        elseif char == ']'
            if cells[pointer] != 0
                # Skip to matching [
                count_left = 0
                count_right = 0
                new_index = index - 1
                while string[new_index] != '[' || count_left - 1 != count_right
                    new_index -= 1
                    if string[new_index] == '['
                        count_left += 1
                    elseif string[new_index] == ']'
                        count_right += 1
                    end
                end
                index = new_index
            end
        end
    end
    return res
end

print(
    bf_eval(
        "++++++++[>++++[>++>+++>+++>+<<<<-]>+>+>->>+[<]<-]>>.>---.+++++++..+++.>>.<-.<.+++.------.--------.>>+.>++.",
        "",
    ),
)

permutations = generate_permutations(10, "1234567890")
println("generated $(length(permutations)) permutations ")
golden_permutation = "1234567890"
for permutation in permutations
    new_input = input
    for i in 1:10
        new_i = parse(Int, permutation[i])
        if new_i == 0
            new_i += 10
        end
        new_input = replace(new_input, letters[i] => bf_instructions[new_i])
    end
    if count(char -> char == '.', new_input) != 2 ||
        count(char -> char == ',', new_input) != 1 ||
        (count(char -> char == '[', new_input) != count(char -> char == ']', new_input))
        continue
    end
    # println(new_input)
    try
        bf_res = bf_eval(new_input, "1")
        if bf_res == "42"
            print("found golden permutation!")
            global golden_permutation = permutation
            break
        end
    catch
        continue
    end
end

golden_permutation = "3215496078"

new_input = "TCTTCHNETCTTCTU_OEHUTNTE_OUCELHETCTUC_OEHETETNENTCUCUCU_OUNUHETETETUUCU_OLHETU_ONLNECEH_NUCUC_ECEONUNTCTCIECENE_CIU_INUCUCUCIENECEECETCITCTTTCTCI_C__NELC_NECTCTNTTHUN_C_C_E_NONUHUIE_OUCUCU_N_C__N_C_IECECIUCUUUCUCTI"
println("golden_permutation")
println(golden_permutation)
for i in 1:10
    new_i = parse(Int, golden_permutation[i])
    if new_i == 0
        new_i += 10
    end
    global new_input = replace(new_input, letters[i] => bf_instructions[new_i])
end
println(new_input)
println(bf_eval(new_input, "1987"))
