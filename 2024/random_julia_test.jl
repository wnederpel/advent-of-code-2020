function f(x::AbstractChar)
    if isnumeric(x)
        return parse(Int, x)
    else
        return -1
    end
end
@btime f('1')
@btime f('a')