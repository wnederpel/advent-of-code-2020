using BenchmarkTools
using Revise
using DataStructures
using Profile
using PProf

function parse_data()
    data = []
    open(joinpath(dirname(@__FILE__), "../data/day03.txt")) do f
        while !eof(f)
            line = readline(f)
            push!(data, line)
        end
    end
    global len_line = length(data[1])
    global len_data = length(data)
    return data
end

function belongs_to_part(row, col_range, data, number)
    # Look above
    res = false
    range = max(col_range[1] - 1, 1):min(col_range[2] + 1, len_line)
    if row > 1
        line = data[row - 1]
        for col in range
            char = line[col]
            if !isdigit(char) && char != "."
                if char == '*'
                    gears[(row - 1, col)][1] += 1
                    gears[(row - 1, col)][2] *= number
                end
                res = true
            end
        end
    end
    # Look below
    if row < len_data
        line = data[row + 1]
        for col in range
            char = line[col]
            if !isdigit(char) && char != "."
                if char == '*'
                    gears[(row + 1, col)][1] += 1
                    gears[(row + 1, col)][2] *= number
                end
                res = true
            end
        end
    end
    # Look beside
    line = data[row]
    if col_range[1] > 1
        char = line[col_range[1] - 1]
        if !isdigit(char) && char != "."
            if char == '*'
                gears[(row, col_range[1] - 1)][1] += 1
                gears[(row, col_range[1] - 1)][2] *= number
            end
            res = true
        end
    end
    if col_range[2] < len_line
        char = line[col_range[2] + 1]
        if !isdigit(char) && char != "."
            if char == '*'
                gears[(row, col_range[2] + 1)][1] += 1
                gears[(row, col_range[2] + 1)][2] *= number
            end
            res = true
        end
    end
    return res
end

function solve_problem(data)
    global gears = DefaultDict() do
        return [0, 1]
    end
    for (row, line) in enumerate(data)
        i = 1
        while i <= len_line
            char = line[i]
            if isdigit(char)
                end_number = i
                for nextchar in line[(i + 1):end]
                    if !isdigit(nextchar)
                        break
                    end
                    end_number += 1
                end
                # So there is anumber from i - end_number
                number = parse(Int, line[i:end_number])
                belongs_to_part(row, (i, end_number), data, number)

                i = end_number + 1
            else
                i += 1
            end
        end
    end
    # gears |> display
    res = 0
    for gear in values(gears)
        if gear[1] == 2
            res += gear[2]
        end
    end
    return res
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
    # b = @benchmarkable parse_data()
    # tune!(b)
    # run(b)
    b = @benchmarkable solve_problem($data)
    tune!(b)
    run(b)
end

main()
