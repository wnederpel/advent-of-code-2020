using BenchmarkTools
using DataStructures
using ProfileView

function parse_data()
    data = []
    open(joinpath(dirname(@__FILE__), "data/day18.txt")) do f
        while !eof(f)
            line = readline(f)
            push!(data, parse.(Int, split(line,",")))
        end
    end
    return data
end

# function get_neighs(cur, min_x, max_x, min_y, max_z)

function solve_problem(data)
    blob = DefaultDict(false)
    outside = DefaultDict(false)

    max_x = maximum(x -> x[1], data) + 1
    min_x = minimum(x -> x[1], data) - 1

    max_y = maximum(x -> x[2], data) + 1
    min_y = minimum(x -> x[2], data) - 1

    max_z = maximum(x -> x[3], data) + 1
    min_z = minimum(x -> x[3], data) - 1

    println((min_x, max_x, min_y, max_y, min_z, max_z))

    for datum in data
        blob[datum] = true
    end

    que = [[min_x, min_y, min_z]]
#     visited = Set{Tuple{Int,Int,Int}}()
    visited = DefaultDict(false)
    visited[[min_x, min_y, min_z]] = true
    sides = [[1,0,0], [0,1,0], [0, 0, 1]]
    spaces = (max_x - min_x + 2) * (max_y - min_y + 2) * (max_z - min_z + 2)
    que_len = 1
    iter = 0
    while que_len != 0
        iter += 1
#         println(que)
#         println(visited)
        cur = popfirst!(que)
#         println(cur)
#         println()
#         if iter > 3000
#             @error("to many iters")
#             break
#         end
        que_len -= 1
        visited[cur] = true
        for side in sides
            neigh = cur + side
            if !visited[neigh] && !blob[neigh] && min_x <= neigh[1] <= max_x && min_y <= neigh[2] <= max_y && min_z <= neigh[3] <= max_z
                push!(que, neigh)
                visited[neigh] = true
                que_len += 1
            end
            neigh = cur - side
            if !visited[neigh] && !blob[neigh] && min_x <= neigh[1] <= max_x && min_y <= neigh[2] <= max_y && min_z <= neigh[3] <= max_z
                push!(que, neigh)
                visited[neigh] = true
                que_len += 1
            end
        end
    end
#     return
    count = 0
    for datum in data
        for side in sides
            neigh = datum + side
            if !blob[datum + side] && visited[datum + side]
                # it does not touch something on this side and that side has been visited
                count += 1
            end
            neigh = datum - side
            if !blob[datum - side] && visited[datum - side]
                # it does not touch something on this side too and that side has been visited
                count += 1
            end
        end
    end
    return count
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
end

main()
@time main()
# @profview main()