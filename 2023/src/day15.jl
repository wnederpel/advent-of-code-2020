using BenchmarkTools

function parse_data()
    open(joinpath(dirname(@__FILE__), "../data/day15.txt")) do f
        while !eof(f)
            line = readline(f)
            return split(line, ',')
        end
    end
end

function hash(string)
    res = 0
    for char in string
        res += Int(char) 
        res *= 17
        res %= 256
    end
    return res
end

function solve_problem(data)
    res = 0
    boxes = Dict()
    for string in data
        if string[end] == '-'
            label, operation = string[begin:end-1], string[end]
        else
            label, operation, value = string[begin:end-2], string[end-1], parse(Int, string[end])
        end
        label_val = hash(label)
        if !(label_val in keys(boxes))  
            boxes[label_val] = MutableLinkedList{Tuple{String, Int}}()
        end
        box = boxes[label_val]
        loc = findfirst(x -> x == label, getindex.(box, 1))
        if operation == '-'
            if loc !== nothing
                delete!(box, loc)
            end
        else
            if loc !== nothing
                box[loc] = (label, value)
            else
                push!(box, (label, value))
            end
        end
    end
    boxes |> display
    for (boxi, box) in boxes
        for (j, (_, value)) in enumerate(box)
            (boxi+1, j, value) |> display
            res += (boxi + 1) * j * value
        end
    end
    return res
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
end

main()
