Computation times:

Computed using BenchmarkTools.jl. 
Time presented is median time resulting from 
```
b = @benchmarkable <function>
tune!(b)
run(b)
```

Time presented in 
- ms = 10 ^ -3 s
- us = 10 ^ -6 s
- ns = 10 ^ -9 s

Only solution time has been optimized (somewhat), parse time has been neglected and is included only for completeness, input parsing does nothing extravagant.
Code is intented to handle a general problem, no input specific optimizations have been made. Result made with Technolution laptop and without fancy GPU optimizations.

| day | solution time | parse time | notes                                                                                                              |
|-----|---------------|------------|--------------------------------------------------------------------------------------------------------------------|
| 1   | 2.45 ms       | 110 us     |                                                                                                                    |
| 2   | 235 us        | 610 us     |                                                                                                                    |
| 3   | 700 us        | 65 us      |                                                                                                                    |
| 4   | 700 us        | 1.6 ms     | Relatively large parse time                                                                                        |
| 5   | 570 us        | 135 us     |                                                                                                                    |
| 6   | 12 ns         | 55 us      | fast!                                                                                                              |
| 7   | 960 us        | 273 us     |                                                                                                                    |
| 8   | 515 ms        | 400 us     | Code is intended be able to handle a general instance of the problem, so no lcm is used.                           |
| 8*  | 8.3 ms         | 400 us    | Using the assumption that lcm works. |
| 9  | 890 us         | 400 us     |  |
| 10  | 1.2 ms         | 70 us     | Check what is inside by doing a flood fill from the outside with expanding each node to a 9x9 grid to be able to go through pipes. Solution using smart inside / outside detection without path finder is probably faster. |
| 11  | 100 ms         | 210 us     |  |