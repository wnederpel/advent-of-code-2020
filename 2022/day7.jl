using DataStructures
using BenchmarkTools

function parse_data()
    data = []
    open(joinpath(dirname(@__FILE__), "data/day7.txt")) do f
        while !eof(f)
            line = readline(f)
            push!(data, line)
        end
    end
    return data
end

function solve_problem(data)
    path = []
    sizes = DefaultDict(0)
    for datum in data
        splitted = split(datum, " ")
        if splitted[1] == "\$"
            if splitted[2] == "cd" && splitted[3] != ".."
                if !isempty(path)
                    prev_path = path[end]
                else
                    prev_path = ""
                end
                new_path = prev_path * splitted[3]
                push!(path, new_path)
            elseif splitted[2] == "cd" && splitted[3] == ".."
                pop!(path)
            elseif splitted[2] == "ls"
                continue
            end
        else
            if splitted[1] == "dir"
                continue
            else
                size = parse(Int, splitted[1])
                for dir in path
                    sizes[dir] += size
                end
            end
        end
    end
    total_space = 70_000_000
    needed_space = 30_000_000
    used_space = sizes["/"]
    unused_space = total_space - used_space
    space_to_free = needed_space - unused_space
    smallest_dir_size = Inf
    for (dir, size) in sizes
        if size > space_to_free
            if size < smallest_dir_size
                smallest_dir_size = size
            end
        end
    end
    return smallest_dir_size
end

function main()
    data = parse_data()
    @time sol = solve_problem(data)
    println(sol)
end

@time main()