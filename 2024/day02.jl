using DataStructures

function parse_data()
    data = []
    open(joinpath(dirname(@__FILE__), "data/day02.txt")) do f
        while !eof(f)
            line = readline(f)
            push!(data, parse.(Int, split(line, " ")))
        end
    end
    return data
end

function solve_problem(data)
    sol = 0
    for report in data
        to_remove = 0:length(report)
        any_right = false
        for rem in to_remove
            decreasing = true
            increasing = true
            gradual = true
            start = rem == 1 ? 2 : 1
            prev = report[start]
            # println("deleting $rem")
            for i in (start + 1):length(report)
                if i == rem
                    continue
                end
                cur = report[i]
                # println("comparing $prev and $cur")

                decreasing &= cur < prev
                increasing &= prev < cur
                gradual &= prev != cur && abs(prev - cur) <= 3

                if !(gradual & (increasing | decreasing))
                    break
                end
                prev = cur
            end
            any_right |= gradual & (increasing | decreasing)
            if any_right
                break
            end
        end

        if any_right
            sol += 1
        end
    end
    return sol
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
    @btime solve_problem($data) seconds = 1
end

main()