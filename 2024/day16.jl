using DataStructures

function parse_data()
    data = Vector{String}()
    open(joinpath(dirname(@__FILE__), "data/day16.txt")) do f
        while !eof(f)
            line = readline(f)
            push!(data, line)
        end
    end
    return data
end

function solve_problem(data)
    start = [length(data) - 1, 2], :right
    front = PriorityQueue{Tuple{Vector{Int},Symbol},Int}()
    fixed_dists = Dict{Tuple{Vector{Int},Symbol},Int}()

    front[start] = 0

    fastest_time = -1
    end_dir = -1
    histories = Dict{Tuple{Vector{Int},Symbol},Vector{Tuple{Vector{Int},Symbol}}}(start => [])
    while !isempty(front)
        cur_loc, cur_dist = peek(front)
        fixed_dists[cur_loc] = cur_dist
        delete!(front, cur_loc)

        # println("exploring $((cur_loc, cur_dist))")

        if cur_loc[1] == [2, length(data[1]) - 1]
            fastest_time = cur_dist
            end_dir = cur_loc[2]
            break
        end

        neighs, score_add = get_neighs(cur_loc)
        for (n, cost) in zip(neighs, score_add)
            if data[n[1][1]][n[1][2]] == '#' # || n in visited
                continue
            end
            cur_cost = -1
            if n in keys(fixed_dists)
                cur_cost = fixed_dists[n]
            else
                if !(n in keys(front))
                    front[n] = typemax(Int)
                end
                cur_cost = front[n]
            end
            new_cost = cur_dist + cost
            if new_cost < cur_cost
                front[n] = new_cost
                # println("cost of $n is $new_cost, $(front[n])")
                # println("can only do down!!")
                histories[n] = [cur_loc]
                # if n[1] == [8, 5]
                #     println("moving to $(n[1]) from $(cur_loc[1])")
                #     println()
                # end
            elseif new_cost == cur_cost
                push!(histories[n], cur_loc)
            end
        end
    end
    all_histories = Set{Vector{Int}}()
    locs_to_check = Stack{Tuple{Vector{Int},Symbol}}()
    push!(locs_to_check, ([2, length(data[1]) - 1], end_dir))
    while !isempty(locs_to_check)
        loc = pop!(locs_to_check)
        push!(all_histories, loc[1])
        for hloc in histories[loc]
            if hloc == loc
                println("inf loop")
                break
            end
            # println("from $loc to $hloc")
            push!(locs_to_check, hloc)
        end
        # println()
    end

    return length(all_histories)
end

function get_neighs(cur_loc)
    loc, dir = cur_loc
    dir_vecs = Dict{Symbol,Vector{Int}}(
        :up => [-1, 0], :down => [1, 0], :right => [0, 1], :left => [0, -1]
    )
    neigh_dir_vecs = Dict{Symbol,Vector{Symbol}}(
        :up => [:left, :right],
        :down => [:left, :right],
        :right => [:down, :up],
        :left => [:down, :up],
    )
    return (
        (loc + dir_vecs[dir], dir), (loc, neigh_dir_vecs[dir][1]), (loc, neigh_dir_vecs[dir][2])
    ),
    (1, 1000, 1000)
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    # println(sol)
    return sol
end

sol = main()