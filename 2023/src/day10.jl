using BenchmarkTools
using DataStructures
using Revise
using Profile
using PProf

function parse_data()
    map = []
    s = nothing
    i = 1
    open(joinpath(dirname(@__FILE__), "../data/day10.txt")) do f
        while !eof(f)
            line = readline(f)
            if 'S' in line
                s = (i, findfirst('S', line))
            end
            push!(map, line)
            i += 1
        end
    end
    return s, map
end

function access(map, loc, map_size)
    x, y = loc
    if 1 <= x <= map_size[1] && 1 <= y <= map_size[2]
        return map[x][y]
    else
        return '.'
    end
end

function access_explored(explored, loc, explored_size)
    x, y = loc
    if 1 <= x <= explored_size[1] && 1 <= y <= explored_size[2]
        return explored[x, y]
    else
        return false
    end
end

function neighbors(s)
    x, y = s
    return ((x - 1, y), (x, y - 1), (x, y + 1), (x + 1, y))
end

function tuple_min(tuple1, tuple2)
    return (tuple1[1] - tuple2[1], tuple1[2] - tuple2[2])
end

function connects(neigh, s, char)
    # neigh is to direct left of s
    diff = tuple_min(neigh, s)
    return (diff == (0, -1) && (char == '-' || char == 'F' || char == 'L')) ||
           (diff == (0, 1) && (char == '-' || char == '7' || char == 'J')) ||
           (diff == (-1, 0) && (char == '|' || char == '7' || char == 'F')) ||
           (diff == (1, 0) && (char == '|' || char == 'L' || char == 'J'))
end

function new_possibilities(char, loc)
    x, y = loc
    if char == '|'
        return ((x + 1, y), (x - 1, y))
    elseif char == '-'
        return ((x, y + 1), (x, y - 1))
    elseif char == 'J'
        return ((x - 1, y), (x, y - 1))
    elseif char == 'F'
        return ((x + 1, y), (x, y + 1))
    elseif char == '7'
        return ((x + 1, y), (x, y - 1))
    elseif char == 'L'
        return ((x - 1, y), (x, y + 1))
    end
end

function fill_new_map(char, loc, new_map)
    x, y = loc
    new_base_x, new_base_y = 2 * x, 2 * y

    new_map[new_base_x, new_base_y] = false

    if char == '|'
        new_map[new_base_x + 1, new_base_y] = false
        new_map[new_base_x - 1, new_base_y] = false
    elseif char == '-'
        new_map[new_base_x, new_base_y + 1] = false
        new_map[new_base_x, new_base_y - 1] = false
    elseif char == 'F'
        new_map[new_base_x + 1, new_base_y] = false
        new_map[new_base_x, new_base_y + 1] = false
    elseif char == 'J'
        new_map[new_base_x - 1, new_base_y] = false
        new_map[new_base_x, new_base_y - 1] = false
    elseif char == '7'
        new_map[new_base_x + 1, new_base_y] = false
        new_map[new_base_x, new_base_y - 1] = false
    elseif char == 'L'
        new_map[new_base_x - 1, new_base_y] = false
        new_map[new_base_x, new_base_y + 1] = false
    end
end

function solve_problem(data)
    s, map = data
    map_size = length(map), length(map[1])
    new_map = fill(true, length(map) * 2 + 1, length(map[1]) * 2 + 1)
    cur_char = nothing
    cur_loc = nothing
    fill_new_map('S', s, new_map)
    for neigh in neighbors(s)
        char = access(map, neigh, map_size)
        if char == '.'
            continue
        end
        if connects(neigh, s, char)
            cur_char = char
            cur_loc = neigh
            break
        end
    end
    len = 1
    prev_loc = s
    while cur_char != 'S'
        fill_new_map(cur_char, cur_loc, new_map)
        news = new_possibilities(cur_char, cur_loc)
        for new in news
            x, y = new
            new_char = map[x][y]
            if new == prev_loc
                continue
            end
            prev_loc = cur_loc
            cur_loc = new
            cur_char = new_char
            len += 1
            break
        end
    end
    # Idea is to mark wall of the main loop, then
    return length(map) * length(map[1]) - compute_outside_region(new_map) - len
end

function compute_outside_region(new_map)
    res = 0
    # Start at top left, throw error if impossible
    start = 1, 1
    new_map_size = size(new_map)
    new_map[start...] = false
    queue = Queue{Tuple{Int,Int}}()
    enqueue!(queue, start)

    while !isempty(queue)
        node = dequeue!(queue)
        if node[1] % 2 == 0 && node[2] % 2 == 0
            res += 1
        end
        for neigh in neighbors(node)
            if access_explored(new_map, neigh, new_map_size)
                new_map[neigh...] = false
                enqueue!(queue, neigh)
            end
        end
    end
    return res
end

function solve_problem_area(data)
    s, map = data
    map_size = length(map), length(map[1])
    cur_char = nothing
    cur_loc = nothing
    for neigh in neighbors(s)
        char = access(map, neigh, map_size)
        if char == '.'
            continue
        end
        if connects(neigh, s, char)
            cur_char = char
            cur_loc = neigh
            break
        end
    end
    len = 1
    horizontals = 0
    prev_loc = s
    start_area_block = nothing
    end_area_block = nothing
    prev_added = nothing
    area = 0
    while cur_loc != s
        if cur_char == '-'
            horizontals += 1
        end
        if cur_char == 'L' ||
            cur_char == 'J' ||
            cur_char == '7' ||
            cur_char == 'F' ||
            cur_char == 'S'
            if start_area_block === nothing
                start_area_block = cur_loc[1]
            else
                end_area_block = cur_loc[1]
                if (end_area_block - start_area_block) > 0 &&
                    (prev_added === nothing || prev_added < 0)
                    added = (end_area_block - start_area_block + 1) * cur_loc[2]
                elseif (prev_added === nothing || prev_added > 0)
                    added = (end_area_block - start_area_block - 1) * cur_loc[2]
                else
                    added = (end_area_block - start_area_block) * cur_loc[2]
                end
                prev_added = added
                area += added
                display(
                    "adding area from $start_area_block to $end_area_block with distance to wall $(cur_loc[2])",
                )
                display("added = $added, for total of $area")
                start_area_block = nothing
            end
        end

        news = new_possibilities(cur_char, cur_loc)
        for new in news
            x, y = new
            new_char = map[x][y]
            if new == prev_loc
                continue
            end
            prev_loc = cur_loc
            cur_loc = new
            cur_char = new_char
            len += 1
            break
        end
    end
    if cur_char == 'L' ||
        cur_char == 'J' ||
        cur_char == '7' ||
        cur_char == 'F' ||
        cur_char == 'S'
        if start_area_block === nothing
            start_area_block = cur_loc[1]
        else
            end_area_block = cur_loc[1]
            if (end_area_block - start_area_block) > 0 && prev_added < 0
                added = (end_area_block - start_area_block + 1) * cur_loc[2]
            elseif prev_added > 0
                added = (end_area_block - start_area_block - 1) * cur_loc[2]
            else
                added = (end_area_block - start_area_block) * cur_loc[2]
            end
            area += added
            display(
                "adding area from $start_area_block to $end_area_block with distance to wall $(cur_loc[2])",
            )
            display("added = $added, for total of $area")
            start_area_block = nothing
        end
    end
    area = abs(area)
    # Idea is to mark wall of the main loop, then
    verticals = len - horizontals
    return area - verticals / 2 - horizontals
end

function main()
    global s_char = 'F'
    data = parse_data()
    @time sol = solve_problem_area(data)
    println(sol)
    # Profile.clear()
    # @profile (
    #     for i = 1:500
    #         solve_problem(data)
    #     end
    # )
    # pprof()

    # b = @benchmarkable solve_problem($data)
    # tune!(b)
    # run(b)
end

main()
