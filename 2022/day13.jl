using BenchmarkTools

function parse_data()
    data = []

    open(joinpath(dirname(@__FILE__), "data/day13.txt")) do f
        while !eof(f)
            line = readline(f)
            if line != ""
                push!(data, line)
            end
        end
    end
    return data
end

function get_elements(pack)
    # Return empty list if the string is empty
    if pack == "[]"
        return []
    end
    # Check if the pack is an integer
    if pack[1] != '['
        return parse(Int, pack)
    end
    # If not the pack is a list, return its elements
    elements = []
    start_element = -1
    open = 0
    close = 0
    for (i, char) in enumerate(pack)
        if char == '['
            open += 1
            if open - close == 1
                start_element = i + 1
            end
        elseif char == ']'
            if open - close == 1
                push!(elements, get_elements(view(pack, start_element:i-1)))
                start_element = i + 1
            end
            close += 1
        elseif char == ',' && open - close == 1
            push!(elements, get_elements(view(pack, start_element:i-1)))
            start_element = i + 1
        end
    end
    return elements
end

function check_elem1_before_elem2(elem1, elem2)::Int8
    if typeof(elem1) <: Vector && typeof(elem2) <: Vector
        if length(elem1) > length(elem2)
            length_check = 0
        elseif length(elem1) < length(elem2)
            length_check = 1
        else
            length_check = -1
        end
        min_length = min(length(elem1), length(elem2))
        for i = 1:min_length
            el1, el2 = elem1[i], elem2[i]
            res = check_elem1_before_elem2(el1, el2)
            if !(res == -1)
                return res
            end
        end
        return length_check
    elseif typeof(elem1) == typeof(elem2) <: Real
        if elem1 < elem2
            return 1
        elseif elem1 > elem2
            return 0
        else
            return -1
        end
    else
        if typeof(elem1) == Int
            check_elem1_before_elem2([elem1], elem2)
        else
            check_elem1_before_elem2(elem1, [elem2])
        end
    end
end

function solve_problem(data)
    elem_data = get_elements.(data)
    sort!(elem_data; lt = (x,y) -> Bool(check_elem1_before_elem2(x, y)))
    sol = 1
    div1 = [[2]]
    div2 = [[6]]
    for (i, elem) in enumerate(elem_data)
        if elem == div1 || elem == div2
            sol *= i
        end
    end
    return sol
#     for i = 1:Int(length(data) / 2)
#         pack1 = data[2 * i - 1]
#         pack2 = data[2 * i]
#         elem1 = get_elements(pack1)
#         elem2 = get_elements(pack2)
#         valid = check_elem1_before_elem2(elem1, elem2)
#
#         if valid
#             sol += i
#         end
#     end
#     return sol
end

function main()
    data = parse_data()
    sol = solve_problem(data)
#     println(sol)
end

main()
@time main()