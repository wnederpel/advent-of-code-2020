

def get_possible_strings(number, rules, valid_list):
    rule = rules[number]
    print('valid list start call:', valid_list)
    print('rule for current call:', repr(rule))
    if rule.count('"a"') == 1 or rule.count('"b"') == 1:
        for i, valid in enumerate(valid_list):
            valid_list[i] += rule[1]
    elif rule.count(' | ') == 1:
        cur_len_valid = len(valid_list)
        for i in range(cur_len_valid):
            valid_list.append(valid_list[i])
        rule_1, rule_2 = rule.split(' | ')
        for _ in range(cur_len_valid):
            next_rules = rule_1.split(' ')
            for _ in range(cur_len_valid):
                for next_rule in next_rules:
                    print(next_rule)
                    valid_list[:cur_len_valid] = get_possible_strings(next_rule, rules, valid_list[:cur_len_valid])
            next_rules = rule_2.split(' ')
            for _ in range(cur_len_valid):
                for next_rule in next_rules:
                    print(next_rule)
                    valid_list[cur_len_valid:] = get_possible_strings(next_rule, rules, valid_list[cur_len_valid:])
        print('after | call is valid list:', valid_list)
    else:
        next_rules = rule.split(' ')
        for next_rule in next_rules:
            valid_list = get_possible_strings(next_rule, rules, valid_list)


    print('valid list end call', valid_list)
    print()
    print()
    return valid_list



lines = open('data_day_19.txt').read().splitlines()

rules = {}
messages = []
rule = True
for line in lines:
    if line == '':
        rule = False
        continue
    if rule:
        key, value = line.split(': ')
        rules[key] = value
    else:
        messages.append(line)

rule_list = ['']
possible_strings = get_possible_strings('0', rules, rule_list)
print('rules:', rules)
print('possible strings:', possible_strings)