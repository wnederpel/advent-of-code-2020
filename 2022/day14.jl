using BenchmarkTools
using DataStructures
using ProfileView

function parse_data()
    data = []
    open(joinpath(dirname(@__FILE__), "data/day14.txt")) do f
        while !eof(f)
            line = readline(f)
            push!(data, line)
        end
    end
    return data
end

function solve_problem(data)
    occupied = DefaultDict{Tuple{Int, Int}, Bool}(false)
    minx, miny, maxx, maxy = Inf, Inf, -Inf, -Inf
    for datum in data
        points = split(datum, " -> ")
        for i = 1:length(points) - 1
            p1 = parse.(Int, split(points[i],","))
            p2 = parse.(Int, split(points[i + 1],","))
            xs = sort([p1[1], p2[1]])
            ys = sort([p1[2], p2[2]])
            for x = xs[1]:xs[2]
                for y = ys[1]:ys[2]
                    occupied[(x, y)] = true
                    if x > maxx maxx = x end
                    if y > maxy maxy = y end
                    if x < minx minx = x end
                    if y < miny miny = y end
                end
            end
        end
    end
    still_sands = 0
    voiding = false
    while !occupied[(500, 0)]
        sand_posx, sand_posy = 500, 0
        sand_still = false
        while !sand_still
            if sand_posy == maxy + 1
                occupied[(sand_posx, sand_posy)] = true
                still_sands += 1
                sand_still = true
                continue
            end
            if !occupied[(sand_posx, sand_posy + 1)]
                sand_posy += 1
            elseif !occupied[(sand_posx - 1, sand_posy + 1)]
                sand_posx -= 1
                sand_posy += 1
            elseif !occupied[(sand_posx + 1, sand_posy + 1)]
                sand_posy += 1
                sand_posx += 1
            else
                occupied[(sand_posx, sand_posy)] = true
                still_sands += 1
                sand_still = true
            end
        end
    end
    return still_sands
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
end

main()
