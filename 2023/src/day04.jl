using BenchmarkTools
using DataStructures
using Revise

function parse_data()
    data = []
    open(joinpath(dirname(@__FILE__), "../data/day04.txt")) do f
        while !eof(f)
            line = readline(f)
            line = split(line, ": ")[2]
            winning, my = split(line, " | ")
            w_numbers = split(winning, " ")
            m_numbers = split(my, " ")
            w = []
            m = []
            for num in w_numbers
                if isempty(num)
                    continue
                end
                num = parse(Int, num)
                push!(w, num)
            end
            for num in m_numbers
                if isempty(num)
                    continue
                end
                num = parse(Int, num)
                push!(m, num)
            end
            push!(data, (w, m))
        end
    end
    return data
end

function solve_problem(data)
    copies = DefaultDict(1)
    for card in eachindex(data)
        w, m = data[card]
        for i in eachindex(intersect(w, m))
            copies[card + i] += copies[card]
        end
    end
    res = 0
    for i in eachindex(data)
        res += copies[i]
    end
    return res
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
    # b = @benchmarkable parse_data()
    # tune!(b)
    # run(b)
    b = @benchmarkable solve_problem($data)
    tune!(b)
    run(b)
end

main()
