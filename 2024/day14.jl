using DataStructures

function parse_data()
    data = Tuple{Vector{Int},Vector{Int}}[]

    open(joinpath(dirname(@__FILE__), "data/day14.txt")) do f
        while !eof(f)
            line = readline(f)
            pos, vel = split(line, " ")
            pos = parse.(Int, split(pos[3:end], ','))
            vel = parse.(Int, split(vel[3:end], ','))
            push!(data, (pos, vel))
        end
    end
    return data
end

function solve_problem(data)
    END_RIGHT = 101
    END_DOWN = 103

    # END_RIGHT = 11
    # END_DOWN = 7
    check_nums = Dict()
    highest_match_end = Set()
    highest_match_score = 0

    for STEPS in 0:10000
        end_pos = Set()

        for (pos, vel) in data
            end_pos_x, end_pos_y = pos + vel * STEPS

            norm_end_pos_x = (end_pos_x + 1) % END_RIGHT - 1
            norm_end_pos_y = (end_pos_y + 1) % END_DOWN - 1
            if norm_end_pos_x < 0
                norm_end_pos_x += END_RIGHT
            end
            if norm_end_pos_y < 0
                norm_end_pos_y += END_DOWN
            end

            # println("$((pos, vel)) after $STEPS steps is at $((norm_end_pos_x, norm_end_pos_y))")
            push!(end_pos, (norm_end_pos_x, norm_end_pos_y))
        end

        # quandrants = [
        #     (1:(END_DOWN ÷ 2), 1:(END_RIGHT ÷ 2)),
        #     (1:(END_DOWN ÷ 2), ((END_RIGHT ÷ 2) + 2):END_RIGHT),
        #     (((END_DOWN ÷ 2) + 2):END_DOWN, 1:(END_RIGHT ÷ 2)),
        #     (((END_DOWN ÷ 2) + 2):END_DOWN, ((END_RIGHT ÷ 2) + 2):END_RIGHT),
        # ]
        # Count how many have one below to right or below to left and in the other diagonal
        christmas_checks = [[[-1, -1], [1, 1]], [[-1, 1], [1, -1]]]

        num_checks = 0
        for pos in end_pos
            for checks in christmas_checks
                valid_check = true
                for check in checks
                    check_pos = [pos...] + check
                    if !(tuple(check_pos...) in end_pos)
                        valid_check &= false
                    else
                    end
                end
                if valid_check
                    num_checks += 1
                end
            end
        end
        if num_checks > 9
            println((STEPS, num_checks))
            check_nums[end_pos] = num_checks
            if num_checks > highest_match_score
                highest_match_score = num_checks
                highest_match_end = end_pos
            end
        end
    end
    println("highest score $highest_match_score")
    for j in 0:(END_DOWN - 1)
        for i in 0:(END_RIGHT - 1)
            if (i, j) in highest_match_end
                print('x')
            else
                print('.')
            end
        end
        println()
    end
    println()

    # sol = 1
    # for (j_range, i_range) in quandrants
    #     q_sum = 0
    #     for j in j_range
    #         for i in i_range
    #             q_sum += end_pos[(i - 1, j - 1)]
    #         end
    #     end
    #     println(q_sum)
    #     sol *= q_sum
    # end

    return nothing
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
end

main()
