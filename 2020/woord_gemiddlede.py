from math import *
import random

woord_1 = 'chantal'
woord_2 = 'marieke'
woord_1 = list(woord_1)
woord_2 = list(woord_2)
woord_new = []
alfabet = 'bcdfghjklmnpqrstvwxyz'
vowels = 'aeiou'
alfabet = list(alfabet)

len_1 = len(woord_1)
len_2 = len(woord_2)

min_len = min(len_1, len_2)

if min_len == len_1 == len_2:
    even_lang = True
elif min_len == len_1:
    lang_woord = woord_2
    even_lang = False
elif min_len == len_2:
    lang_woord = woord_1
    even_lang = False


for char in range(min_len):
    char_1 = woord_1[char]
    char_2 = woord_2[char]
    if char_1 in vowels and char_2 in vowels:
        i_1 = vowels.index(char_1)
        i_2 = vowels.index(char_2)
        i_new = round((i_1 + i_2) / 2)
        woord_new.append(vowels[i_new])
    elif char_1 in vowels or char_2 in vowels:
        if random.uniform(0, 1) > 0.5:
            if random.uniform(0, 1) > 0.5:
                i_new = floor(random.uniform(0, 4.99))
                woord_new.append(vowels[i_new])
            else:
                if char_1 in vowels:
                    woord_new.append(char_1)
                else:
                    woord_new.append(char_2)
        else:
            if random.uniform(0, 1) > 0.5:
                i_new = floor(random.uniform(0, 20.99))
                woord_new.append(alfabet[i_new])
            else:
                if char_1 not in vowels:
                    woord_new.append(char_1)
                else:
                    woord_new.append(char_2)

    else:
        i_1 = alfabet.index(char_1)
        i_2 = alfabet.index(char_2)
        i_new = floor((i_1 + i_2) / 2)
        woord_new.append(alfabet[i_new])

print(min_len)
print(not even_lang)
if not even_lang:
    for i, char in enumerate(lang_woord):
        if i < min_len:
            continue
        if char not in vowels:
            chance = 0.2
            if random.uniform(0, 1) > chance:
                woord_new.append(char)
            else:
                index = floor(random.uniform(0, 20.99))
                woord_new.append(alfabet[index])
        else:
            woord_new.append(char)


print(''.join(woord_new))