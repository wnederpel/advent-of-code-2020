using BenchmarkTools
using DataStructures

function parse_data()
    data = []
    open(joinpath(dirname(@__FILE__), "data/day16.txt")) do f
        while !eof(f)
            line = readline(f)
            push!(data, line)
        end
    end
    return data
end

function setup_big_graph(data)
    big_graph = Dict{String, Tuple{Int, Vector{String}}}()
    for datum in data
        splitted = split(datum, " ")
        vertex_name = splitted[2]

        flow = string(split(splitted[5], "=")[2])
        flow = parse(Int, flow[begin:end-1])

        to = Vector{String}()
        for i = 10:length(splitted)
            if i != length(splitted)
                push!(to, splitted[i][begin:end-1])
            else
                push!(to, splitted[i])
            end
        end

        big_graph[vertex_name] = (flow, to)
    end
    return big_graph
end

function findshortest_distance(dict, v1, v2)
   que = []
   explored = DefaultDict{String, Bool}(false)
   dist = Dict{String, Int}()
   push!(que, v1)
   dist[v1] = 0
   explored[v1] = true
   while !isempty(que)
       v = popfirst!(que)
       if v == v2
           return dist[v]
       end
       for neigh = dict[v][2]
           if !explored[neigh]
               explored[neigh] = true
               dist[neigh] = dist[v] + 1
               push!(que, neigh)
           end
       end
   end
end

function setup_comp_graph(big_graph)
    non_zeros = ["AA"]
    for (vertex, (flow, to)) in big_graph
        if flow > 0
            push!(non_zeros, vertex)
        end
    end
    small_graph = Dict{String, Tuple{Dict{String, Int}, Int}}()
    for vertex1 in non_zeros
        sub_graph = Dict{String, Int}()
        for vertex2 in non_zeros
            if vertex2 != vertex1
                distance = findshortest_distance(big_graph, vertex1, vertex2)
                sub_graph[vertex2] = distance
            end
        end
        small_graph[vertex1] = (sub_graph, big_graph[vertex1][1])
    end
    return small_graph
end

function get_max_path(cur_path, cur_value, cur_time, cur_loc, graph)
   max = cur_value
   if cur_time > 30
       iter = Dict()
   else
      iter = graph[cur_loc][1]
   end
   for (neigh, length) in iter
        if !(neigh in cur_path)
            new_time = cur_time + length + 1
            if new_time <= 30
                new_value = cur_value + graph[neigh][2] * (30 - new_time)
            else
                new_value = cur_value
            end
            new_path = copy(cur_path)
            push!(new_path, neigh)
            new_max = get_max_path(new_path, new_value, new_time, neigh, graph)
            if new_max > max
                max = new_max
            end
        end
    end
    return max
end

function get_max_path_el_better(open, value, my_time_left, el_time_left, my_loc, el_loc, graph, cache)
    # Open is a dictionaryof all openlocations with the time at which they were opened
    if my_time_left <= 0 || el_time_left <= 0
        return value
    end
    if (keys(open), my_loc, el_loc, my_time_left, el_time_left) in keys(cache)
        cvalue = cache[(keys(open), my_loc, el_loc, my_time_left, el_time_left)]
        # Eerder deze dingen geopend op betere manier, stop
        if cvalue >= value
            return 0
        end
    end
#     Assume the my_loc & el_loc have just beed added to open, find which next non-open to move to
    max_val = value
    act_mntl = my_time_left
    act_entl = el_time_left
    act_mneigh = my_loc
    act_eneigh = el_loc
    act_open = open
    for (myneigh, mylength) in graph[my_loc][1]
        if !(myneigh in keys(open))
            for (elneigh, ellength) in graph[el_loc][1]
                if !(elneigh in keys(open)) && myneigh != elneigh
                    my_new_time_left = my_time_left - mylength - 1
                    el_new_time_left = el_time_left - ellength - 1
                    # Case where I open myneigh and el opens elneigh
                    # add neighs to open, increase time and recurse
                    new_open = copy(open)
                    if my_new_time_left > 0
                        new_open[myneigh] = my_new_time_left
                    end
                    if el_new_time_left > 0
                        new_open[elneigh] = el_new_time_left
                    end
                    new_val = calc_val(new_open, graph)
                    final_value = get_max_path_el_better(new_open, new_val, my_new_time_left, el_new_time_left, myneigh, elneigh, graph, cache)
                    if final_value > max_val
                        max_val = final_value
                        act_mntl = my_new_time_left
                        act_entl = el_new_time_left
                        act_mneigh = myneigh
                        act_eneigh = elneigh
                        act_open = new_open
                    end
                end
            end
        end
    end
    cache[(keys(act_open), act_mneigh, act_eneigh, max_val, act_entl)] = act_mntl
    return max_val
end

function get_max_path_best(opened, value, time_left, loc, next_players, graph, cache)
    max = value
    iter = graph[loc][1]
    for (neigh, length) in keys(iter)
        if !(neigh in keys(opened))
            new_time = time_left - length - 1
            if time_left >= 0
                new_value = value + graph[neigh][2] * time_left
            else
                new_value = value
            end
            new_path = copy(opened)
            push!(new_path, neigh)
            new_max = get_max_path(new_path, new_value, new_time, neigh, graph)
            if new_max > max
                max = new_max
            end
        end
    end
    cache[(opened, time_left, loc)] = value
    return max
end
function calc_val(open, graph)
    tot = 0
   for (node, time_left) in open
       tot += graph[node][2] * time_left
   end
   return tot
end

function solve_problem(graph)
    cache = Dict()
    path = Dict("AA" => 0)
#     sol = get_max_path_el_better(path, 0, 26, 26, "AA", "AA", graph, cache)
    sol = get_max_path_best(path, 0, 30, "AA", 0, graph, cache)
    return sol
end

function main()
    data = parse_data()
    big_graph = setup_big_graph(data)
    small_graph = setup_comp_graph(big_graph)
    sol = solve_problem(small_graph)
    println(sol)
end

main()
@time main()