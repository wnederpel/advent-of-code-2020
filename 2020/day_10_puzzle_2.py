def get_ways_to_reach(number, numbers, known_numbers, value_of_known_number):
    if number not in numbers:
        return 0
    elif number == 0:
        return 1
    elif number in known_numbers:
        return value_of_known_number[known_numbers.index(number)]
    ways = 0
    for i in range(number - 3, number):
        ways += get_ways_to_reach(i, numbers)
    return ways

with open('data_day_10.txt') as f:
    lines = f.read().splitlines()
f.close()
numbers = [0]
for number in lines:
    numbers.append(eval(number))
numbers = sorted(numbers)
numbers.append(numbers[-1] + 3)

print(numbers)

value_of_numbers = [0] * len(numbers)
for index in range(0, len(numbers)):
    number = numbers[index]
    if number == 0:
        value_of_numbers[index] = 1
    else:
        ways_to_reach_index = 0
        for num in range(number - 3, number):
            if num in numbers:
                ways_to_reach_index += value_of_numbers[numbers.index(num)]
        value_of_numbers[index] = ways_to_reach_index

print(value_of_numbers)
