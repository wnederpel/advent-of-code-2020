def get_adj_state(row_index, col_index, current_seats, iterations):
    adj = []
    for x in (-1, 0, 1):
        for y in (-1, 0, 1):
            if x == y == 0:
                continue
            else:
                for i in range(1, 2):
                    if 0 <= x*i + row_index < len(current_seats) and 0 <= y*i + col_index < len(current_seats[0]):
                        value = current_seats[x*i + row_index][y*i + col_index]
                        if value != ".":
                            adj.append(value)
                            break
                    # else:
                    #     break
                    # i += 1
    # if col_index == len(current_seats) and iterations < 4:
    #     print(row_index, col_index, adj)
    #     print(adj.count("#"))
    return adj
for i in range(1,2):
    print(i)

with open('data_day_11.txt') as f:
    seats = f.read().splitlines()
f.close()
print(len(seats), len(seats[0]))
stable_situation = False
become_empty = 4
become_occu = 0
iterations = 0
while not stable_situation:
    iterations += 1
    current_seats = seats
    # print(len(current_seats), len(current_seats[0]))

    next_seats = ['' for _ in range(len(current_seats))]
    for row in range(0, len(current_seats)):
        for col in range(0, len(current_seats[0])):
            # print("col = ", col)
            state = current_seats[row][col]
            adj = get_adj_state(row, col, current_seats, iterations)
            if state == "L" and "#" not in adj:
                next_seats[row] += "#"
            elif state == "#" and adj.count("#") >= 4:
                next_seats[row] += "L"
            else:
                next_seats[row] += state
    # if iterations < 4 :
    #     for line in next_seats:
    #         print(line)
    #     print("---------------")
    if next_seats == current_seats:
        stable_situation = True
    seats = next_seats

print(len(next_seats), len(next_seats[0]))

occu_final = 0
dots = 0
empties = 0
for row in range(0, len(next_seats)):
    for col in range(0, len(next_seats)):
        if next_seats[row][col] == "#":
            occu_final += 1
        elif next_seats[row][col] == ".":
            dots += 1
        elif next_seats[row][col] == "L":
            empties += 1

print(occu_final)
print(dots + empties + occu_final)
print(''.join(next_seats).count('#'))



