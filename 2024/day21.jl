using DataStructures

function parse_data()
    data = String[]
    open(joinpath(dirname(@__FILE__), "data/day21.txt")) do f
        while !eof(f)
            line = readline(f)
            push!(data, line)
        end
    end
    return data
end

function push_distances_forward(prev_distances)
    new_distances = Dict{Tuple{Symbol,Symbol},Int}()

    # From activate
    new_distances[(:activate, :activate)] = prev_distances[(:activate, :activate)]
    new_distances[(:activate, :up)] =
        prev_distances[(:activate, :left)] + prev_distances[(:left, :activate)]
    new_distances[(:activate, :right)] =
        prev_distances[(:activate, :down)] + prev_distances[(:down, :activate)]
    down_via_right =
        prev_distances[(:activate, :down)] +
        prev_distances[(:down, :left)] +
        prev_distances[(:left, :activate)]
    down_via_up =
        prev_distances[(:activate, :left)] +
        prev_distances[(:left, :down)] +
        prev_distances[(:down, :activate)]
    new_distances[(:activate, :down)] = min(down_via_right, down_via_up)
    left_via_right =
        prev_distances[(:activate, :down)] +
        prev_distances[(:down, :left)] +
        prev_distances[(:left, :left)] +
        prev_distances[(:left, :activate)]
    left_via_up =
        prev_distances[(:activate, :left)] +
        prev_distances[(:left, :down)] +
        prev_distances[(:down, :left)] +
        prev_distances[(:left, :activate)]
    new_distances[(:activate, :left)] = min(left_via_right, left_via_up)

    # from up
    new_distances[(:up, :activate)] =
        prev_distances[(:activate, :right)] + prev_distances[(:right, :activate)]
    new_distances[(:up, :down)] =
        prev_distances[(:activate, :down)] + prev_distances[(:down, :activate)]
    # println("up to right via right then down costs $((prev_distances[(:activate, :right)],
    #     prev_distances[(:right, :down)],
    #     prev_distances[(:down, :activate)]))")
    # println("up to right via down then right costs sum $((prev_distances[(:activate, :down)],
    # prev_distances[(:down, :right)],
    # prev_distances[(:right, :activate)]))")
    new_distances[(:up, :right)] = min(
        prev_distances[(:activate, :right)] +
        prev_distances[(:right, :down)] +
        prev_distances[(:down, :activate)],
        prev_distances[(:activate, :down)] +
        prev_distances[(:down, :right)] +
        prev_distances[(:right, :activate)],
    )
    new_distances[(:up, :up)] = prev_distances[(:activate, :activate)]
    new_distances[(:up, :left)] =
        prev_distances[(:activate, :down)] +
        prev_distances[(:down, :left)] +
        prev_distances[(:left, :activate)]

    # from right
    new_distances[(:right, :right)] = prev_distances[(:activate, :activate)]
    new_distances[(:right, :down)] =
        prev_distances[(:activate, :left)] + prev_distances[(:left, :activate)]
    new_distances[(:right, :left)] =
        prev_distances[(:activate, :left)] +
        prev_distances[(:left, :left)] +
        prev_distances[(:left, :activate)]
    new_distances[(:right, :activate)] =
        prev_distances[(:activate, :up)] + prev_distances[(:up, :activate)]
    new_distances[(:right, :up)] = min(
        prev_distances[(:activate, :up)] +
        prev_distances[(:up, :left)] +
        prev_distances[(:left, :activate)],
        prev_distances[(:activate, :left)] +
        prev_distances[(:left, :up)] +
        prev_distances[(:up, :activate)],
    )

    # from down
    new_distances[(:down, :down)] = prev_distances[(:activate, :activate)]
    new_distances[(:down, :right)] =
        prev_distances[(:activate, :right)] + prev_distances[(:right, :activate)]
    new_distances[(:down, :left)] =
        prev_distances[(:activate, :left)] + prev_distances[(:left, :activate)]
    new_distances[(:down, :up)] =
        prev_distances[(:activate, :up)] + prev_distances[(:up, :activate)]
    new_distances[(:down, :activate)] = min(
        prev_distances[(:activate, :up)] +
        prev_distances[(:up, :right)] +
        prev_distances[(:right, :activate)],
        prev_distances[(:activate, :right)] +
        prev_distances[(:right, :up)] +
        prev_distances[(:up, :activate)],
    )

    # from left
    new_distances[(:left, :left)] = prev_distances[(:activate, :activate)]
    new_distances[(:left, :right)] =
        prev_distances[(:activate, :right)] +
        prev_distances[(:right, :right)] +
        prev_distances[(:right, :activate)]
    new_distances[(:left, :down)] =
        prev_distances[(:activate, :right)] + prev_distances[(:right, :activate)]
    new_distances[(:left, :up)] =
        prev_distances[(:activate, :right)] +
        prev_distances[(:right, :up)] +
        prev_distances[(:up, :activate)]
    new_distances[(:left, :activate)] = min(
        prev_distances[(:activate, :right)] +
        prev_distances[(:right, :up)] +
        prev_distances[(:up, :right)] +
        prev_distances[(:right, :activate)],
        prev_distances[(:activate, :right)] +
        prev_distances[(:right, :right)] +
        prev_distances[(:right, :up)] +
        prev_distances[(:up, :activate)],
    )

    return new_distances
end

function solve_problem(data)
    ret = 0
    distances = DefaultDict{Tuple{Symbol,Symbol},Int}(1)
    for _ in 1:25
        distances = push_distances_forward(distances)
    end
    # distances = push_distances_forward(distances)
    # for (key, val) in distances
    #     from, to = key
    #     println("from $from to $to costs $val")
    # end

    for goal in data
        # Do some kind of dijkstra to find shortest path to the goal
        # We continually build up a final code string 
        # If the goal string does not start with the build code string the node is invallid
        pq = PriorityQueue{Tuple{Int,Symbol,String},Int}()
        dist = Dict{Tuple{Int,Symbol,String},Int}()
        # State (node) consist of num pointer, r1 pointer, r2 pointer
        start = (-1, :activate, "")
        pq[start] = 0
        dist[start] = 0

        actions = Symbol[:down, :up, :right, :left, :activate]
        min_d = 0
        while !isempty(pq)
            state = dequeue!(pq)
            d = dist[state]
            if state[end] == goal
                min_d = d
                # println(min_d)
                break
            end
            for action in actions
                prev_action = state[2]
                new_state, valid = do_actions(state, action)

                if valid && startswith(goal, new_state[end])
                    # The first time we find this is always the shortest
                    new_dist = d + distances[(prev_action, action)]
                    if !(new_state in keys(dist)) || new_dist < dist[new_state]
                        dist[new_state] = new_dist
                        pq[new_state] = new_dist
                    end
                end
            end
        end
        ret += min_d * parse(Int, goal[begin:(end - 1)])
    end
    return ret
end

function do_actions(state, action)
    num, _, build_code = state

    # Do my action on num pad
    num, valid = move_numpad(num, action)
    if !valid
        return (:err, :err, :err, ""), false
    end
    if action == :activate
        if num == -1
            build_code *= 'A'
        else
            build_code *= string(num)
        end
    end
    return (num, action, build_code), true
end

function move_numpad(cur, action)
    if action == :activate
        return cur, true
    end
    # First the invalid moves
    if cur >= 1
        if cur >= 7 && action == :up
            return cur, false
        elseif cur % 3 == 0 && action == :right
            return cur, false
        elseif cur % 3 == 1 && action == :left
            return cur, false
        elseif cur == 1 && action == :down
            return cur, false
        end
    elseif cur == 0
        if action == :left || action == :down
            return cur, false
        end
    elseif cur == -1
        # cur == a
        if action == :right || action == :down
            return cur, false
        end
    end

    # Now the valid moves
    if cur >= 1
        if action == :up
            return cur + 3, true
        elseif action == :right
            return cur + 1, true
        elseif action == :left
            return cur - 1, true
        elseif action == :down
            if cur == 2
                return 0, true
            elseif cur == 3
                return -1, true
            else
                return cur - 3, true
            end
        else
            error("should not happen, action $(action) at cur $cur")
        end
    elseif cur == 0
        if action == :up
            return 2, true
        elseif action == :right
            return -1, true
        else
            error("should not happen, action $(action) at cur $cur")
        end
    elseif cur == -1
        if action == :up
            return 3, true
        elseif action == :left
            return 0, true
        else
            error("should not happen, action $(action) at cur $cur")
        end
    end
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
    @btime solve_problem($data) seconds = 1
end

main()

# function move_keypad(cur, action)
#     if action == :activate
#         return cur, true
#     end
#     if cur == :activate
#         if action == :right || action == :up
#             return cur, false
#         elseif action == :left
#             return :up, true
#         elseif action == :down
#             return :right, true
#         else
#             error("should not happen, action $(action) at cur $cur")
#         end
#     elseif cur == :up
#         if action == :left || action == :up
#             return cur, false
#         elseif action == :right
#             return :activate, true
#         elseif action == :down
#             return :down, true
#         else
#             error("should not happen, action $(action) at cur $cur")
#         end
#     elseif cur == :left
#         if action == :right
#             return :down, true
#         else
#             return cur, false
#         end
#     elseif cur == :down
#         if action == :down
#             return cur, false
#         elseif action == :right
#             return :right, true
#         elseif action == :up
#             return :up, true
#         elseif action == :left
#             return :left, true
#         else
#             error("should not happen, action $(action) at cur $cur")
#         end
#     elseif cur == :right
#         if action == :right || action == :down
#             return cur, false
#         elseif action == :up
#             return :activate, true
#         elseif action == :left
#             return :down, true
#         else
#             error("should not happen, action $(action) at cur $cur")
#         end
#     else
#         error("should not happen, action $(action) at cur $cur")
#     end
# end
