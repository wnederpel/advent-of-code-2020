def rotate_180(index, info):
    cur_right = info['rights'][index]
    cur_left = info['lefts'][index]
    cur_top = info['tops'][index]
    cur_bottom = info['bottoms'][index]
    info['rights'][index] = cur_left
    info['lefts'][index] = cur_right
    info['tops'][index] = cur_bottom
    info['bottoms'][index] = cur_top
    return info


def rotate_90_right(index, info):
    cur_right = info['rights'][index]
    cur_left = info['lefts'][index]
    cur_top = info['tops'][index]
    cur_bottom = info['bottoms'][index]
    info['rights'][index] = cur_top
    info['lefts'][index] = cur_bottom
    info['tops'][index] = cur_left
    info['bottoms'][index] = cur_right
    return info


def rotate_90_left(index, info):
    cur_right = info['rights'][index]
    cur_left = info['lefts'][index]
    cur_top = info['tops'][index]
    cur_bottom = info['bottoms'][index]
    info['rights'][index] = cur_bottom
    info['lefts'][index] = cur_top
    info['tops'][index] = cur_right
    info['bottoms'][index] = cur_left
    return info


def rotate(index, diff, info):
    if diff == 0:
        info = rotate_180(index, info)
    elif diff == 1:
        info = rotate_90_right(index, info)
    elif diff == 3:
        info = rotate_90_left(index, info)
    else:
        return info
    return info

def flip(index, info):
    cur_right = info['rights'][index]
    cur_left = info['lefts'][index]
    cur_top = info['tops'][index]
    cur_bottom = info['bottoms'][index]
    info['rights'][index] = cur_left[::-1]
    info['lefts'][index] = cur_right[::-1]
    info['tops'][index] = cur_top[::-1]
    info['bottoms'][index] = cur_bottom[::-1]
    return info


lines = open('data_day_20.txt').read().splitlines()

images = [[]]
info = {'tops': [], 'rights': [], 'bottoms': [], 'lefts': []}
global ids
ids = []

image_index = 0
for line in lines:
    if line == '':
        image_index += 1
        images.append([])
    else:
        images[image_index].append(line)



for image in images:
    ids.append(image[0][5:9])
    length = len(image[1])
    info['tops'].append(image[1])
    right_str = ''
    left_str = ''
    bot_str = ''
    for i in range(length):
        right_str += image[i + 1][-1]
        left_str += image[-1 -i][0]
        bot_str += image[-1][-1 -i]
    info['bottoms'].append(bot_str)
    info['rights'].append(right_str)
    info['lefts'].append(left_str)


nr_images = len(info['tops'])
locations = ['_'] * nr_images
known_ids = ['_'] * nr_images
locations[0] = [1, 0]
known_ids[0] = ids[0]
known_locations = [0]
unknown_locations = list(range(1, nr_images))

while locations.count('_') != 0:
    for known_loc_i in known_locations:
        for unknown_list_index, unknown_loc_i in enumerate(unknown_locations):
            if unknown_loc_i == known_loc_i:
                continue
            for val_key_i, validation_key in enumerate(info):
                for check_key_i, check_key in enumerate(info):
                    if info[validation_key][known_loc_i] == info[check_key][unknown_loc_i][::-1]:
                        if validation_key == 'rights':
                            new_location = [locations[known_loc_i][0] + 1, locations[known_loc_i][1]]
                        if validation_key == 'lefts':
                            new_location = [locations[known_loc_i][0] - 1, locations[known_loc_i][1]]
                        if validation_key == 'tops':
                            new_location = [locations[known_loc_i][0], locations[known_loc_i][1] + 1]
                        if validation_key == 'bottoms':
                            new_location = [locations[known_loc_i][0], locations[known_loc_i][1] - 1]
                        info = rotate(unknown_loc_i, abs(val_key_i - check_key_i), info)
                        if new_location not in locations:
                            locations[unknown_loc_i] = new_location
                            known_ids[unknown_loc_i] = ids[unknown_loc_i]

                            del unknown_locations[unknown_list_index]
                            known_locations.append(unknown_loc_i)

                    if info[validation_key][known_loc_i] == info[check_key][unknown_loc_i]:
                        info = flip(unknown_loc_i, info)


x_min = 0
x_max = 0
y_min = 0
y_max = 0
for loc in locations:
    if loc[0] > x_max:
        x_max = loc[0]
    if loc[0] < x_min:
        x_min = loc[0]
    if loc[1] > y_max:
        y_max = loc[1]
    if loc[1] < y_min:
        y_min = loc[1]

### p one above, now continue with p2, make image

for image in images:
    


