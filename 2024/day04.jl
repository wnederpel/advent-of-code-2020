using DataStructures

function parse_data()
    data = []
    open(joinpath(dirname(@__FILE__), "data/day04.txt")) do f
        while !eof(f)
            line = collect(readline(f))
            push!(data, line)
        end
    end
    return data
end

function xmas_starts_at(i, j, data)
    if xmas_in_dir_up_right(i, j, data) && xmas_in_dir_up_left(i, j, data)
        return 1
    end
    return 0
end

function xmas_in_dir_up_left(i, j, data)
    if !(2 <= i <= length(data) - 1 && 2 <= j <= length(data[i]) - 1)
        return false
    end
    right = [i + 1, j + 1]
    left = [i - 1, j - 1]
    if data[left[1]][left[2]] == 'M'
        if data[right[1]][right[2]] == 'S'
            return true
        end
    end
    if data[left[1]][left[2]] == 'S'
        if data[right[1]][right[2]] == 'M'
            return true
        end
    end
    return false
end

function xmas_in_dir_up_right(i, j, data)
    if !(2 <= i <= length(data) - 1 && 2 <= j <= length(data[i]) - 1)
        return false
    end
    right = [i + 1, j - 1]
    left = [i - 1, j + 1]
    if data[left[1]][left[2]] == 'M'
        if data[right[1]][right[2]] == 'S'
            return true
        end
    end
    if data[left[1]][left[2]] == 'S'
        if data[right[1]][right[2]] == 'M'
            return true
        end
    end
    return false
end

function solve_problem(data)
    sol = 0
    for (i, line) in enumerate(data)
        for (j, char) in enumerate(line)
            if char == 'A'
                # Try to find an XMAS here
                sol += xmas_starts_at(i, j, data)
            end
        end
    end
    return sol
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
    @btime solve_problem($data) seconds = 1
end

main()