using DataStructures
using StaticArrays
using Profile
using PProf
using Bumper
using LinearAlgebra

function parse_data()
    machines = Vector{Tuple{Matrix{Int},Vector{Int}}}()
    # machines = Vector{Tuple{Matrix{Float64},Vector{Float64}}}()
    matrix = zeros(2, 2)
    line = ""
    prev_line = []
    open(joinpath(dirname(@__FILE__), "data/day13.txt")) do f
        while !eof(f)
            line = readline(f)
            if line == ""
                push!(machines, (copy(matrix), SizedVector{2,Float64}(prev_line)))
                continue
            elseif occursin("A:", line)
                line = split(line, "A: ")[2]
                line = split(line, ", ")
                line = [line[1][3:end], line[2][3:end]]
                line = parse.(Int, line)
            elseif occursin("B:", line)
                line = split(line, "B: ")[2]
                line = split(line, ", ")
                line = [line[1][3:end], line[2][3:end]]
                line = parse.(Int, line)
                matrix = [prev_line line]
                # matrix = [prev_line line]
            elseif occursin("Prize", line)
                line = split(line, "e: ")[2]
                line = split(line, ", ")
                line = [line[1][3:end], line[2][3:end]]
                line = parse.(Int, line) .+ 10000000000000
            end
            prev_line = line
        end
    end
    push!(machines, (matrix, line))
    return machines
end

function solve_problem(machines)
    ret = 0
    for machine in machines
        matrix, goal = machine
        # have to solve v1*a + v2*b = goal
        # goal \= matrix
        # vals = goal
        # mul!(vals,inv(matrix), goal)

        # vals = inv(matrix) * goal
        det = matrix[1, 1] * matrix[2, 2] - matrix[1, 2] * matrix[2, 1]
        # # det_inv = 1 / det

        intermediate1 = (goal[1] * matrix[2, 2] - goal[2] * matrix[1, 2])
        intermediate2 = (-goal[1] * matrix[2, 1] + goal[2] * matrix[1, 1])
        if intermediate2 % det != 0 || intermediate1 % det != 0
            continue
        end
        val1 = intermediate1 ÷ det
        val2 = intermediate2 ÷ det

        # println()
        # println(matrix)
        # println(goal)
        # println((val1, val2))

        # ldiv!(vals, lu!(matrix), goal)
        # vals = goal

        if val1 >= 0 && val2 >= 0
            ret += val1 * 3 + val2
        end
    end
    return ret
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)

    @btime solve_problem($data) seconds = 5

    # Profile.Allocs.clear()
    # Profile.Allocs.@profile sample_rate = 1 solve_problem(data)

    # PProf.Allocs.pprof()

    Profile.clear()
    Profile.@profile for _ in 1:1_000_000
        solve_problem(data)
    end

    PProf.pprof()
end

main()