def get_bags_directly_in(bag, d):
    if bag[0] in 'abcdefghijklmnopqrstuvwxyz':
        return d[bag]
    elif d[bag[2:]] == ['no other bag']:
        return []
    else:
        return d[bag[2:]]

def number_of_bags_in_bag(bag, d, sum):
    print(bag[2:])
    if d[bag[2:]] == ['no other bag']:
        sum += 1
        return sum
    else:
        for contains in d[bag[2:]]:
            print(contains[2:])
            if d[contains[2:]] == ['no other bag']:
                sum += eval(contains[0])
            else:
                sum += number_of_bags_in_bag(contains, d, 0) * eval(contains[0]) + eval(contains[0])
        print(sum)
        return sum




with open('data_day_7') as f:
    lines = f.read().splitlines()

d = {}
for line in lines:
    main, bags_contained = line.split('s contain ')
    value = []
    for contains in bags_contained.split(', '):
        if contains.endswith('s.'):
            value.append(contains[:-2])
        elif contains.endswith('.') or contains.endswith('s'):
            value.append(contains[:-1])
        else:
            value.append(contains)
    d[main] = value

print(d)
bags_to_check = ['shiny gold bag']
bags_contained = []
total_bags = 1
while bags_to_check != []:
    current_bag = bags_to_check[0]
    directly_contained = get_bags_directly_in(current_bag, d)
    for bag in directly_contained:
        if bag not in bags_contained:
            bags_contained.append(bag)
        if bag not in bags_to_check:
            bags_to_check.append(bag)
    del bags_to_check[0]
print(number_of_bags_in_bag("  shiny gold bag", d, 0))