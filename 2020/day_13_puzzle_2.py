from math import *

data = open('data_day_13.txt').read().splitlines()

arrival = eval(data[0])
ids = data[1].split(',')

start = eval(ids[0])
i = floor(100000000000000 % start)

while True:
    given = start * i
    delay = 0
    all_times_good = True
    for id in ids:
        if id != 'x':
            id = eval(id)
            if (given + delay) % id != 0:
                all_times_good = False
        delay += 1
    if all_times_good:
        print(given)
        break
    i += 1

