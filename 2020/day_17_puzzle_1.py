def get_neigh(x, y, z, w, grid):
    checks = [-1, 0, 1]
    neigh = []
    for w_extra in checks:
        for z_extra in checks:
            for x_extra in checks:
                for y_extra in checks:
                    z_check = z + z_extra
                    x_check = x + x_extra
                    y_check = y + y_extra
                    w_check = w + w_extra
                    if 0 <= w_check < len(grid):
                        if 0 <= z_check < len(grid[w_check]):
                            if 0 <= y_check < len(grid[w_check][z_check]):
                                if 0 <= x_check < len(grid[w_check][z_check][y_check]):
                                    if not (x_extra == 0 and y_extra == 0 and z_extra == 0 and w_extra == 0):
                                        neigh.append(grid[w_check][z_check][y_check][x_check])
    return neigh

start_lines = open('data_day_17.txt').read().splitlines()

start_size = 8
grid = []
size = start_size + 12
for _ in range(13):
    layer_layer  = []
    for _ in range(13):
        layer = []
        for _ in range(size):
            layer.append(['.'] * size)
        layer_layer.append(layer)
    grid.append(layer_layer)

print(len(grid))
print(len(grid[0]))
print(len(grid[0][0]))
print(len(grid[0][0][0]))

for y_i, start_line in enumerate(start_lines):
    for x_i, value in enumerate(start_line):
        trans_x, trans_y, middle = x_i + 6, y_i + 6, 6
        # print(trans_x, trans_y, value)
        grid[middle][middle][trans_y][trans_x] = value
# print(grid[6][6])
for iteration in range(0, 6):
    new_grid = []
    for _ in range(13):
        layer_layer  = []
        for _ in range(13):
            layer = []
            for _ in range(size):
                layer.append(['.'] * size)
            layer_layer.append(layer)
        new_grid.append(layer_layer)

    for w in range(len(grid)):
        for z in range(len(grid[w])):
            for y in range(len(grid[w][z])):
                for x in range(len(grid[w][z][y])):
                    neigh = get_neigh(x, y, z, w, grid)
                    num_hash = neigh.count('#')
                    # print(x, y, z, len(neigh))
                    if grid[w][z][y][x] == '#':
                        if 2 <= num_hash <= 3:
                            new_grid[w][z][y][x] = '#'
                        else:
                            new_grid[w][z][y][x] = '.'
                    else:
                        if num_hash == 3:
                            new_grid[w][z][y][x] = '#'
                        else:
                            new_grid[w][z][y][x] = '.'
    grid = new_grid
    print(iteration)
# print()
# print(grid[5][5])
ans = 0
for w in range(len(grid)):
    for z in range(len(grid[w])):
        for y in range(len(grid[w][z])):
            for x in range(len(grid[w][z][y])):
                if grid[w][z][y][x] == '#':
                    ans += 1
print(ans)


