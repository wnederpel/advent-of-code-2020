import re
f = open('data_day_4', 'r')

data = ['']
pw_index = 0
lines = f.readlines()
for index in range(0, len(lines)):
    if lines[index] != '\n':
        if not lines[index].endswith('\n'):
            data[pw_index] += lines[index]
        else:
            data[pw_index] += lines[index][:-1] + ' '
    else:
        data[pw_index] = data[pw_index][:-1]
        data.append('')
        pw_index += 1

req_fields = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']
op_fields = ['cid']
valid = 0
invalid = 0
for pw in data:
    items = pw.split(' ')
    num_checks = 0
    for item in items:
        for check in req_fields:
            key, value = item.split(':')
            if key == check:
                if check == 'byr':
                    int = re.compile(r'^[0-9]+$')
                    if len(value) == 4:
                        if bool(int.match(value)):
                            if 1920 <= eval(value) <= 2002:
                                num_checks += 1
                            else:
                                print(item)
                        else:
                            print(item)
                    else:
                        print(item)
                elif check == 'iyr':
                    int = re.compile(r'^[0-9]+$')
                    if len(value) == 4:
                        if bool(int.match(value)):
                            if 2010 <= eval(value) <= 2020:
                                num_checks += 1
                            else:
                                print(item)
                        else:
                            print(item)
                    else:
                        print(item)
                elif check == 'eyr':
                    int = re.compile(r'^[0-9]+$')
                    if len(value) == 4:
                        if bool(int.match(value)):
                            if 2020 <= eval(value) <= 2030:
                                num_checks += 1
                            else:
                                print(item)
                        else:
                            print(item)
                    else:
                        print(item)

                elif check == 'hgt':
                    if value.endswith('cm'):
                        if 150 <= eval(value[:-2]) <= 193:
                            num_checks += 1
                        else:
                            print(item)
                    elif value.endswith('in'):
                        if 59 <= eval(value[:-2]) <= 76:
                            num_checks += 1
                        else:
                            print(item)
                    else:
                        print(item)


                elif check == "hcl":
                    pid = re.compile(r'^#[0-9a-f]{6}$')
                    if pid.match(value):
                        num_checks += 1

                elif check == 'ecl':
                    if value in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']:
                        num_checks += 1
                    else:
                        print(item)


                elif check == 'pid':
                    pid = re.compile(r'^[0-9]{9}$')
                    if len(value) == 9:
                        if bool(pid.match(value)):
                            num_checks += 1
                    else:
                        print(item)

    if num_checks == len(req_fields):
            valid += 1
    else:
        invalid += 1
        print(pw)

print(valid)
print(invalid)