with open('data_day_7') as f:
    lines = f.read().splitlines()

def get_bags_which_can_directly_contain(bag_to_check, d):
    can_contain_directly = []
    for container in d.keys():
        for contained in d[container]:
            if contained[2:] == bag_to_check:
                can_contain_directly.append(container)
    return can_contain_directly

d = {}
for line in lines:
    main, bags_contained = line.split('s contain ')
    value = []
    for contains in bags_contained.split(', '):
        if contains.endswith('s.'):
            value.append(contains[:-2])
        elif contains.endswith('.') or contains.endswith('s'):
            value.append(contains[:-1])
        else:
            value.append(contains)
    d[main] = value
print(d)
can_contain_gold = []
bags_to_check = ['shiny gold bag']
while bags_to_check != []:
    bag_to_check = bags_to_check[0]
    can_contain_directly = get_bags_which_can_directly_contain(bag_to_check, d)
    del bags_to_check[0]
    for bag in can_contain_directly:
        if bag not in can_contain_gold:
            can_contain_gold.append(bag)
        if bag not in bags_to_check:
            bags_to_check.append(bag)

print(len(can_contain_gold))

