function parse_data()
    data = [[],[],[],[],[],[],[],[],[]]
    instructions = []
    set_instructions = false
    open(joinpath(dirname(@__FILE__), "data/day5.txt")) do f
        while !eof(f)
            line = readline(f)
            if line == "" || line[2] == '1'
                set_instructions = true
                continue
            end
            if !set_instructions
                i = 2
                while i <= length(line)
                    if line[i] != ' '
                        push!(data[Int((i - 2) / 4 + 1)], line[i])
                    end
                    i += 4
                end
            else
                line = split(line, " ")
                push!(instructions, (parse(Int, line[2]), parse(Int, line[4]), parse(Int, line[6])))
            end
        end
    end
    return data, instructions
end

function solve_problem(data, instructions)
    for instruction in instructions
        number = instruction[1]
        from = instruction[2]
        to = instruction[3]
        for i = reverse(1:number)
            insert!(data[to],1, data[from][i])
        end
        deleteat!(data[from], 1:number)
    end
    println(data)
    for datum in data
        print(datum[1])
    end
    println()
end

function main()
    data, instructions = parse_data()
    sol = solve_problem(data, instructions)
end

main()