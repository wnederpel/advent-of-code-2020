function parse_data()
    data = []
    open(joinpath(dirname(@__FILE__), "data/day2.txt")) do f
        while !eof(f)
            line = readline(f)
            push!(data, split(line, " "))
        end
    end
    return data
end

abstract type Shape end
struct Rock     <: Shape end
struct Paper    <: Shape end
struct Scissors <: Shape end

abstract type Outcome end
struct Win <: Outcome end
struct Draw <: Outcome end
struct Loss <: Outcome end

# Scores for an outcome
score(::Type{Win}) = 6
score(::Type{Draw}) = 3
score(::Type{Loss}) = 0

# Scores for picking a shape
score(::Type{Rock}) = 1
score(::Type{Paper}) = 2
score(::Type{Scissors}) = 3

# Rules for what you pick for a certain outcome
score(::Type{Rock},     ::Type{Win}) = score(Paper)
score(::Type{Paper},    ::Type{Win}) = score(Scissors)
score(::Type{Scissors}, ::Type{Win}) = score(Rock)
score(::Type{T},        ::Type{Draw}) where {T <: Shape} = score(T)
score(::Type{Rock},     ::Type{Loss}) = score(Scissors)
score(::Type{Paper},    ::Type{Loss}) = score(Rock)
score(::Type{Scissors}, ::Type{Loss}) = score(Paper)

function get_shape(input)
    if input == "A"
        return Rock
    elseif input == "B"
        return Paper
    else
        return Scissors
    end
end

function get_outcome(input)
   if input == "X"
        return Loss
    elseif input == "Y"
        return Draw
    else
        return Win
    end
end

function solve_problem(data)
    sum = 0
    for datum = data
        other = get_shape(datum[1])
        self = get_outcome(datum[2])
        sum += score(other, self)
        sum += score(self)
    end
    println(sum)
end

function main()
    data = parse_data()
    solve_problem(data)
end

main()

