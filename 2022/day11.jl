using BenchmarkTools

function parse_data()
    data = []
    monkey0 = Monkey([57],
        x -> x * 13, 11, 3, 2, 0)
    monkey1 = Monkey([58, 93, 88, 81, 72, 73, 65],
        x -> x + 2, 7, 6, 7, 0)
    monkey2 = Monkey([65, 95],
        x -> x + 6, 13, 3, 5, 0)
    monkey3 = Monkey([58, 80, 81, 83],
        x -> x * x, 5, 4, 5, 0)
    monkey4 = Monkey([58, 89, 90, 96, 55],
        x -> x + 3, 3, 1, 7, 0)
    monkey5 = Monkey([66, 73, 87, 58, 62, 67],
        x -> x * 7, 17, 4, 1, 0)
    monkey6 = Monkey([85, 55, 89],
        x -> x + 4, 2, 2, 0, 0)
    monkey7 = Monkey([73, 80, 54, 94, 90, 52, 69, 58],
        x -> x + 7, 19, 6, 0, 0)
    return [monkey0, monkey1, monkey2, monkey3, monkey4, monkey5, monkey6, monkey7]
end

mutable struct Monkey
    items::Vector{Int}
    operation::Function
    test::Int
    iftrue::Int
    iffalse::Int
    tot_inspected::Int
end

function solve_problem(data)
    for round = 1:10000
        for monkey in data
            while !isempty(monkey.items)
                # inverstigate last item first, does not matter in current formulation.
                item = pop!(monkey.items)
                monkey.tot_inspected += 1
#                 new_item = floor(monkey.operation(item) / 3)
                new_item = monkey.operation(item) % (2 * 3 * 5 * 7 * 11 * 13 * 17 * 19)
                if new_item % monkey.test == 0
                    to_throw = monkey.iftrue
                else
                    to_throw = monkey.iffalse
                end
                push!(data[to_throw + 1].items, new_item)
            end
        end
    end
    for monkey in data
        println(monkey.tot_inspected)
    end
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
end

main()