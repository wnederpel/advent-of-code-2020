using BenchmarkTools
using DataStructures

function parse_data()
    data = [[]]
    mirror_index = 1
    open(joinpath(dirname(@__FILE__), "../data/day13.txt")) do f
        while !eof(f)
            line = readline(f)
            if line == ""
                mirror_index += 1
                push!(data, [])
            else
                push!(data[mirror_index], collect(line))
            end
        end
    end
    return data
end

function solve_hor(mirror)
    errs = Int.(zeros(length(mirror) - 1))
    sol = nothing
    counts = counter(mirror)
    for (i, line) in enumerate(mirror)
        if i == length(mirror)
            break
        end
        line_count = counts[line]
        # if line_count % 2 != 0
        #     continue
        # end
        # Other wise might be valid. 
        # Check if next line is equal to cur line and repeat for all previous lines 
        valid = true
        for j in 1:i
            index1 = reverse(1:i)[j]
            index2 = i + (1:i)[j]
            if index2 > length(mirror)
                break
            end
            # "testing i = $i" |> display
            if mirror[index1] != mirror[index2]
                for k in eachindex(mirror[1])
                    if mirror[index1][k] != mirror[index2][k]
                        errs[i] += 1
                    end
                end
                valid = false
            end
        end
        if valid 
            sol = i
        end
    end
    return sol, errs
end


function transpose(vert)
    new_vert = Vector{Vector{Char}}(undef, length(vert[1]))
    for i in eachindex(new_vert)
        new_vert[i] = Vector{Char}(undef, length(vert))
    end
    for (i, line) in enumerate(vert)
        for (j, char) in enumerate(line)
            # vert[i][j] should become new_vert[j][i]
            new_vert[j][i] = vert[i][j]
        end
    end
    return new_vert
end

function solve_vert(vert)
    hor = transpose(vert)
    return solve_hor(hor)
end

function solve_problem(data)
    sol = 0
    for mirror in data
        _, errs = solve_hor(mirror)
        line = findfirst(x -> x == 1, errs)
        if line === nothing
            _, errs = solve_vert(mirror)
            line = findfirst(x -> x == 1, errs)
            sol += line
        else
            sol += line * 100
        end
    end
    return sol
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
end

main()
