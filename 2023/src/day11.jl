using BenchmarkTools
using Profile
using PProf

function parse_data()
    data = []
    gals = []
    empty_rows = []
    empty_cols = []
    i = 1
    open(joinpath(dirname(@__FILE__), "../data/day11.txt")) do f
        while !eof(f)
            line = readline(f)
            push!(data, line)
            for (j, char) in enumerate(line)
                if char == '#'
                    push!(gals, (i, j))
                end
            end
            if !('#' in line)
                push!(empty_rows, i)
            end
            i += 1
        end
    end
    for j in 1:length(data[1])
        empty_col = true
        for row in data
            if row[j] == '#'
                empty_col = false
                break
            end
        end
        if empty_col
            push!(empty_cols, j)
        end
    end
    return gals, data, empty_rows, empty_cols
end

@inbounds function solve_problem(data)
    gals, data, erows, ecols = data
    res = 0
    adds = 0
    added_dist = 1_000_000
    for i in eachindex(gals)
        for j in (i + 1):length(gals)
            # i, j is a pair of galaxies.
            gali = gals[i]
            galj = gals[j]
            # By our ordering, we always have that gali[1] <= galj[1]
            minx, maxx = gali[1], galj[1]

            if gali[2] < galj[2]
                miny, maxy = gali[2], galj[2]
            else
                miny, maxy = galj[2], gali[2]
            end
            diff = (maxx - minx, maxy - miny)
            dist = diff[1] + diff[2]
            # x is row, y is col
            for r in erows
                if r < minx
                    continue
                elseif r > maxx
                    break
                else
                    adds += 1
                end
            end
            for c in ecols
                if c < miny
                    continue
                elseif c > maxy
                    break
                else
                    adds += 1
                end
            end
            res += dist
        end
    end
    res += (added_dist - 1) * adds
    return res
end

function main()
    data = parse_data()
    @time sol = solve_problem(data)
    println(sol)

    Profile.clear()
    @profile (
        for i in 1:10
            solve_problem(data)
        end
    )
    pprof()

    b = @benchmarkable solve_problem($data)
    tune!(b)
    run(b)
end

main()
