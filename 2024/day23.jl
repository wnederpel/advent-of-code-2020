using DataStructures

function parse_data()
    data = String[]
    open(joinpath(dirname(@__FILE__), "data/day23.txt")) do f
        while !eof(f)
            line = readline(f)
            push!(data, line)
        end
    end
    return data
end

function solve_problem(data)
    node_2_conn = DefaultDict{String,Set{String}}(() -> Set())
    for edge in data
        n1, n2 = split(edge, '-')
        push!(node_2_conn[n1], n2)
        push!(node_2_conn[n2], n1)
    end

    for (node, conns) in node_2_conn
        # No way to be in a clique if you have only 1 connection
        if length(conns) == 1
            node_2_conn[node] = Set()
        end
    end
    # cliques = 0
    cliques = Set{Set{String}}()
    for (node, conns) in node_2_conn
        # if !startswith(node, "t")
        #     continue
        # end
        # For each pair of nodes in the connected nodes, check if they have eachother as conns
        for n1 in conns
            # check if n1 and node have 2 or more in common
            n1_conns = node_2_conn[n1]
            common_conns = intersect(conns, n1_conns)
            delete!(common_conns, n1)
            delete!(common_conns, node)
            for comm in common_conns
                push!(cliques, Set([node, n1, comm]))
            end
        end
    end
    for size in 4:20
        new_cliques = Set{Set{String}}()
        for clique in cliques
            # See if there is any thing that is a comm conn of all
            common_conns = Set()
            for node in clique
                common_conns = copy(node_2_conn[node])
                break
            end
            for node in clique
                intersect!(common_conns, node_2_conn[node])
            end
            for node in clique
                delete!(common_conns, node)
            end
            if !isempty(common_conns)
                for node in common_conns
                    new_clique = copy(clique)
                    push!(new_clique, node)
                    push!(new_cliques, new_clique)
                end
            end
        end
        if isempty(new_cliques)
            for clique in cliques
                println(join(sort!(collect(clique)), ","))
                println()
            end
            return size - 1
        end
        cliques = copy(new_cliques)
    end
    return -1
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
end

main()