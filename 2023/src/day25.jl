using BenchmarkTools
using Graphs, Plots, GraphRecipes
using DataStructures

function parse_data()
    data = DefaultDict(deepcopy([]))
    open(joinpath(dirname(@__FILE__), "../data/day25.txt")) do f
        while !eof(f)
            line = readline(f)
            start, edges = split(line, ": ")
            edges = split(edges, " ")
            data[start] = edges
        end
    end
    return data
end

function solve_problem(data)
    println(data)
    g = Graph()
    add_vertex!(g)
    for node in keys(data)

    end

    # g = wheel_graph(10)
    return g
end

function main()
    data = parse_data()
    g = solve_problem(data)
    graphplot(g, curves=false)
end

main()
