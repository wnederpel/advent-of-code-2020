using DataStructures

function parse_data()
    data = []
    open(joinpath(dirname(@__FILE__), "data/day03.txt")) do f
        while !eof(f)
            line = readline(f)
            push!(data, line)
        end
    end
    return data
end

function solve_problem(data)
    mul_regex = r"mul\(([0-9]+),([0-9]+)\)"
    do_regex = r"do\(\)"
    dont_regex = r"don't\(\)"
    sol = 0
    enabled = true
    for str in data
        mul_ranges = findall(mul_regex, str)
        do_starts = getindex.(findall(do_regex, str), 1)
        dont_starts = getindex.(findall(dont_regex, str), 1)
        for mul_range in mul_ranges
            start_range = mul_range[begin]
            for i in start_range:-1:1
                if i in do_starts
                    enabled = true
                    break
                elseif i in dont_starts
                    enabled = false
                    break
                end
            end
            if enabled
                m = match(mul_regex, str[mul_range])
                sol += parse(Int, m[1]) * parse(Int, m[2])
            end
        end
    end
    return sol
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
    @btime solve_problem($data) seconds = 1
end

main()