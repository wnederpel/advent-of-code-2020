using BenchmarkTools
using Revise

function parse_data()
    data = []
    open(joinpath(dirname(@__FILE__), "../data/day01.txt")) do f
        while !eof(f)
            line = readline(f)
            push!(data, line)
        end
    end
    return data
end

@inline function matches_spelled_out(index, line)
    numbers_val = [6, 1, 2, 5, 4, 9, 7, 8, 3]
    numbers = [
        "six", "one", "two", "five", "four", "nine", "seven", "eight", "three"
    ]
    numbers_len = [3, 3, 3, 4, 4, 4, 5, 5, 5]
    for i in eachindex(numbers)
        # @inbounds if startswith(line[index:end], numbers[i])
        #     return numbers_val[i]
        # end
        @inbounds if index + numbers_len[i] - 1 > length(line)
            break
        end
        @inbounds if view(line, index:(index + numbers_len[i] - 1)) ==
            numbers[i]
            return numbers_val[i]
        end
    end
    return -1
end

@inline function solve_problem(data)
    res = 0
    for line in data
        first_val = -1
        for (i, char) in enumerate(line)
            val = matches_spelled_out(i, line)
            if val != -1
                first_val = val
                break
            end
            if isdigit(char)
                first_val = parse(Int, char)
                break
            end
        end
        last_val = -1
        for (i, char) in enumerate(reverse(line))
            val = matches_spelled_out(length(line) - i + 1, line)
            if val != -1
                last_val = val
                break
            end
            if isdigit(char)
                last_val = parse(Int, char)
                break
            end
        end
        res += first_val * 10 + last_val
    end
    return res
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
    b = @benchmarkable parse_data()
    tune!(b)
    run(b)
    # b = @benchmarkable solve_problem($data)
    # tune!(b)
    # run(b)
end

main()
