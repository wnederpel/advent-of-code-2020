using BenchmarkTools
using Profile
using PProf

function parse_data()
    maps = []
    map = []
    seeds = nothing
    seeds_ranges = []
    open(joinpath(dirname(@__FILE__), "../data/day05.txt")) do f
        i = 1
        while !eof(f)
            line = readline(f)
            if startswith(line, "seeds: ")
                line = split(line, ": ")[2]
                seeds = parse.(Int, split(line, " "))
                for j in 1:Int(length(seeds) / 2)
                    push!(
                        seeds_ranges,
                        seeds[j * 2 - 1]:(seeds[2 * j - 1] + seeds[2 * j] - 1),
                    )
                end
                i += 1
                continue
            end
            if i == 2 || i == 3
                i += 1
                continue
            end
            if endswith(line, "map:")
                i += 1
                continue
            end
            if !isempty(line)
                push!(map, parse.(Int, split(line, " ")))
            else
                push!(maps, map)
                map = []
            end
        end
        push!(maps, map)
        map = []
    end
    return seeds_ranges, maps
end

function map_value(val, map)
    for part in map
        if part[2] <= val < part[2] + part[3]
            return part[1] + val - part[2]
        end
    end
    return val
end

@inbounds @inline function my_intersect(range, map)
    map_range = map[2]:(map[2] + map[3] - 1)
    return intersect(range, map_range)
end

@inbounds @inline function map_range(range, map)
    ranges_to_consider = [range]
    # Always start with one range, cut parts of when it intersects, the new intersected parts can be put in a return list and forgotten about.
    # Guess that each map will split the range, so the total of new ranges should be about length of num_maps
    new_ranges = []
    for part in map
        new_ranges_to_consider = []
        for r in ranges_to_consider
            # Look at the intersection if there is one put it in the return list, and cut it out of the ranges to consider
            intersect = my_intersect(r, part)

            if !isempty(intersect)
                if r[begin] < intersect[begin]
                    push!(
                        new_ranges_to_consider, r[begin]:(intersect[begin] - 1)
                    )
                end
                if r[end] > intersect[end]
                    push!(new_ranges_to_consider, (intersect[end] + 1):r[end])
                end

                push!(new_ranges, (part[1] - part[2]) .+ intersect)
            else
                push!(new_ranges_to_consider, r)
            end
        end
        ranges_to_consider = new_ranges_to_consider
    end
    return [new_ranges; ranges_to_consider]
end

function solve_problem(data)
    seeds_ranges, maps = data

    range = seeds_ranges
    for map in maps
        new_ranges = []
        for r in range
            push!(new_ranges, map_range(r, map)...)
        end
        range = new_ranges
    end
    # for r in range
    return minimum(getindex.(range, 1))
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
    # data = parse_data()
    # Profile.clear()
    # @profile (
    #     for i = 1:100
    #         solve_problem(data)
    #     end
    # )
    # pprof()
    b = @benchmarkable parse_data()
    tune!(b)
    run(b)
    # b = @benchmarkable solve_problem($data)
    # tune!(b)
    # run(b)
end

main()
