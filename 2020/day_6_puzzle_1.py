import re
f = open('data_day_6', 'r')

data = ['']
form_index = 0
lines = f.readlines()
for index in range(0, len(lines)):
    if lines[index] != '\n':
        if not lines[index].endswith('\n'):
            data[form_index] += lines[index]
        else:
            data[form_index] += lines[index][:-1] + ' '
    else:
        data[form_index] = data[form_index][:-1]
        data.append('')
        form_index += 1

alfabet = 'abcdefghijklmnopqrstuvwxyz'
sum = 0
for group in data:
    persons = group.split(' ')
    answers = []
    for person in persons:
        yes_q = ''
        for question in alfabet:
            if question in person:
                yes_q += question
        answers.append(yes_q)

    for question in alfabet:
        everyone = True
        for answer in answers:
            if question not in answer:
                everyone = False

        if everyone:
            sum += 1
print(sum)