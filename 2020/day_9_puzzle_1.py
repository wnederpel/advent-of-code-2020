with open('data_day_9.txt') as f:
    lines = f.read().splitlines()
f.close()

preamble = []
for index, number in enumerate(lines):
    number = eval(number)
    if index < 25:
        preamble.append(number)
    else:
        number_valid = False
        for number_1 in preamble:
            for number_2 in preamble:
                if number_1 != number_2:
                    if number_1 + number_2 == number:
                        number_valid = True
        if not number_valid:
            break
        del preamble[0]
        preamble.append(number)

print(number)
print(min([5779902, 5821115, 5841937, 14752667, 6932934, 9843037, 6240013, 6745010, 8585628, 8937860, 9076207, 7222000,
           8634163, 9316176, 9739942, 10442589, 10470490]) + max([5779902, 5821115, 5841937, 14752667, 6932934, 9843037,
                                                                  6240013, 6745010, 8585628, 8937860, 9076207, 7222000,
                                                                  8634163, 9316176, 9739942, 10442589, 10470490]
                                                                                                                                                                                                                ))

for start in range(0, len(lines)):
    for length in range(1, len(lines) - start):
        potential_numbers = lines[start:(start+length)]
        sum = 0
        mini = 0
        maxi = 0
        for num in potential_numbers:
            mini = min(mini, eval(num))
            maxi = max(maxi, eval(num))
            sum += eval(num)
        if sum == number:
            print(potential_numbers)
            if len(potential_numbers) != 1:
                print(mini + maxi)
                break
        elif sum > number:
            break

