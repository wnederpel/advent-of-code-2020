using BenchmarkTools

function parse_data()
    data = []
    open(joinpath(dirname(@__FILE__), "../data/day16.txt")) do f
        while !eof(f)
            line = readline(f)
            push!(data, line)
        end
    end
    return data
end

function advance_beam(beam, data, visited)
    x, y = beam[1], beam[2]
    dir = beam[3]
    if (x, y) in keys(visited) 
        if dir in visited[(x, y)]
            return []
        else
            push!(visited[(x, y)], dir)
        end    
    else
        visited[(x, y)] = [dir]
    end
    if dir == :right
        new_x, new_y = x, y + 1 
    elseif dir == :up 
        new_x, new_y = x - 1, y
    elseif dir == :down
        new_x, new_y = x + 1, y
    elseif dir == :left
        new_x, new_y = x, y - 1
    end
    if !(1 <= new_x <= length(data) && 1 <= new_y <= length(data[1]))
        return []
    end
    next = data[new_x][new_y]
    # check for splits
    if next == '.'
        # continue on untill no .
        return [[new_x, new_y, dir]]
    elseif next == '|' 
        if dir == :right || dir == :left
            return [[new_x, new_y, :up], [new_x, new_y, :down]]
        else
            return [[new_x, new_y, dir]]
        end
    elseif next == '-' 
        if dir == :up || dir == :down
            return [[new_x, new_y, :right], [new_x, new_y, :left]]
        else
            return [[new_x, new_y, dir]]
        end
    elseif next == '/'
        if dir == :right
            return [[new_x, new_y, :up]]
        elseif dir == :down
            return [[new_x, new_y, :left]]
        elseif dir == :up
            return [[new_x, new_y, :right]]
        elseif dir == :left
            return [[new_x, new_y, :down]]
        end
    elseif next == 'L'
        if dir == :left
            return [[new_x, new_y, :up]]
        elseif dir == :down
            return [[new_x, new_y, :right]]
        elseif dir == :up
            return [[new_x, new_y, :left]]
        elseif dir == :right
            return [[new_x, new_y, :down]]
        end
    else
        "oh oh" |> display
        "next spot is $new_x, $new_y, there is a $next there" |> display
        return nothing
    end
end

function show(data, visited)
    for (i, line) in enumerate(data)
        for (j, char) in enumerate(line)
            if (i, j) in keys(visited)
                print('#')
            else
                print(char)
            end
        end
        println()
    end
end

function solve_problem(data)
    max_heat = 0
    for x in [0, length(data) + 1]
        for y in eachindex(data[1])
            if x == 0
                beams = [[x, y, :down]]
            else 
                beams = [[x, y, :up]]
            end
            max_heat = max(solve_problem_from_start(data, beams), max_heat)
        end
        println("hey!")
    end
    for y in [0, length(data[1]) + 1]
        for x in eachindex(data)
            if y == 0
                beams = [[x, y, :right]]
            else 
                beams = [[x, y, :left]]
            end
            max_heat = max(solve_problem_from_start(data, beams), max_heat)
        end
        println("hey!")
    end
    return max_heat
end

function solve_problem_from_start(data, beams)
    # A beam is a vector with posx, poy, direction
    visited = Dict()
    while !isempty(beams)
        for (i, beam) in enumerate(beams)
            new_beams = advance_beam(beam, data, visited)
            if isempty(new_beams)
                deleteat!(beams, i)
                # beam is dead, delete index
            else
                beams[i] = new_beams[1]
                if length(new_beams) == 2
                    push!(beams, new_beams[2])
                end
            end
        end
    end
    return length(visited) - 1
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
end

main()
