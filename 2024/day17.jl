using DataStructures
using BenchmarkTools
using PProf
using Profile
using StaticArrays
using Bumper

function parse_data()
    instructions = Int[]
    registers = Dict{Char,Int}()
    open(joinpath(dirname(@__FILE__), "data/day17.txt")) do f
        while !eof(f)
            line = readline(f)
            if 'A' in line
                registers['A'] = parse(Int, split(line, ':')[2])
            elseif 'B' in line
                registers['B'] = parse(Int, split(line, ':')[2])
            elseif 'C' in line
                registers['C'] = parse(Int, split(line, ':')[2])
            elseif line != ""
                instructions = parse.(Int, split(split(line, ": ")[2], ','))
                break
            end
        end
    end
    return instructions, registers
end

function solve_problem(A)
    B, C = 0, 0
    first = true
    out = ""
    while A != 0 || first
        # println("A:   " * showbin(A))
        # Only A persists between loops, A and B are fully reset.

        B = A % 8       # Next tree bits of A 
        # println("B:   " * showbin(B))

        B = B ⊻ 1       # Flip last bit of B
        tmp = (A >> B)  # Shift A by up to 7 bits
        C = tmp % 8     # Next 3 bits of A after shifting
        # println("tmp: " * showbin(C))

        B = B ⊻ C ⊻ 4   # Flip the bits in C and the 4th in B
        # println("out = $B")

        out *= "$B,"
        A = A >> 3      # look at next 3 bits of A
        # println()
        first = false
    end
    println(out)

    return A, B, C
end

function to_res(B, tmp)
    # B = B ⊻ 1
    # B = B ⊻ tmp ⊻ 4⊻ 1
    return B ⊻ tmp ⊻ 5
end

function main(to_makes)
    @inbounds @no_escape begin

        # println("      B →")
        # print("tmp ↓ ")
        # for j in 0:7
        #     print("(" * showbin(j) * ",o) ")
        # end
        # println()
        # println()
        # B_tmp_map = Vector{Vector{Tuple{Int,Int,Int}}}([
        #     Tuple{Int,Int,Int}[],
        #     Tuple{Int,Int,Int}[],
        #     Tuple{Int,Int,Int}[],
        #     Tuple{Int,Int,Int}[],
        #     Tuple{Int,Int,Int}[],
        #     Tuple{Int,Int,Int}[],
        #     Tuple{Int,Int,Int}[],
        #     Tuple{Int,Int,Int}[],
        # ])
        # SizedVector{8,Tuple{Int,Int,Int}}
        B_tmp_map = @alloc(Tuple{Int,Int}, 64)
        # println(B_tmp_map)

        for i in 0:7
            # print(showbin(i) * "   ")
            for j in 0:7
                res = to_res(j, i)
                offset = j ⊻ 1
                B_tmp_map[res * 8 + (j + 1)] = (i, offset)
                # push!(B_tmp_map[res + 1], (j, i, offset))

                # print(showbin(res) * " ")
                # print("(" * showbin(res) * ",")
                # print(string(offset) * ") ")
            end
            # println()
        end
        # println(B_tmp_map)

        # goal = [0, 3, 5]
        # println("2,4,1,1,7,5,0,3,1,4,4,0,5,5,3,0,")
        # options = Stack{Tuple{Int,Int}}()

        # showmap(B_tmp_map)

        options = @alloc(Tuple{Int,Int}, 10)
        # showmap(B_tmp_map)
        # println(options)
        # options = Vector{Tuple{Int,Int}}(undef, 10)
        options_index = 1
        options[options_index] = (0b0, 1)
        options_index += 1
        best_num = typemax(Int)
        while options_index != 1
            options_index -= 1
            num_so_far, num_to_make_index = options[options_index]
            # println((num_so_far, num_to_make_index))
            if num_to_make_index > length(to_makes)
                # println("continue")
                if num_so_far < best_num
                    best_num = num_so_far
                end
                continue
            end
            num_to_make = to_makes[num_to_make_index]
            index_base = num_to_make * 8 + 1

            new_base_num = num_so_far
            new_base_num *= 2^3
            for B in 0:7
                tmp, offset = B_tmp_map[index_base + B]
                # if num_to_make == 7
                #     showmap(B_tmp_map)
                #     println(num_to_make)
                #     println((num_to_make + 1) * 8 + B + 1)
                #     println(tmp)
                # end

                num_pot = new_base_num + B
                # println((num_pot, num_to_make, offset, tmp))

                if (num_pot >>> offset) % 8 == tmp
                    # println("match at $options_index")
                    options[options_index] = (num_pot, num_to_make_index + 1)
                    options_index += 1
                    # println(options_index)
                    # if options_index > 100
                    #     error("")
                    # end
                end
            end
        end
    end
    return best_num

    #     num = 0b0
    #     options = Int[0b0]
    #     for num_to_make in to_makes
    #         # ways_to_make = B_tmp_map[(num_to_make + 1) * 8 .+ (1:8)]
    #         # found = false
    #         new_options = Int[]
    #         for num_so_far in options
    #             for B in 0:7
    #                 tmp, offset = B_tmp_map[(num_to_make + 1) * 8 + B + 1]
    #                 # for (B, (tmp, offset)) in enumerate(ways_to_make)
    #                 num_pot = num_so_far
    #                 num_pot *= 2^3
    #                 num_pot += B
    #                 if (num_pot >> offset) % 8 == tmp
    #                     # to_add = B
    #                     # println("adding " * showbin(to_add))
    #                     # print("such that B = " * showbin(to_add))
    #                     # print(" with tmp " * showbin(tmp))
    #                     # print(" after $offset")
    #                     # println(" makes $num_to_make")
    #                     # println("in number " * showbin(num_pot))
    #                     # num *= 2^3
    #                     # num += to_add
    #                     push!(new_options, num_pot)
    #                 end
    #             end
    #             options = new_options
    #         end
    #         num = minimum(options)
    #         # num = options[1]
    #     end
    # end
    # return num
end

function showbin(num)
    string(num; base=2, pad=3)
end

function main2()
    to_makes::Vector{Int} = reverse(parse.(Int, split("2,4,1,1,7,5,0,3,1,4,4,0,5,5,3,0", ',')))

    res = main(to_makes)
    println(res)
    @btime main($to_makes) seconds = 1

    # Profile.Allocs.clear()
    # Profile.Allocs.@profile sample_rate = 1 main(to_makes)

    # PProf.Allocs.pprof()

    Profile.clear()
    Profile.@profile for _ in 1:10_000_000
        main(to_makes)
    end

    PProf.pprof()
end

function showmap(B_tmp_map)
    for i in 1:8
        for j in 0:7
            print("$(B_tmp_map[j * 8 + i]) ")
        end
        println()
    end
    println()
end

main2()

# while 1 <= i <= length(instructions)
#     # println((A, B, C))
#     instruction = instructions[i]
#     operand = instructions[i + 1]
#     combo_operand = instructions[i + 1]
#     if combo_operand == 4
#         combo_operand = A
#     elseif combo_operand == 5
#         combo_operand = B
#     elseif combo_operand == 6
#         combo_operand = C
#     elseif combo_operand == 7
#         println("INVALID PROGRAM")
#     end

#     if A < 0 || B < 0 || C < 0
#         println(registers)
#         error()
#     end

#     if instruction == 0
#         num = A
#         denom = 2^combo_operand
#         res = num ÷ denom
#         A = res
#     elseif instruction == 1
#         # println("B = $B xor $operand")
#         B = B ⊻ operand
#     elseif instruction == 2
#         B = combo_operand % 8
#     elseif instruction == 3
#         if A != 0
#             i = operand + 1
#             continue
#         end
#     elseif instruction == 4
#         # println("B = $B xor $C")
#         B = B ⊻ C
#     elseif instruction == 5
#         print("$(combo_operand % 8),")
#     elseif instruction == 6
#         # num = A
#         # denom = 2^combo_operand
#         # res = num ÷ denom
#         # B = res
#     elseif instruction == 7
#         num = A
#         denom = 2^combo_operand
#         res = num ÷ denom
#         C = res
#     end

#     i += 2
# end