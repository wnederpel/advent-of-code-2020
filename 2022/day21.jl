using BenchmarkTools

struct operation_monkey
   name1::String
   name2::String
   operation::String
end

function parse_data()
    names = Dict()
    open(joinpath(dirname(@__FILE__), "data/day21.txt")) do f
        while !eof(f)
            line = readline(f)
            splitted = split(line, " ")
            name = splitted[1][begin:end-1]
            if length(splitted) == 2
                names[name] = parse(Int, splitted[2])
            else
                names[name] = operation_monkey(splitted[2], splitted[4], splitted[3])
            end
        end
    end
    return names
end

function eval_monkey(monkey_name, names, known_values; skip_name="")
#     println("processing monkey $monkey_name")
    if monkey_name == skip_name
        return 0.5
    end
#     println(monkey_name)
    if monkey_name in keys(known_values)
        return known_values[monkey_name]
    end
    # Then the monkey must be an operation monkey
    val1 = eval_monkey(names[monkey_name].name1, names, known_values; skip_name)
    val2 = eval_monkey(names[monkey_name].name2, names, known_values; skip_name)

    if (val1 == 0.5 || val2 == 0.5)
        return 0.5
    end

    operation = names[monkey_name].operation
    if operation == "+"
        my_val = val1 + val2
    elseif operation == "-"
        my_val = val1 - val2
    elseif operation == "*"
        my_val = val1 * val2
    elseif operation == "/"
        my_val = div(val1, val2)
    else
        @error("invalid operation $operation")
    end
    known_values[monkey_name] = my_val
    return my_val
end

function reverse_operation(operation)
    if operation == "+"
        return "-"
    elseif operation == "-"
        return "+"
    elseif operation == "*"
        return "/"
    elseif operation == "/"
        return "*"
    else @error("invalid operation $operation") end
end

function fix_node(cur_name, new_names, original_names, known_values, handled_names)
    # Find our where & how the cur_name appears in original names, and add that to new_names.
    if cur_name in keys(known_values) || cur_name == "root"
        return new_names
    end
#     println("$cur_name is not a known value")

    cur_name1 = ""; cur_name2 = ""; operation = ""
    for (name, monkey) in original_names
        if (!(name in keys(known_values)) && name != "humn") || name == "root"
            if monkey.name1 == cur_name
                cur_name1 = name
                cur_name2 = monkey.name2
                # We are in the situation cur_name1 = cur_name <op> cur_name2
                # We want cur_name = name1 <op> name2
                operation = reverse_operation(monkey.operation)
                new_names[cur_name] = operation_monkey(cur_name1, cur_name2, operation)

            elseif monkey.name2 == cur_name
                # We are in the situation cur_name2 = cur_name1 <op> cur_name
                # We want cur_name = cur_name1 <op> cur_name2
                # if the operator is + or *, this is identical to previous case,
                # if the operator is - or /, do not change instead
                cur_name1 = name
                cur_name2 = monkey.name1
                operation = monkey.operation
                if operation == "+" || operation == "*"
                    operation = reverse_operation(operation)
                    new_names[cur_name] = operation_monkey(cur_name1, cur_name2, operation)
                else
                    new_names[cur_name] = operation_monkey(cur_name2, cur_name1, operation)
                end
            end
        end
    end
    if cur_name1 == "" && cur_name2 == "" && operation == ""
        @error("name $cur_name was not found in other operations")
        return
    end
#     println("name $cur_name apears first in $cur_name1 and second in $cur_name2")
#     println("new entry = $(new_names[cur_name])")
    if !(cur_name1 in handled_names)
        push!(handled_names, cur_name1)
        new_names = fix_node(cur_name1, new_names, original_names, known_values, handled_names)
    end
    if !(cur_name2 in handled_names)
        push!(handled_names, cur_name2)
        new_names = fix_node(cur_name2, new_names, original_names, known_values, handled_names)
    end
    return new_names
end

function solve_problem(names)
    # reverse the tree so that humn in at the top
    known_values = Dict()
    for name in keys(names)
        if typeof(names[name]) == Int && name != "humn"
            known_values[name] = names[name]
        end
    end
#     known_values["root"] = 0
    println(known_values)
    # collapses all subtrees not containing humn
    eval_monkey("root", names, known_values; skip_name="humn")
    println(known_values)

    start_name = "humn"
    new_names = copy(names)
    println()
    for (key, value) in names
        println((key, value))
    end
    names = fix_node(start_name, new_names, names, known_values, [])
    names["root"] = 0
    known_values["root"] = 0

#     println(names)
    println()
    println(known_values)
    for (key, value) in known_values
        names[key] = value
    end
    for (key, value) in names
        println((key, value))
    end

#
    humn_val = eval_monkey("humn", names, known_values)
    println()
    for (key, value) in known_values
        names[key] = value
    end
    for (key, value) in names
        println((key, value))
    end
    return humn_val
#     @error("nothing found..")
end

function main()
    names = parse_data()
    sol = solve_problem(names)
    println(sol)
end

main()