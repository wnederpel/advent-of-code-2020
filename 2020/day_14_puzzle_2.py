def transform_dec_to_bits(value):
    value = eval(value)
    bits = [0] * 36
    curr_bit_index = -1
    while value != 0:
        if value % 2 == 1:
            bit = 1
        else:
            bit = 0
        value = value // 2
        bits[curr_bit_index] = bit
        curr_bit_index -= 1
    return bits

def mask_bits(bits, mask):
    masked_bits = []
    for i in range(len(bits)):
        curr_bit = bits[i]
        curr_mask_value = mask[i]
        if curr_mask_value == '0':
            new_bit = curr_bit
        elif curr_mask_value == '1':
            new_bit = 1
        else:
            new_bit = 'X'
        masked_bits.append(new_bit)
    return masked_bits

def transform_bits_to_dec(bits):
    values = [0]
    for index in range(len(bits)):
        bit = bits[-index - 1]
        if bit != 'X':
            for i in range(len(values)):
                values[i] += bit*(2**index)
        else:
            length = len(values)
            for i in range(length):
                values.append(values[i] + (2**index))
    return values

lines = open('data_day_14.txt').read().splitlines()

memory = {}
for line in lines:
    inst, value = line.split(' = ')
    if inst == 'mask':
        current_mask = list(value)
        print(current_mask)
    elif inst.startswith('mem['):
        adress = inst[4:-1]
        print('addres:', adress)
        adress_binary = transform_dec_to_bits(adress)
        print('addres in binary', adress_binary)
        masked_adress_binary = mask_bits(adress_binary, current_mask)
        print('masked adress binary', masked_adress_binary)
        new_adresses = transform_bits_to_dec(masked_adress_binary)
        print('new adresses', new_adresses)
        for adress in new_adresses:
            memory[adress] = value

print(memory)
sum = 0
for adress in memory:
    sum += eval(memory[adress])
print(sum)
