data = open(r"data_day_1_puzzle_1","r")
numbers = []
for number in data.readlines():
    numbers.append(int(number))

numbers = sorted(numbers)

def get_puzzel_sol(numbers):
    for number_small in numbers:
        for index_number_large in range(len(numbers)):
            for number_also_small in numbers:
                number_large = numbers[ - 1 - index_number_large]
                if number_small + number_large + number_also_small == 2020:
                    print("product: ", number_small * number_large * number_also_small)
                    print("sum: ", number_small + number_large + number_also_small)
                    print("small: ", number_small)
                    print("large: ", number_large)
                    print("also small: ", number_also_small)
                    return

get_puzzel_sol(numbers)