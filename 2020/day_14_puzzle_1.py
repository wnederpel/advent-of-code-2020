def transform_dec_to_bits(value):
    value = eval(value)
    bits = [0] * 36
    curr_bit_index = -1
    while value != 0:
        if value % 2 == 1:
            bit = 1
        else:
            bit = 0
        value = value // 2
        bits[curr_bit_index] = bit
        curr_bit_index -= 1
    return bits

def mask_bits(bits, mask):
    masked_bits = []
    for i in range(len(bits)):
        curr_bit = bits[i]
        curr_mask_value = mask[i]
        if curr_mask_value != 'X':
            new_bit = eval(curr_mask_value)
        else:
            new_bit = curr_bit
        masked_bits.append(new_bit)
    return masked_bits

def transform_bits_to_dec(bits):
    value = 0
    for index in range(len(bits)):
        bit = bits[-index - 1]
        value += bit*(2**index)
    return value

lines = open('data_day_14.txt').read().splitlines()

memory = {}
for line in lines:
    inst, value = line.split(' = ')
    if inst == 'mask':
        current_mask = list(value)
    elif inst.startswith('mem['):
        adress = inst[4:-1]
        bits = transform_dec_to_bits(value)
        masked_bits = mask_bits(bits, current_mask)
        new_value = transform_bits_to_dec(masked_bits)
        memory[adress] = new_value

sum = 0
for adress in memory:
    sum += memory[adress]
print(sum)


