using DataStructures
using BenchmarkTools
using PProf
using Profile

function parse_data()
    data = Set{Tuple{Int,SVector{13,Int}}}()
    open(joinpath(dirname(@__FILE__), "data/day07.txt")) do f
        while !eof(f)
            line = readline(f)
            test, rest = split(line, ':')
            test = parse(Int, test)
            rest = parse.(Int, split(strip(rest), ' '))
            while length(rest) < 13
                push!(rest, 0)
            end
            rest = SVector{13,Int}(rest)
            push!(data, (test, rest))
        end
    end
    return data
end

function solve_problem(data)
    sol = 0
    nsol = 0
    bits = Vector{Int}(undef, 13)

    @inbounds for (test, rest) in data
        if check(test, rest, 2, bits)
            sol += test
            nsol += 1
        elseif check(test, rest, 3, bits)
            sol += test
            nsol += 1
        end
    end
    return sol, nsol
end

@inline function check(test, rest, switch, bits)
    is_true = false
    n_operators = count(x -> x != 0, rest) - 1
    fin = ((switch^n_operators) - 1)
    @inbounds for n in 0:fin
        # bits_str = string(n; base=switch, pad=n_operators)
        # println(bits_str)

        x = n
        for i in 1:n_operators
            bits[i] = x % switch
            x ÷= switch
        end

        val = rest[1]
        for i in 2:count(x -> x != 0, rest)
            if val > test
                break
            end
            if bits[i - 1] == 0
                val += rest[i]
            elseif bits[i - 1] == 2
                len = ndigits(rest[i])
                val = val * 10^len + rest[i]
            else
                val *= rest[i]
            end
        end
        if val == test
            return true
        end
    end
    return false
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
    @btime solve_problem($data) seconds = 1

    # Profile.Allocs.clear()
    # Profile.Allocs.@profile sample_rate = 1 solve_problem(data)

    # PProf.Allocs.pprof()

    # Profile.clear()
    # Profile.@profile solve_problem(data)

    # PProf.pprof()
end

main()