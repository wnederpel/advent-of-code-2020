with open('data_day_8.txt') as f:
    lines = f.read().splitlines()

actions = []
values = []
for line in lines:
    action, value = line.split(' ')
    actions.append(action)
    values.append(value)

next_index = 0
indices_checked = []
accu = 0
while True:
    current_index = next_index
    current_action = actions[current_index]
    current_value = values[current_index]
    if current_action == "acc":
        accu += eval(current_value)
        next_index +=1
    elif current_action == "jmp":
        next_index += eval(current_value)
    elif current_action == "nop":
        next_index += 1
    indices_checked.append(current_index)
    if next_index in indices_checked:
        break
print(accu)

