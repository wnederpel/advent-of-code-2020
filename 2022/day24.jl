using BenchmarkTools
using ProfileView

function parse_data()
    data = []
    open(joinpath(dirname(@__FILE__), "data/day24.txt")) do f
    i = 0
        while !eof(f)
            i += 1
            line = readline(f)
            push!(data, [])
            for char in line
                if char == '.'
                    push!(data[i], [])
                else
                    push!(data[i], [char])
                end
            end
        end
    end
    return data
end

function one_time_step(data)
    new_data = deepcopy(data)
    for i in 1:length(data)
        for j in 1:length(data[1])
            new_data[i][j] = []
        end
    end
    for i in 1:length(data)
        for j in 1:length(data[1])
            for char in data[i][j]
                if char == '>'
                    new_j = j + 1
                    if data[i][new_j] == ['#']
                        new_j = 2
                    end
                    push!(new_data[i][new_j], '>')
                elseif char == '<'
                    new_j = j - 1
                    if data[i][new_j] == ['#']
                        new_j = length(data[1]) - 1
                    end
                    push!(new_data[i][new_j], '<')
                elseif char == '^'
                    new_i = i - 1
                    if data[new_i][j] == ['#']
                        new_i = length(data) - 1
                    end
                    push!(new_data[new_i][j], '^')
                elseif char == 'v'
                    new_i = i + 1
                    if data[new_i][j] == ['#']
                        new_i = 2
                    end
                    push!(new_data[new_i][j], 'v')
                elseif char == '#'
                    new_data[i][j] = ['#']
                else
                    @warn("invalid char $char")
                end
            end
        end
    end
    return new_data
end

function get_new_data(time, data, known_times)
    if time == 0
        return data
    end
    if time in keys(known_times)
        return known_times[time]
    end
    new_data = get_new_data(time - 1, data, known_times)
    new_data = one_time_step(new_data)
    known_times[time] = new_data
    return new_data
end

function print_state(data)
    for datum in data
        for dat in datum
            if length(dat) > 1
                 print(length(dat))
            elseif length(dat) == 0
                 print('.')
            else
                 print(dat[1])
            end
        end
        println()
    end
end

function solve_problem(data)
    t = 0
    start = [1, 2, t]
    goal = [length(data), length(data[1]) - 1]
    t = time_to_reach(start, goal, data)
    start = [length(data), length(data[1]) - 1, t]
    goal = [1, 2]
    t = time_to_reach(start, goal, data)
    start = [1, 2, t]
    goal = [length(data), length(data[1]) - 1]
    t = time_to_reach(start, goal, data)

    return t
end

function time_to_reach(start, goal, data)
    que = [start]
    tmp = deepcopy(data)
    known_times = Dict()

    neigh_diff = [[1, 0, 1], [-1, 0, 1], [0, 1, 1], [0, -1, 1], [0, 0, 1]]
    max_iter = 1000
    while !isempty(que)
        u = popfirst!(que)
        if u[3] > max_iter
            @warn("over $max_iter iterations", maxlog=1)
            return
        end
        if u[1:2] == goal
            return u[3]
        end
        # u[3] + 1 to get data at next time_step
        new_data = get_new_data(u[3] + 1, data, known_times)
        for rel_neigh in neigh_diff
            neigh = rel_neigh + u
            if !(neigh in que) && neigh[1] > 0 && neigh[1] <= length(data) && neigh[2] > 0 && neigh[2] <= length(data[1]) && new_data[neigh[1]][neigh[2]] == []
                push!(que, neigh)
            end
        end
    end
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
end

@time main()
