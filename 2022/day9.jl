using BenchmarkTools

function parse_data()
    data = []
    open(joinpath(dirname(@__FILE__), "data/day9.txt")) do f
        while !eof(f)
            line = readline(f)
            push!(data, line)
        end
    end
    return data
end

function move_tail(head_i, head_j, tail_i, tail_j)
    diff1 = head_i - tail_i
    diff2 = head_j - tail_j
    if diff1 ^ 2 + diff2 ^ 2 == 0 || max(abs(diff1), abs(diff2)) <= 1
        return tail_i, tail_j
    elseif diff1 ^ 2 + diff2 ^ 2 == 4
        return tail_i + div(diff1, 2), tail_j + div(diff2, 2)
    else
        if diff2 ^ 2 == 1
            return tail_i + div(diff1, 2), tail_j + diff2
        elseif diff1 ^ 2 == 1
            return tail_i + diff1, tail_j + div(diff2, 2)
        else
            return tail_i + div(diff1, 2), tail_j + div(diff2, 2)
        end
    end
end

function solve_problem(data)
    rope = Vector{Vector{Int}}([])
    for i = 1:10
        push!(rope, [0, 0])
    end
    tail_vis = Set()
    for datum in data
        direction, steps = split(datum, " ")
        steps = parse(Int, steps)
        for _ = 1:steps
            rope[1][1] += (direction == "R") - (direction == "L")
            rope[1][2] += (direction == "U") - (direction == "D")
            for i = 1:9
                rope[i+1][1], rope[i+1][2] = move_tail(rope[i][1], rope[i][2], rope[i+1][1], rope[i+1][2])
            end
            push!(tail_vis, (rope[10][1], rope[10][2]))
        end
    end
    return length(tail_vis)
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
end

main()
# @benchmark main()