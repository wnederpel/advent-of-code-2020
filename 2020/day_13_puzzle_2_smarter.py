from math import *

data = open('data_day_13.txt').read().splitlines()

arrival = eval(data[0])
ids = data[1].split(',')

rem = 0
rem_lst = []
mod_lst = []
for id in ids:
    if id != 'x':
        id = eval(id)
        mod_lst.append(id)
        rem_lst.append(rem)
    rem += 1

mod_lst_sorted = sorted(mod_lst, reverse=True)
rem_lst_sorted =[0] * len(mod_lst_sorted)

print(mod_lst, rem_lst)
print(mod_lst_sorted, rem_lst_sorted)

for index, value in enumerate(mod_lst):
    new_index = mod_lst_sorted.index(value)
    rem_lst_sorted[new_index] = rem_lst[index]

print(mod_lst, rem_lst)
print(mod_lst_sorted, rem_lst_sorted)

# mod_lst = mod_lst_sorted
# rem_lst = rem_lst_sorted

index = 0
start = rem_lst[index]
increase = mod_lst[index]
factor = 1

for index in range(len(mod_lst) - 1):
    check_mod = mod_lst[index + 1]
    check_rem = rem_lst[index + 1]
    while True:
        if (start + increase * factor) % check_mod == check_mod - check_rem:
            print(start + increase * factor)
            print(check_mod)
            print(check_rem)
            print()
            start = start + increase * factor
            increase *= check_mod
            factor = 0
            break
        else:
            factor += 1
print(start)
