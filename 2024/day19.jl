using DataStructures
using BenchmarkTools
using PProf
using Profile
using StaticArrays
using Bumper

function parse_data()
    towels = String[]
    to_make = String[]
    open(joinpath(dirname(@__FILE__), "data/day19.txt")) do f
        while !eof(f)
            line = readline(f)
            if line == ""
                continue
            end
            if isempty(towels)
                towels = split(line, ", ")
            else
                push!(to_make, line)
            end
        end
    end
    return towels, to_make
end

function ways_to_make(part, towels, known_ways, t_lengths)
    if part == ""
        return 1
    end
    if part in keys(known_ways)
        return known_ways[part]
    end
    ways = 0
    partlen = length(part)
    for (towel, t_length) in zip(towels, t_lengths)
        if startswith(part, towel)
            ways += ways_to_make(view(part, (t_length + 1):partlen), towels, known_ways, t_lengths)
        end
    end
    known_ways[part] = ways
    return ways
end

function solve_problem(data)
    # println(data)
    towels, to_make = data
    t_lengths = length.(towels)
    # There are atmost 60 * 400 (maxlen pattern * num patters) = 24000 known ways
    # It should be possible to map these all to an array, to make the whole more efficient all
    known_ways = SwissDict{SubString{String},Int}()
    # known_ways = Base.ImmutableDict{SubString{String},Int}()
    tot = 0
    for pattern in to_make
        tot += ways_to_make(pattern, towels, known_ways, t_lengths)
    end
    # println(length(known_ways))
    return tot
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
    @btime solve_problem($data) seconds = 1

    Profile.Allocs.clear()
    Profile.Allocs.@profile sample_rate = 1 solve_problem(data)

    PProf.Allocs.pprof()

    # Profile.clear()
    # Profile.@profile for _ in 1:10_000_000
    #     main(to_makes)
    # end

    # PProf.pprof()
end

main()