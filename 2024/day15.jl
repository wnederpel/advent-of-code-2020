using DataStructures
using StaticArrays
using Profile
using PProf
using Bumper
using LinearAlgebra

function parse_data()
    data = []
    instructions = ""
    get_instructions = false
    open(joinpath(dirname(@__FILE__), "data/day15.txt")) do f
        while !eof(f)
            line = readline(f)
            if get_instructions
                instructions *= line
                continue
            end
            if line == ""
                get_instructions = true
                continue
            end
            line = collect(line)
            push!(data, line)
        end
    end
    r_row, r_col = 0, 0
    new_data = Vector{Vector{Char}}()
    for row in eachindex(data)
        push!(new_data, Vector{Char}())
        for col in eachindex(data[row])
            if get(data, [row, col]) == '#'
                push!(new_data[row], '#')
                push!(new_data[row], '#')
            elseif get(data, [row, col]) == '.'
                push!(new_data[row], '.')
                push!(new_data[row], '.')
            elseif get(data, [row, col]) == 'O'
                push!(new_data[row], '[')
                push!(new_data[row], ']')
            elseif get(data, [row, col]) == '@'
                push!(new_data[row], '.')
                push!(new_data[row], '.')
                r_row, r_col = row, col * 2 - 1
            end
        end
    end
    return new_data, [r_row, r_col], instructions
end

function get(data, pos)
    return data[pos[1]][pos[2]]
end

function set(data, pos, val)
    data[pos[1]][pos[2]] = val
end

function show(data, pos; instruction=' ')
    println("about to do $instruction")
    for i in eachindex(data)
        for j in eachindex(data[i])
            if [i, j] == pos
                print('@')
            else
                print(get(data, (i, j)))
            end
        end
        println()
    end
    println()
end

function solve_problem(data)
    data, r_pos, instructions = data
    dirs = Dict('<' => [0, -1], '>' => [0, 1], '^' => [-1, 0], 'v' => [1, 0])
    set(data, r_pos, '.')
    front = Stack{Vector{Int}}()
    locs_to_move = Set{Vector{Int}}()

    for instruction in instructions
        next_r_pos = r_pos + dirs[instruction]

        if get(data, next_r_pos) == '#'
            continue
        end
        if get(data, next_r_pos) == '.'
            r_pos = next_r_pos
            continue
        end
        if get(data, next_r_pos) in "[]"
            empty!(locs_to_move)
            og_next = next_r_pos
            if instruction in "<>"
                while get(data, next_r_pos) in "[]"
                    push!(locs_to_move, next_r_pos)
                    next_r_pos = next_r_pos + dirs[instruction]
                end
                # The next must then be a dot
                if get(data, next_r_pos) == '.'
                    data_copy = deepcopy(data)

                    for loc_to_move in locs_to_move
                        set(data, loc_to_move, '.')
                    end

                    for loc_to_move in locs_to_move
                        new_loc = loc_to_move + dirs[instruction]
                        set(data, new_loc, get(data_copy, loc_to_move))
                    end

                    r_pos = og_next
                end

                continue
            else
                # Something similar but with a front of things to check if they can move
                push!(front, og_next)
                if get(data, next_r_pos) == '['
                    # Also push the matching ]
                    push!(front, og_next + dirs['>'])
                elseif get(data, next_r_pos) == ']'
                    # Also push the matching [
                    push!(front, og_next + dirs['<'])
                end
                can_move = true
                while !isempty(front)
                    loc_to_check = pop!(front)
                    push!(locs_to_move, loc_to_check)
                    next = loc_to_check + dirs[instruction]
                    if get(data, next) == '['
                        # Also push the matching ]
                        push!(front, next)
                        push!(front, next + dirs['>'])
                    elseif get(data, next) == ']'
                        # Also push the matching [
                        push!(front, next)
                        push!(front, next + dirs['<'])
                    elseif get(data, next) == '#'
                        empty!(front)
                        can_move = false
                        break
                    end
                    # Then we have 
                end
                if can_move
                    data_copy = deepcopy(data)

                    for loc_to_move in locs_to_move
                        set(data, loc_to_move, '.')
                    end

                    for loc_to_move in locs_to_move
                        new_loc = loc_to_move + dirs[instruction]
                        set(data, new_loc, get(data_copy, loc_to_move))
                    end

                    r_pos = og_next
                end
            end
        end
    end

    sol = 0
    for i in eachindex(data)
        for j in eachindex(data[i])
            if get(data, [i, j]) == '['
                sol += 100 * (i - 1) + (j - 1)
            end
        end
    end
    return sol
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)

    data_save = data

    @btime solve_problem(data) setup = (data = parse_data()) seconds = 1

    # Profile.Allocs.clear()
    # Profile.Allocs.@profile sample_rate = 1 solve_problem(data)

    # PProf.Allocs.pprof()
end

main()