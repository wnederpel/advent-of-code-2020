using BenchmarkTools
using DataStructures
using ProfileView

function parse_data()
    ore_cost = []
    clay_cost = []
    obsidian_cost = []
    geode_cost = []
    open(joinpath(dirname(@__FILE__), "data/day19.txt")) do f
        while !eof(f)
            line = readline(f)
            push!(ore_cost, parse(Int, split(line, " ")[7]))
            push!(clay_cost, parse(Int, split(line, " ")[13]))
            push!(obsidian_cost, (parse(Int, split(line, " ")[19]), parse(Int, split(line, " ")[22])))
            push!(geode_cost, (parse(Int, split(line, " ")[28]), parse(Int, split(line, " ")[31])))
        end
    end
    return ore_cost, clay_cost, obsidian_cost, geode_cost
end

function f(ore_bots, clay_bots, obsidian_bots, ore_stack, clay_stack, obsidian_stack, projected_geodes, time_left, data, id, cache, max_found, max_ore; not_allowed = [false, false, false, false])
    if time_left <= 0
        max_found[1] = max(max_found[1], geode_stack)
        return projected_geodes
    end

    max_possible_geodes = projected_geodes + time_left * (time_left - 1) / 2
    if max_possible_geodes <= max_found[1]
        return 0
    end

    if (ore_bots, clay_bots, ore_stack, clay_stack, obsidian_bots, obsidian_stack) in keys(cache)
        time_left_then, projected_geodes_then = cache[(ore_bots, clay_bots, ore_stack, clay_stack, obsidian_bots, obsidian_stack)]
        if time_left_then >= time_left && projected_geodes_then >= projected_geodes
            return 0
        else
            cache[(ore_bots, clay_bots, ore_stack, clay_stack, obsidian_bots, obsidian_stack)] = time_left, projected_geodes
        end
    end

    # Deficit assumes continuing full production!
    ore_deficit = max_ore * time_left - ore_bots * time_left - ore_stack
    clay_deficit = data[3][id][2] * time_left - clay_bots * time_left - clay_stack
    obsidian_deficit = data[4][id][2] * time_left - obsidian_bots * time_left - obsidian_stack

    new_ore_stack = ore_bots + ore_stack
    new_clay_stack = clay_bots + clay_stack
    new_obsidian_stack = obsidian_bots + obsidian_stack

    max_geodes = 0

    geode_build = false
    if !not_allowed[1] && ore_stack >= data[4][id][1] && obsidian_stack >= data[4][id][2]
        geode_build = true
        new_ore_stack2 = new_ore_stack - data[4][id][1]
        new_obsidian_stack2 = new_obsidian_stack - data[4][id][2]
        max_geodes = max(max_geodes, f(ore_bots, clay_bots, obsidian_bots, new_ore_stack2, new_clay_stack, new_obsidian_stack2, projected_geodes + time_left, time_left - 1, data, id, cache, max_found, max_ore))
    end
    if !not_allowed[2] && ore_stack >= data[3][id][1] && clay_stack >= data[3][id][2] && obsidian_deficit > 0
        new_ore_stack2 = new_ore_stack - data[3][id][1]
        new_clay_stack2 = new_clay_stack - data[3][id][2]
        max_geodes = max(max_geodes, f(ore_bots, clay_bots, obsidian_bots + 1, new_ore_stack2, new_clay_stack2, new_obsidian_stack, projected_geodes, time_left - 1, data, id, cache, max_found, max_ore))
    end
    # Only build clay bot if the number of bots is less than the amount of clay needed for obs bot
    if !not_allowed[3] && ore_stack >= data[2][id] && clay_deficit > 0
        new_ore_stack2 = new_ore_stack - data[2][id]
        max_geodes = max(max_geodes, f(ore_bots, clay_bots + 1, obsidian_bots, new_ore_stack2, new_clay_stack, new_obsidian_stack, projected_geodes, time_left - 1, data, id, cache, max_found, max_ore))
    end
    if !not_allowed[4] && ore_stack >= data[1][id] && ore_deficit > 0
        new_ore_stack2 = new_ore_stack - data[1][id]
        max_geodes = max(max_geodes, f(ore_bots + 1, clay_bots, obsidian_bots, new_ore_stack2, new_clay_stack, new_obsidian_stack, projected_geodes, time_left - 1, data, id, cache, max_found, max_ore))
    end
    # If you can build smth, but build nothing, then you must build smth else before you build it.
    # Optimal solution will always build it in the iteration in which it build nothing
    not_allowed[1] |= ore_stack >= data[4][id][1] && obsidian_stack >= data[4][id][2]
    not_allowed[2] |= ore_stack >= data[3][id][1] && clay_stack >= data[3][id][2]
    not_allowed[3] |= ore_stack >= data[2][id]
    not_allowed[4] |= ore_stack >= data[1][id]

    max_geodes = max(max_geodes, f(ore_bots, clay_bots, obsidian_bots, new_ore_stack, new_clay_stack, new_obsidian_stack, projected_geodes, time_left - 1, data, id, cache, max_found, max_ore; not_allowed))

    cache[(ore_bots, clay_bots, ore_stack, clay_stack, obsidian_bots, obsidian_stack)] = time_left, projected_geodes
    return max_geodes
end

function g(ore_bots, clay_bots, obsidian_bots, ore_stack, clay_stack, obsidian_stack, projected_geodes, time_left, data, id, cache, max_found, max_ore; not_allowed = [false, false, false, false])
    if time_left <= 0
        return 0
    end

    max_possible_geodes = projected_geodes + time_left * (time_left - 1) / 2
    if max_possible_geodes <= max_found[1]
        return 0
    end

    if (ore_bots, clay_bots, ore_stack, clay_stack, obsidian_bots, obsidian_stack) in keys(cache)
        time_left_then, projected_geodes_then = cache[(ore_bots, clay_bots, ore_stack, clay_stack, obsidian_bots, obsidian_stack)]
        if time_left_then >= time_left && projected_geodes_then >= projected_geodes
            return 0
        else
            cache[(ore_bots, clay_bots, ore_stack, clay_stack, obsidian_bots, obsidian_stack)] = time_left, projected_geodes
        end
    end

    # Deficit assumes continuing full production!
    ore_deficit = max_ore * time_left - ore_bots * time_left - ore_stack
    clay_deficit = data[3][id][2] * time_left - clay_bots * time_left - clay_stack
    obsidian_deficit = data[4][id][2] * time_left - obsidian_bots * time_left - obsidian_stack

    new_ore_stack = ore_stack
    new_clay_stack = clay_stack
    new_obsidian_stack = obsidian_stack
#     println(ore_bots, clay_bots, obsidian_bots, geode_bots)
    max_geodes = 0
    # Decide to build a geode robot
    if obsidian_bots > 0
        # Do nothing untill you can afford it
        time_waited = 0
        while !(new_ore_stack >= data[4][id][1] && new_obsidian_stack >= data[4][id][2])
            new_ore_stack += ore_bots
            new_clay_stack += clay_bots
            new_obsidian_stack += obsidian_bots
            time_waited += 1
        end
        # once you can affort it, you spend a cycle paying, getting rescources, and getting the new robot
        new_ore_stack += ore_bots - data[4][id][1]
        new_clay_stack += clay_bots
        new_obsidian_stack += obsidian_bots - data[4][id][2]
        time_waited += 1
        max_geodes = max(max_geodes, g(ore_bots, clay_bots, obsidian_bots, new_ore_stack, new_clay_stack, new_obsidian_stack, projected_geodes + time_left, time_left - time_waited, data, id, cache, max_found, max_ore))
    end
    new_ore_stack = ore_stack
    new_clay_stack = clay_stack
    new_obsidian_stack = obsidian_stack
    # Decide to build an obsidian bot
    if clay_bots > 0 && obsidian_deficit > 0
        # Do nothing untill you can afford it
        time_waited = 0
        while !(new_ore_stack >= data[3][id][1] && new_clay_stack >= data[3][id][2])
            new_ore_stack += ore_bots
            new_clay_stack += clay_bots
            new_obsidian_stack += obsidian_bots
            time_waited += 1
        end
        # once you can affort it, you spend a cycle paying, getting rescources, and getting the new robot
        new_ore_stack += ore_bots - data[3][id][1]
        new_clay_stack += clay_bots - data[3][id][2]
        new_obsidian_stack += obsidian_bots
        time_waited += 1
        max_geodes = max(max_geodes, g(ore_bots, clay_bots, obsidian_bots + 1, new_ore_stack, new_clay_stack, new_obsidian_stack, projected_geodes, time_left - time_waited, data, id, cache, max_found, max_ore))
    end
    new_ore_stack = ore_stack
    new_clay_stack = clay_stack
    new_obsidian_stack = obsidian_stack
    # Decide to build a clay bot
    if clay_deficit > 0
        # Do nothing untill you can afford it
        time_waited = 0
        while !(new_ore_stack >= data[2][id])
            new_ore_stack += ore_bots
            new_clay_stack += clay_bots
            new_obsidian_stack += obsidian_bots
            time_waited += 1
        end
        # once you can affort it, you spend a cycle paying, getting rescources, and getting the new robot
        new_ore_stack += ore_bots - data[2][id]
        new_clay_stack += clay_bots
        new_obsidian_stack += obsidian_bots
        time_waited += 1
        max_geodes = max(max_geodes, g(ore_bots, clay_bots + 1, obsidian_bots, new_ore_stack, new_clay_stack, new_obsidian_stack, projected_geodes, time_left - time_waited, data, id, cache, max_found, max_ore))
    end

    new_ore_stack = ore_stack
    new_clay_stack = clay_stack
    new_obsidian_stack = obsidian_stack
    # Decide to build an ore bot
    if ore_deficit > 0
        # Do nothing untill you can afford it
        time_waited = 0
        while !(new_ore_stack >= data[1][id])
            new_ore_stack += ore_bots
            new_clay_stack += clay_bots
            new_obsidian_stack += obsidian_bots
            time_waited += 1
        end
        # once you can affort it, you spend a cycle paying, getting rescources, and getting the new robot
        new_ore_stack += ore_bots - data[1][id]
        new_clay_stack += clay_bots
        new_obsidian_stack += obsidian_bots
        time_waited += 1
        max_geodes = max(max_geodes, g(ore_bots + 1, clay_bots, obsidian_bots, new_ore_stack, new_clay_stack, new_obsidian_stack, projected_geodes, time_left - time_waited, data, id, cache, max_found, max_ore))
    end

    # Decide to wait untill the time is up
    # Consider this the only valid way to end, all other return should yield 0
    max_geodes = max(max_geodes, geode_stack + geode_bots * time_left)
    cache[(ore_bots, clay_bots, ore_stack, clay_stack, obsidian_bots, obsidian_stack)] = time_left
    max_found[1] = max(max_found[1], max_geodes)
    return projected_geodes
end

function solve_problem(data)
    prod = 1
    for index = 1:length(data[1])
        max_ore = max(data[1][index], data[2][index], data[3][index][1], data[3][index][1])
        cache = Dict()
        start_ore = min(data[1][index], data[2][index])
        sol = f(1, 0, 0, 0, start_ore, 0, 0, 0, 32 - start_ore, data, index, cache, [0], max_ore)
        prod *= sol
    end
    return prod
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
end

# main()
@time main()