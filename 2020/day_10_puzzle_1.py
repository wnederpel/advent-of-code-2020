with open('data_day_10.txt') as f:
    lines = f.read().splitlines()
f.close()
numbers = [0]
for number in lines:
    numbers.append(eval(number))
numbers = sorted(numbers)
numbers.append(numbers[-1] + 3)

one_jolt = 0
three_jolt = 0

for index in range(0, len(numbers) - 1):
    difference = numbers[index + 1] - numbers[index]
    if difference == 1:
        one_jolt += 1
    elif difference == 3:
        three_jolt += 1

print(one_jolt)
print(three_jolt)
print(one_jolt * three_jolt)