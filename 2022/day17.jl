using BenchmarkTools
using DataStructures
using ProfileView

function parse_data()
    open(joinpath(dirname(@__FILE__), "data/day17.txt")) do f
        while !eof(f)
            line = readline(f)
            return line
        end
    end
end

struct rock
    rock_locs::Vector{Vector{Int}}
    side_x::Vector{Int}
    left::Vector{Vector{Int}}
    right::Vector{Vector{Int}}
    bottom::Vector{Vector{Int}}
end

function solve_problem(data)
    hor_line = rock(
        [[0, 0], [1, 0], [2, 0], [3, 0]],
        [0, 3],
        [[-1, 0]],
        [[4, 0]],
        [[0, -1], [1, -1], [2, -1], [3, -1]],
    )
    cross = rock(
        [[0, 0], [0, -1], [1, -1], [-1, -1], [0, -2]],
        [-1, 1],
        [[-1, 0], [-2, -1], [-1, -2]],
        [[1, 0], [2, -1], [1, -2]],
        [[-1, -2], [0, -3], [1, -2]],
    )
    l_shape = rock(
        [[0, 0], [0, -1], [0, -2], [-1, -2], [-2, -2]],
        [-2, 0],
        [[-1, 0], [-1, -1], [-3, -2]],
        [[1, 0], [1, -1], [1, -2]],
        [[-2, -3], [-1, -3], [0, -3]],
    )
    vert_line = rock(
        [[0, 0], [0, -1], [0, -2], [0, -3]],
        [0, 0],
        [[-1, 0], [-1, -1], [-1, -2], [-1, -3]],
        [[1, 0], [1, -1], [1, -2], [1, -3]],
        [[0, -4]],
    )
    square = rock(
        [[0, 0], [1, 0], [0, -1], [1, -1]],
        [0, 1],
        [[-1, 0], [-1, -1]],
        [[2, 0], [2, -1]],
        [[0, -2], [1, -2]],
    )
    objects = [hor_line, cross, l_shape, vert_line, square]
    num_objects = 5
    data_length = length(data)
    println(data_length)
    stacks = Dict()
    stacks[0] = [true, true, true, true, true, true, true]
    maxes = [0, 0, 0, 0, 0, 0, 0]
    iteration = 0
    to_go = 1000000000000
#     2021000
    cache = Dict()
    i = -1
    max_to_add = 0
    while i < 1000000000000 - 1
        i += 1
#     for i = 0:1000000000000
        object = objects[i % num_objects + 1]
        clean_stacks = clean_up_stacks(stacks)
        state = (clean_stacks, object, iteration)
        if state in keys(cache) && i < 10000000
            then_max, then_i = cache[state]
            cur_max = maximum(maxes)
            max_increase = cur_max - then_max
            i_increase = i - then_i
            i_left = 1000000000000 - i - 1
            known_cycles = div(i_left, i_increase)
            max_to_add = known_cycles * max_increase
            i += known_cycles * i_increase - 1
            object = objects[i % num_objects + 1]

        end
        cache[state] = (maximum(maxes), i)


        loc = get_starting_loc(maxes, object)
        loc, iteration = move_3_start(loc, data, iteration, object, data_length)
        while true
            loc = apply_gas(loc, object, data[iteration + 1], stacks)
            iteration += 1
            iteration = iteration % data_length
            can_move = check_can_move(loc, object.bottom, stacks)
            if can_move
                loc -= [0, 1]
            else
                break
            end
        end
        update_stacks(stacks, maxes, loc, object)
    end
    return maximum(maxes) + max_to_add
end

function clean_up_stacks(stacks)
    min_key = minimum(keys(stacks))
    clean_stacks = Dict()
    for key in keys(stacks)
        clean_stacks[key - min_key] = stacks[key]
    end
    return clean_stacks
end

function move_3_start(loc, data, iteration, object, data_length)
#     iteration_plus_three = (iteration + 3) % data_length
    iteration1 = (iteration + 1) % data_length
    iteration2 = (iteration + 2) % data_length
    iteration3 = (iteration + 3) % data_length
    directions = data[iteration + 1] * data[iteration1 + 1] * data[iteration2 + 1]
    if directions == ">>>"
        rights = min(3, 7 - loc[1] - object.side_x[2])
        lefts = 0
    elseif directions == "<<<"
        rights = 0
        lefts = min(3, loc[1] + object.side_x[1] - 1)
    elseif directions == ">><"
        rights = min(2, 7 - loc[1] - object.side_x[2])
        lefts = 1
    elseif directions == "><>"
        rights = min(1, 7 - loc[1] - object.side_x[2])
        lefts = 0
    elseif directions == "<>>"
        rights = 1 + min(1, 7 - loc[1] - object.side_x[2])
        lefts = min(1, loc[1] + object.side_x[1] - 1)
    elseif directions == "><<"
        rights = min(1, 7 - loc[1] - object.side_x[2])
        lefts = 1 + min(1, loc[1] + object.side_x[1] - 1)
    elseif directions == "<><"
        rights = 0
        lefts = min(1, loc[1] + object.side_x[1] - 1)
    elseif directions == "<<>"
        rights = 1
        lefts = 2
    else
        @error("directions = $directions")
    end
    movement = rights - lefts
    return loc + [movement, -3], iteration3
end

function print_stack(stacks)
    println()
    println(".......")
    for i in reverse(0:length(stacks) - 1)
        for item in stacks[i]
            if item
                print("#")
            else
                print(".")
            end
        end
        println()
    end
    println()
end

function update_stacks(stacks, maxes, loc, object)
    # Assume object is stuck at loc
    for rel_pos in object.rock_locs
        pos = rel_pos + loc
        if !(pos[2] in keys(stacks))
            stacks[pos[2]] = [false, false, false, false, false, false, false]
        end
        stack = stacks[pos[2]]
        stack[pos[1]] = true
        if stack == [true, true, true, true, true, true, true]
            cleanup_stacks(stacks, pos[2])
        end
        if pos[2] > maxes[pos[1]]
            maxes[pos[1]] = pos[2]
        end
    end
end

function cleanup_stacks(stacks, full_row_index)
    keyss = keys(stacks)
    min = minimum(keyss)
    for i = min:full_row_index - 1
        delete!(stacks, i)
    end
end

function check_can_move(loc, object_list, stacks)
    for rel_pos in object_list
        if rel_pos[2] + loc[2] in keys(stacks)
            stack = stacks[rel_pos[2]+ loc[2]]
            if stack[rel_pos[1]+ loc[1]]
                return false
            end
        end
    end
    return true
end


function apply_gas(loc, object, direction, stacks)
   if direction == '>'
       if loc[1] + object.side_x[2] < 7 && check_can_move(loc, object.right, stacks)
           return loc + [1, 0]
       end
   elseif direction == '<'
        if loc[1] + object.side_x[1] > 1 && check_can_move(loc, object.left, stacks)
           return loc - [1, 0]
       end
   else
       @error("invalid direction")
   end
   return loc
end

function get_starting_loc(maxes, object)
    starting_x = 3 - object.side_x[1]
    max_thickness = 0
    for rel_loc in object.bottom
        thickness = -rel_loc[2]
        if thickness > max_thickness
            max_thickness = thickness
        end
    end
    starting_y = maximum(maxes) + 3 + max_thickness
    return [starting_x, starting_y]
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
end
main()
@profview main()
@time main()
