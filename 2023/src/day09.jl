using BenchmarkTools
using ShiftedArrays
using Revise
using Profile
using PProf

function parse_data()
    data = []
    open(joinpath(dirname(@__FILE__), "../data/day09.txt")) do f
        while !eof(f)
            line = readline(f)
            push!(data, parse.(Int, split(line, " ")))
        end
    end
    return data
end

function solve_problem(data)
    intzeros = Vector{Int}(undef, 25)
    fill!(intzeros, 0)
    res = 0
    for line in data
        distance_from_original = 0
        sub_line = copy(line)
        len_sub_line = length(line)
        while sub_line != view(intzeros, 1:len_sub_line)
            res += sub_line[1] * (-1)^distance_from_original

            sub_line =
                view(sub_line, 2:len_sub_line) .-
                view(sub_line, 1:(len_sub_line - 1))
            # sub_line = ShiftedArrays.lead(sub_line) .- sub_line

            distance_from_original += 1
            len_sub_line -= 1
        end
    end
    return res
end

function main()
    data = parse_data()
    @time sol = solve_problem(data)
    println(sol)
    Profile.clear()
    @profile (
        for i in 1:500
            solve_problem(data)
        end
    )
    pprof()

    # b = @benchmarkable parse_data()
    # tune!(b)
    # run(b)

    b = @benchmarkable solve_problem($data)
    tune!(b)
    run(b)
end

main()
