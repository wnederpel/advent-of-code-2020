with open('data_day_5') as f:
    lines = f.read().splitlines()

max_id = 0
ids = []
for line in lines:
    row = 0
    col = 0
    for i in range(0,7):
        if line[i] == 'B':
            row += 2**(6 - i)
    for i in range(0,3):
        if line[i + 7] == 'R':
            col += 2**(2-i)
    id = row * 8 + col
    ids.append(id)

ids = sorted(ids)
for i in range(0,813):
    if i not in ids:
        print(i)

