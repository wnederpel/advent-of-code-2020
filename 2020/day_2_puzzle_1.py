with open('data_day_2_puzzle_1') as f:
    mylist = f.read().splitlines()


keys = []
values = []
valid = 0
invalid = 0
for line in mylist:
    key, value = line.split(': ')
    range, item = key.split(' ')
    first_place, second_place = range.split('-')
    first_place = int(first_place) - 1
    second_place = int(second_place) - 1
    value = list(value)
    print("###")
    print(value)
    print(value[first_place], value[second_place])
    print(item)
    if bool(value[first_place] == item) != bool(value[second_place] == item):
        valid = valid + 1
        print('is valid')
    else:
        invalid = invalid + 1
    print("###")


print(valid)
print(invalid)


