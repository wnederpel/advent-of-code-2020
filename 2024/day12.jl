using DataStructures
using BenchmarkTools
using StaticArrays
using Profile
using PProf
using Bumper

function parse_data()
    data = Vector{Vector{Char}}()
    open(joinpath(dirname(@__FILE__), "data/day12.txt")) do f
        while !eof(f)
            line = readline(f)
            line = collect(line)
            push!(data, line)
        end
    end
    return data
end

function solve_problem(data)
    # println(data)
    price = 0
    @inbounds for row in eachindex(data)
        for col in eachindex(data[row])
            char = data[row][col]
            if char != '.'
                explored_tiles = Set{Tuple{Int,Int}}()
                # if !((row, col) in explored_tiles)
                # println("exploring region  $char")
                vol, out_corner, in_corner = 0, 0, 0
                vol, out_corner, in_corner = explore_region(
                    data, row, col, char, vol, out_corner, in_corner, explored_tiles
                )
                for (explored_row, explored_col) in explored_tiles
                    data[explored_row][explored_col] = '.'
                end
                # println("outside corners = $out_corner")
                # println("inside corners = $in_corner")
                corners = out_corner + in_corner
                # add four corners for each hole in the thing, how to find holes? 
                # println("vol = $vol")
                # println("corners = $(corners)")
                price += vol * corners
            end
        end
    end
    return price
end

@inline function explore_region(data, row, col, char, vol, out_corner, in_corner, explored_tiles)
    vol += 1
    push!(explored_tiles, (row, col))
    down, right, up, left = [-1, 0], [0, 1], [1, 0], [0, -1]
    neighs = [down, right, up, left]
    cur_node = [row, col]
    is_filled_neigh = [true, true, true, true]
    for (i, n) in enumerate(neighs)
        n_row, n_col = n + cur_node
        if !(1 <= n_row <= length(data) && 1 <= n_col <= length(data[1]))
            is_filled_neigh[i] = false
            continue
        end

        char_n_node = data[n_row][n_col]
        if char_n_node != char
            is_filled_neigh[i] = false
        elseif !((n_row, n_col) in explored_tiles)
            vol, out_corner, in_corner = explore_region(
                data, n_row, n_col, char, vol, out_corner, in_corner, explored_tiles
            )
        end
    end
    inner_corner_detection = [
        [up, up + right, right],
        [right, right + down, down],
        [down, down + left, left],
        [left, left + up, up],
    ]
    # println("cur_node $cur_node")
    for mask in inner_corner_detection
        normal1, diag, normal2 = mask
        normal1 += cur_node
        diag += cur_node
        normal2 += cur_node
        # println("mask $((normal1, diag, normal2))")
        normal1_is_valid = (1 <= normal1[1] <= length(data) && 1 <= normal1[2] <= length(data[1]))
        normal2_is_valid = (1 <= normal2[1] <= length(data) && 1 <= normal2[2] <= length(data[1]))
        if normal1_is_valid && normal2_is_valid
            # then diag must be valid
            # check if normal1 and normal2 are char and diag isn't, we have an inner corner
            normal1_char = data[normal1[1]][normal1[2]]
            normal2_char = data[normal2[1]][normal2[2]]
            diag_char = data[diag[1]][diag[2]]
            # println("mask with chars $((normal1_char, diag_char, normal2_char))")
            if char == normal1_char && char == normal2_char && diag_char != char
                # println("adding an in corner! at node $row, $col")
                in_corner += 1
            end
        end
    end
    for i in eachindex(is_filled_neigh)
        next = i < 4 ? i + 1 : 1
        if !is_filled_neigh[i] && !is_filled_neigh[next]
            out_corner += 1
            # println("adding an outside corner at node $row, $col")
        end
    end

    return vol, out_corner, in_corner
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
    @btime solve_problem($data) seconds = 1

    # Profile.Allocs.clear()
    # Profile.Allocs.@profile sample_rate = 1 solve_problem(data)

    # PProf.Allocs.pprof()

    # Profile.clear()
    # Profile.@profile for _ in 1:100000
    #     solve_problem(data)
    # end

    # PProf.pprof()
end

main()