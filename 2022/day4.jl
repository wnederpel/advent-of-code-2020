function parse_data()
    data = []
    open(joinpath(dirname(@__FILE__), "data/day4.txt")) do f
        while !eof(f)
            line = readline(f)
            push!(data, split(line, ","))
        end
    end
    return data
end

function iscontained(range1, range2)
    min1, max1 = split(range1, "-")
    min2, max2 = split(range2, "-")
    return (parse(Int, min1) >= parse(Int, min2) && parse(Int, max1) <= parse(Int, max2)) ||
            (parse(Int, min2) >= parse(Int, min1) && parse(Int, max2) <= parse(Int, max1))
end

function isoverlap(range1, range2)
    min1, max1 = split(range1, "-")
    min2, max2 = split(range2, "-")
    return  (parse(Int, min1) <= parse(Int, max2) && parse(Int, max1) >= parse(Int, min2)) ||
            (parse(Int, min2) <= parse(Int, max1) && parse(Int, max2) >= parse(Int, min1))
end

function solve_problem(data)
    sol = 0
    for datum = data
#         println(datum)
        range1 = datum[1]
        range2 = datum[2]
        if isoverlap(range1, range2)
            sol += 1
        end
    end
    return sol
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
end

main()