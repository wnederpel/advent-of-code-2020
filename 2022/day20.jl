using BenchmarkTools
using DataStructures

struct CircularArray{T,N} <: AbstractArray{T,N}
    x::AbstractArray{T,N}
end


function Base.size(A::CircularArray{T,N}) where {T,N}
    return Base.size(A.x)
end

function Base.length(A::CircularArray{T,N}) where {T,N}
    return Base.length(A.x)
end


function Base.getindex(A::CircularArray{T,N}, index::Int) where {T,N}
    z_index = index - 1
    while z_index < 0
        z_index += length(A)
    end
    normal_index = (z_index % length(A)) + 1
    return A.x[normal_index]
end

function Base.setindex!(A::CircularArray{T,N}, value, index::Int) where {T,N}
    z_index = index - 1
    while z_index < 0
        z_index += length(A)
    end
    normal_index = (z_index % length(A)) + 1
    A.x[normal_index] = value
end

function parse_data()
    tmp = []
    open(joinpath(dirname(@__FILE__), "data/day20.txt")) do f
        while !eof(f)
            line = readline(f)
            push!(tmp, parse(Int, line) * 811589153)
        end
    end
    test = CircularArray([])
    data = CircularArray(tmp)
    return data
end

function solve_problem(data)
    original = copy(data.x)
    len_data = length(data)
    shuffles = 0
    len_data = length(data)
    indices = []
    for i = 1:len_data
        push!(indices, i)
    end
    indices = CircularArray(indices)
    seen = DefaultDict(0)
    println(data)
    for k = 1:10
        for i = 1:len_data
            to_move = original[i]
    #         println(to_move)
            seen[to_move] += 1
            reverse = false
            if to_move < 0
                reverse = true
            end
            to_move_index = indexin(i, indices.x)[1]
    #         println(data[to_move_index])

    #         to_move_index = findall(x -> x == to_move, data.x)[seen[to_move]]
            shuffled = false
            for j in 1:(abs(to_move) % (len_data - 1))
                if !reverse
                    data[to_move_index + j - 1], data[to_move_index + j] = data[to_move_index + j], data[to_move_index + j - 1]
                    indices[to_move_index + j - 1], indices[to_move_index + j] = indices[to_move_index + j], indices[to_move_index + j - 1]
                else
                    data[to_move_index - j], data[to_move_index - j + 1] = data[to_move_index - j + 1], data[to_move_index - j]
                    indices[to_move_index - j], indices[to_move_index - j + 1] = indices[to_move_index - j + 1], indices[to_move_index - j]
                end
            end
        end
    end
    println(data)
    the_zero = indexin(0, data.x)[1]
    sol = data[the_zero + 1000] + data[the_zero + 2000] + data[the_zero + 3000]
    println()
    println(data[the_zero + 1000])
    println(data[the_zero + 2000])
    println(data[the_zero + 3000])
    println()
    return sol
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
end

main()