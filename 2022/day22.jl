using BenchmarkTools

block_size = 50

function parse_data()
    data = [[], [], [], [], [], []]
    instructions = ""
    open(joinpath(dirname(@__FILE__), "data/day22.txt")) do f
        line_index = 1
        block_index = 0
        while !eof(f)
            line = readline(f)
            if line != "" && line[1] in [' ', '.', '#']
                nblocks = div(length(line), block_size)
                empty_blocks = 0
                for parti = 1:nblocks
                    part = line[1 + (parti - 1) * block_size: parti * block_size]
                    if part[1] == ' '
                        empty_blocks += 1
                    else
                        push!(data[block_index + parti - empty_blocks], part)
                    end
                end

    #             push!(data, line)

                if line_index % block_size == 0
                    block_index += nblocks - empty_blocks
                end
                line_index += 1
            else
                if line != ""
                    instructions = parse_instructions(line)
                end
            end
        end
    end
#     println(data)
    block_map = Dict()
    block_map[(1, 0)] = 6, 1
    block_map[(1, 1)] = 2, 1
    block_map[(1, 2)] = 3, 2
    block_map[(1, 3)] = 4, 1

    block_map[(2, 0)] = 6, 0
    block_map[(2, 1)] = 5, 3
    block_map[(2, 2)] = 3, 3
    block_map[(2, 3)] = 1, 3

    block_map[(3, 0)] = 1, 0
    block_map[(3, 1)] = 2, 0
    block_map[(3, 2)] = 5, 2
    block_map[(3, 3)] = 4, 2

    block_map[(4, 0)] = 3, 1
    block_map[(4, 1)] = 5, 1
    block_map[(4, 2)] = 6, 2
    block_map[(4, 3)] = 1, 1

    block_map[(5, 0)] = 3, 0
    block_map[(5, 1)] = 2, 3
    block_map[(5, 2)] = 6, 3
    block_map[(5, 3)] = 4, 3

    block_map[(6, 0)] = 4, 0
    block_map[(6, 1)] = 5, 0
    block_map[(6, 2)] = 2, 2
    block_map[(6, 3)] = 1, 2

    dir_trans_map = Dict()
    dir_trans_map[(0, 1)] = (row, col) -> (col, 1)
    dir_trans_map[(1, 1)] = (row, col) -> (row, 1)
    dir_trans_map[(3, 2)] = (row, col) -> (1, row)
    dir_trans_map[(2, 3)] = (row, col) -> (col, block_size)

    dir_trans_map[(3, 1)] = (row, col) -> (block_size - row + 1, 1)
    dir_trans_map[(3, 3)] = (row, col) -> (row, block_size)
    dir_trans_map[(0, 0)] = (row, col) -> (block_size, col)
    dir_trans_map[(1, 3)] = (row, col) -> (block_size - row + 1, block_size)
    dir_trans_map[(1, 0)] = (row, col) -> (block_size, row)
    dir_trans_map[(2, 2)] = (row, col) -> (1, col)
#     block_map[(1, 0)] = 5
#     block_map[(1, 1)] = 1
#     block_map[(1, 2)] = 4
#     block_map[(1, 3)] = 1
#
#     block_map[(2, 0)] = 2
#     block_map[(2, 1)] = 3
#     block_map[(2, 2)] = 2
#     block_map[(2, 3)] = 4
#
#     block_map[(3, 0)] = 3
#     block_map[(3, 1)] = 4
#     block_map[(3, 2)] = 3
#     block_map[(3, 3)] = 2
#
#     block_map[(4, 0)] = 1
#     block_map[(4, 1)] = 2
#     block_map[(4, 2)] = 5
#     block_map[(4, 3)] = 3
#
#     block_map[(5, 0)] = 4
#     block_map[(5, 1)] = 6
#     block_map[(5, 2)] = 1
#     block_map[(5, 3)] = 6
#
#     block_map[(6, 0)] = 6
#     block_map[(6, 1)] = 5
#     block_map[(6, 2)] = 6
#     block_map[(6, 3)] = 5

    return data, instructions, block_map, dir_trans_map
end

function parse_instructions(line)
    ins = []
    line = split(line, "R")
    line = split.(line, "L")
    for veci = 1:length(line)
        for insi = 1:length(line[veci]) - 1
            push!(ins, line[veci][insi])
            push!(ins, "L")
        end
        push!(ins, line[veci][end])
        push!(ins, "R")
    end
    ins = ins[begin:end-1]
    return ins
end
row_col_map = Dict()
row_col_map[1] = [0, block_size]
row_col_map[2] = [0, block_size * 2]
row_col_map[3] = [block_size, block_size]
row_col_map[4] = [block_size * 2, 0]
row_col_map[5] = [block_size * 2, block_size]
row_col_map[6] = [block_size * 3, 0]

function block_coord(loc, dir)

#     row_col_map[1] = [0, block_size * 2]
#     row_col_map[2] = [block_size, 0]
#     row_col_map[3] = [block_size, block_size]
#     row_col_map[4] = [block_size, block_size * 2]
#     row_col_map[5] = [block_size * 2, block_size * 2]
#     row_col_map[6] = [block_size * 2, block_size * 3]

    row, col = row_col_map[loc[1]]
    true_row = row + loc[3]
    true_col = col + loc[2]
    println("col = $true_col")
    println("row = $true_row")
    dir_val = (dir + 3) % 4
    println("dir_val = $dir_val")
    return 4 * true_col + 1000 * true_row + dir_val
end

function get_step(dir)
    step = []
    if dir == 1
        step = [1, 0]
    elseif dir == 2
        step = [0, 1]
    elseif dir == 3
        step = [-1, 0]
    elseif dir == 0
        step = [0, -1]
    else
        @warn("invalid dir $dir")
    end
    return step
end

function move(steps, dir, loc, data, block_map, print_data, dir_trans_map)
    step = get_step(dir)

    for i = 1:steps
        prev_loc = deepcopy(loc)
        old_dir = dir
        step = get_step(dir)
        loc[2:3] = loc[2:3] + step
        if loc[2] > block_size || loc[2] < 1 || loc[3] > block_size || loc[3] < 1
            new_block, new_dir = block_map[(loc[1], dir)]
            println((loc[1], dir))
            loc[3], loc[2] = dir_trans_map[(dir, new_dir)](loc[3], loc[2])
            dir = new_dir
            loc[1] = new_block
            step = get_step(dir)
#             println(dir)
        end

#         println(data[loc[1]][loc[3]][loc[2]])
#         println(loc)
        if data[loc[1]][loc[3]][loc[2]] == '#'
#             println("from dir $(loc[1])")
            loc = deepcopy(prev_loc)
            dir = old_dir
#             println("to dir $(loc[1])")
#             print_data = print_state(print_data, loc, dir)
            break
        else
            print_data = print_state(print_data, loc, dir; print=false)
        end
#         println(loc)
    end
#     println(dir)
    return loc, dir
end

function print_state(print_data, loc, dir; print=true)
    tmp = collect(print_data[loc[1]][loc[3]])
    tmp[loc[2]] = repr(dir)[1]
    print_data[loc[1]][loc[3]] = join(tmp)
    if print
        println(tmp)
        println(loc[1])
        for (i, datum) in enumerate(print_data)
            row, col = row_col_map[i]
            for line in datum
                println(repeat(" ", col), line)
            end
            println()
        end
    end
    return print_data
end

function solve_problem(data, insv, block_map, dir_trans_map)
    # positions is [blocki, xi, yi]
    loc = [1, 1, 1]
    # right = 1, down = 2, left = 3, up = 4 (mod 4)
    dir = 1
    print_data = deepcopy(data)
    print_data = print_state(print_data, loc, dir)

    for ins in insv
#         println(dir)
        if ins == "R"
#             println("R")
            dir += 1
#             if dir >= 4
#                 dir -= 4
#             end
        elseif ins == "L"
#             println("L")
            dir -= 1
            if dir < 0
                dir += 4
            end
        else
#             println(dir)
            dir = dir % 4
            loc, dir = move(parse(Int, ins), dir, loc, data, block_map, print_data, dir_trans_map)
        end
    end
#     println(loc)
    print_data = print_state(print_data, loc, dir)
    return block_coord(loc, dir)
end

function main()
    data, insv, block_map, dir_trans_map = parse_data()
    sol = solve_problem(data, insv, block_map, dir_trans_map)
    println(sol)
end

main()