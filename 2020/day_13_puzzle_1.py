from math import *

data = open('data_day_13.txt').read().splitlines()

arrival = eval(data[0])
ids = data[1].split(',')
wait_time = []
min = inf
min_id = -1
for id in ids:
    if id == 'x':
        wait_time.append('x')
    else:
        id = eval(id)
        curr_wait_time = ceil(arrival / id) * id - arrival
        if curr_wait_time < min:
            min_id = id
            min = curr_wait_time
        wait_time.append(curr_wait_time)

print(min *  min_id)