from math import *
import time
start_time = time.perf_counter()
def get_bezout_ints(big_n, n):
    r_old, r = big_n, n
    s_old, s = 1, 0
    t_old, t = 0, 1

    while r != 0:
        q = floor(r_old / r)
        r_old, r = r, r_old - q * r
        s_old, s = s, s_old - q * s
        t_old, t = t, t_old - q * t

    return s_old, t_old



data = open('data_day_13.txt').read().splitlines()

arrival = eval(data[0])
ids = data[1].split(',')

rem = 0
rem_lst = []
mod_lst = []
for id in ids:
    if id != 'x':
        id = eval(id)
        mod_lst.append(id)
        rem_lst.append(rem)
    rem += 1
prod_mods = 1
for mod in mod_lst:
    prod_mods *= mod
x = 0
for k in range(len(mod_lst)):
    index_out = k
    big_n = 1
    for j in range(len(mod_lst)):
        if j != k:
            big_n *= mod_lst[j]
    big_m, m = get_bezout_ints(big_n, mod_lst[k])
    x += (mod_lst[k] - rem_lst[k]) * big_m * big_n

while True:
    if x > 0:
        break
    x += prod_mods
end_time = time.perf_counter()
print('time_elapsed =', end_time - start_time)
print(x)
