using BenchmarkTools
using DataStructures
using Revise
using Profile
using PProf

function parse_data()
    data = []
    open(joinpath(dirname(@__FILE__), "../data/day12.txt")) do f
        while !eof(f)
            line = readline(f)
            splitted = split(line, " ")
            counts = parse.(Int, split(splitted[2], ","))
            push!(data, (splitted[1], counts))
        end
    end
    return data
end

function is_dot(
    new_string, line, new_counts, counts, i_line, i_counts, helper_counts
)
    if i_line - 1 > 0 && new_string[i_line - 1] == '#'
        i_counts += 1
        if i_counts > length(new_counts)
            return 0
        end
    end

    new_string *= '.'
    i_line += 1
    return build_new_string(
        new_string,
        line,
        copy(new_counts),
        counts,
        i_line,
        i_counts,
        helper_counts,
    )
end

function is_hash(
    new_string, line, new_counts, counts, i_line, i_counts, helper_counts
)
    new_string *= '#'
    i_line += 1
    if i_counts == 0
        i_counts = 1
    end
    new_counts[i_counts] += 1
    return build_new_string(
        new_string,
        line,
        copy(new_counts),
        counts,
        i_line,
        i_counts,
        helper_counts,
    )
end

function normal_nolonger_possible(
    new_string, line, new_counts, counts, i_line, i_counts, helper_counts
)
    # When the already fixed counts are not equal 
    # When any of the current counts is higher then the known counts
    return counts[1:(i_counts - 1)] != new_counts[1:(i_counts - 1)] ||
           any(new_counts .> counts)
end

function no_longer_possible(
    new_string, line, new_counts, counts, i_line, i_counts, helper_counts
)
    # We can also check for the number of hashes
    total_true = sum(counts)
    my_min_total = 0
    for i in 1:(i_line - 1)
        if new_string[i] == '#'
            my_min_total += 1
        end
    end
    for i in i_line:length(line)
        if line[i] == '#'
            my_min_total += 1
        end
    end
    if my_min_total > total_true
        return true
    end

    # We can also take into account the minimal future length
    # This is equal to remaining in current i_counts + every in upcomming counts, + 1 for every jump
    if i_counts > 0
        min_future_length = counts[i_counts] - new_counts[i_counts]
        if i_counts !== nothing
            for k in (i_counts + 1):length(counts)
                min_future_length += counts[k] + 1
            end
        end
        if min_future_length + length(new_string) > length(line)
            return true
        end
    end

    return normal_nolonger_possible(
        new_string, line, new_counts, counts, i_line, i_counts, helper_counts
    )
end

function build_new_string(
    new_string, line, new_counts, counts, i_line, i_counts, helper_counts
)
    # display(new_string)
    # counts |> display
    # new_counts |> display
    # i_counts |> display
    # Currently, the above string does not work
    if no_longer_possible(
        new_string, line, new_counts, counts, i_line, i_counts, helper_counts
    )
        # This is already filled in and wrong, so return
        return 0
    end
    if counts == new_counts
        # "is valid" |> display
        return 1
    end
    if i_line > length(line)
        return 0
    end

    if line[i_line] == '#'
        return is_hash(
            new_string,
            line,
            copy(new_counts),
            counts,
            i_line,
            i_counts,
            helper_counts,
        )
    elseif line[i_line] == '?'
        res = 0
        # if current count is full, only add dot if full and prev was a #.
        if i_counts > 0 &&
            i_line > 1 &&
            new_counts[i_counts] == counts[i_counts] &&
            new_string[i_line - 1] == '#'
            return is_dot(
                new_string,
                line,
                copy(new_counts),
                counts,
                i_line,
                i_counts,
                helper_counts,
            )
        end
        res += is_hash(
            new_string,
            line,
            copy(new_counts),
            counts,
            i_line,
            i_counts,
            helper_counts,
        )
        res += is_dot(
            new_string,
            line,
            copy(new_counts),
            counts,
            i_line,
            i_counts,
            helper_counts,
        )

        return res
    elseif line[i_line] == '.'
        return is_dot(
            new_string,
            line,
            copy(new_counts),
            counts,
            i_line,
            i_counts,
            helper_counts,
        )
    end
end

function solve_problem(data, final)
    res = 0
    for (index, entry) in enumerate(data)
        begin_line, begin_counts = entry

        options = Int.(zeros(5))
        for repeats in 1:final
            line = ((begin_line * '#')^repeats)[begin:(end - 1)]
            counts = repeat(begin_counts, repeats)

            helper_counts = []
            prev_dot = 1
            i = 1
            for (j, char) in enumerate(line)
                if char == '#' && length(helper_counts) < i
                    push!(helper_counts, [prev_dot, 1])
                elseif char == '#' && length(helper_counts) == i
                    helper_counts[i][2] += 1
                end
                if char == '.' && length(helper_counts) == i
                    i += 1
                    prev_dot = j
                end
            end

            new_counts = Int.(zeros(length(counts)))
            new_string = ""
            i_line = 1
            i_counts = 0
            options[repeats] = build_new_string(
                new_string,
                line,
                new_counts,
                counts,
                i_line,
                i_counts,
                helper_counts,
            )
            # We can do some optimizations, but counting to 100 million just takes time.
            # Not feasible 
            # Must do caching somehow. 
            # Will think about this. 
            options |> display
        end
        res += options[5]
        res += options[4] * options[1] * 1
        res += options[3] * options[1]^2 * 3
        res += options[3] * options[2] * 3
        res += options[2]^2 * options[1] * 3
        res += options[2] * options[1]^3 * 4
        res += options[1]^5
    end
    return res
end

function main()
    data = parse_data()
    @time sol = solve_problem(data, 1)
    @time sol = solve_problem(data, 4)
    println(sol)

    # Profile.clear()
    # @profile (
    #     for i = 1:10
    #         solve_problem(data, 3)
    #     end
    # )
    # pprof()

end

main()
