using DataStructures

function parse_data()
    data = []
    open(joinpath(dirname(@__FILE__), "data/day18.txt")) do f
        while !eof(f)
            line = parse.(Int, split(readline(f), ","))
            push!(data, line)
        end
    end
    return data
end

function isvalid(key)
    x, y = key
    return 0 <= x <= end_loc && 0 <= y <= end_loc
end

end_loc = 70
end_flake = 1024
function solve_problem(data)
    grid = DefaultDict{Vector{Int},Bool}((key) -> isvalid(key); passkey=true)
    i = 0
    for byte in data
        i += 1
        grid[byte] = false
        if i == end_flake
            break
        end
    end
    for i in 0:end_loc
        for j in 0:end_loc
            if !grid[[i, j]]
                print('#')
            else
                print('.')
            end
        end
        println()
    end
    println()

    path, can_reach = compute_can_reach(grid)

    i = end_flake
    while i < length(data)
        i += 1
        grid[data[i]] = false
        if data[i] in path
            path, can_reach = compute_can_reach(grid)
            if !can_reach
                for k in 0:end_loc
                    for j in 0:end_loc
                        if [k, j] == data[i]
                            print("X")
                        elseif !grid[[k, j]]
                            print('#')
                        else
                            print('.')
                        end
                    end
                    println()
                end
                println()
                return data[i]
            end
        end
    end
end

function compute_can_reach(grid)
    start = [0, 0]
    fin = [end_loc, end_loc]
    distances = Dict()
    q = PriorityQueue()
    history = Dict()
    distances[start] = 0
    q[start] = 0
    neighs = [[-1, 0], [1, 0], [0, 1], [0, -1]]
    while !isempty(q)
        cur_loc, cur_cost = peek(q)
        delete!(q, cur_loc)

        if cur_loc == fin
            best_path = []
            loc = fin
            push!(best_path, loc)
            while loc != start
                loc = history[loc]
                push!(best_path, loc)
            end

            return best_path, true
        end

        for n in neighs
            n_loc = cur_loc + n
            if grid[n_loc]
                # println(n_loc)
                if !(n_loc in keys(distances))
                    distances[n_loc] = typemax(Int)
                end
                known_costs = distances[n_loc]
                new_cost = cur_cost + 1
                if new_cost < known_costs
                    distances[n_loc] = new_cost
                    q[n_loc] = distances[n_loc]

                    # println("adding $cur_loc as the history of $n_loc")
                    # println("bc the new cost via curloc is $new_cost")
                    # println("which is lower then the old costof $known_costs")

                    history[n_loc] = copy(cur_loc)
                end
            end
        end
    end
    return [], false
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
end

main()