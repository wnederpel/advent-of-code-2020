using BenchmarkTools

function parse_data()
    data = []
    open(joinpath(dirname(@__FILE__), "data/day8.txt")) do f
        while !eof(f)
            line = readline(f)
            line_data = []
            for char in line
                push!(line_data, parse(Int, char))
            end
            push!(data, line_data)
        end
    end
    return data
end

function is_visible(i, j, data)
    value = data[i][j]
    score = 1

    distance = 0
    for j1 = j + 1:length(data[i])
        distance += 1
        if data[i][j1] >= value
            break
        end
    end
    score *= distance

    distance = 0
    for j1 = reverse(1:j - 1)
        distance += 1
        if data[i][j1] >= value
            break
        end
    end
    score *= distance

    distance = 0
    for i1 = i + 1:length(data)
        distance += 1
        if data[i1][j] >= value
            break
        end
    end
    score *= distance

    distance = 0
    for i1 = reverse(1:i - 1)
        distance += 1
        if data[i1][j] >= value
            break
        end
    end
    score *= distance
    return score
end

function solve_problem(data)
    max = 0
    for i = 2:length(data) - 1
        for j = 2:length(data[i]) - 1
            score = is_visible(i, j, data)
            if score >= max
                max = score
            end
        end
    end
    return max
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
end

main()
# @benchmark main()

