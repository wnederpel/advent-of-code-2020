def get_adj_state(row_index, col_index, seats):
    adj = []
    if row_index == 0:
        row_check_list = [0, 1]
    elif row_index == len(seats) - 1:
        row_check_list = [-1, 0]
    else:
        row_check_list = [-1, 0, 1]

    if col_index == 0:
        col_check_list = [0, 1]
    elif col_index == len(seats[row_index]) - 1:
        col_check_list = [-1, 0]
    else:
        col_check_list = [-1, 0, 1]
    # print("check: ", row_index, col_index)
    for row_check in row_check_list:
        for col_check in col_check_list:
            if not (row_check == 0 and col_check == 0):
                # print(row_index + row_check, col_index + col_check)
                adj.append(seats[row_index + row_check][col_index + col_check])
    # print(len(row_check_list) * len(col_check_list))
    # print("end cal")
    return adj


with open('data_day_11.txt') as f:
    seats = f.read().splitlines()
f.close()

stable_situation = False
become_empty = 4
become_occu = 0
iterations = 0
while not stable_situation:
    iterations += 1
    current_seats = seats
    next_seats = ['' for _ in range(len(current_seats))]
    for row in range(0, len(current_seats)):
        for col in range(0, len(current_seats[row])):
            state = current_seats[row][col]
            adj = get_adj_state(row, col, current_seats)
            if state == "L" and "#" not in adj:
                    next_seats[row] += "#"
            elif state == "#" and adj.count("#") >= 4:
                    next_seats[row] += "L"
            else:
                    next_seats[row] += state

    print(next_seats)
    if next_seats == current_seats:
        stable_situation = True
    seats = next_seats

occu_final = 0
dots = 0
empties = 0
for row in range(0, len(next_seats)):
    for col in range(0, len(next_seats)):
        if next_seats[row][col] == "#":
            occu_final += 1
        elif next_seats[row][col] == ".":
            dots += 1
        elif next_seats[row][col] == "L":
            empties += 1

print(occu_final)
print(dots + empties + occu_final)




