numbers = open('data_day_15.txt').read().splitlines()[0].split(',')

said_index = 0
last_seen_dic = {}

while said_index <= 30000000 - 1:
    if said_index % 5000000 == 0:
        print('%%', said_index / 30000000)
    if said_index < len(numbers):
        new_number = numbers[said_index]
        last_seen_dic[new_number] = said_index
    else:
        num_to_check = new_number
        if num_to_check in last_seen_dic:
            new_number = str(said_index - last_seen_dic[num_to_check] - 1)
        else:
            new_number = '0'
        last_seen_dic[num_to_check] = said_index - 1

    # print(said_numbers)
    said_index += 1

print(new_number)

# steps, ns = 30000000, numbers
# last, c = ns[-1], {n: i for i,n in enumerate(ns)}
# for i in range(len(ns), steps):
#     c[last], last = i, i - c.get(last, i)
# print(last)