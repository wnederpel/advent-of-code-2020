function parse_data()
    data = []
    open(joinpath(dirname(@__FILE__), "data/day1.txt")) do f
        while !eof(f)
            line = readline(f)
            if line != ""
                push!(data, parse(Int, line))
            else
                push!(data, -1)
            end
        end
    end
    return data
end

function solve_problem(data)
    cals = []
    cur_cal = 0
    for cal = data
        if cal == -1
            push!(cals, cur_cal)
            cur_cal = 0
        else
            cur_cal += cal
        end
    end
    push!(cals, cur_cal)
    tot = 0
    i1 = argmax(cals)
    println(cals[i1])
    tot += cals[i1]
    splice!(cals, i1)

    i2 = argmax(cals)
    println(cals[i2])
    tot += cals[i2]
    splice!(cals, i2)

    i3 = argmax(cals)
    println(cals[i3])
    tot += cals[i3]
    println(tot)
    println((i1, i2, i3))
end

function main()
    data = parse_data()
    solve_problem(data)
end

main()