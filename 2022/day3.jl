function parse_data()
    data = []
    open(joinpath(dirname(@__FILE__), "data/day3.txt")) do f
        i = 1
        rucksack = []
        while !eof(f)
            str = readline(f)
            push!(rucksack, str)
            if i % 3 == 0
                push!(data, rucksack)
                rucksack = []
            end
            i += 1
        end
    end
    return data
end

function score(char)
    if islowercase(char)
        return Int(char) - Int("a"[1]) + 1
    elseif isuppercase(char)
        return Int(char) - Int("A"[1]) + 27
    else
        @warn "not lower or upper case"
    end
end

function get_overlap(str1, str2, str3)
    overlap = intersect(str1, str2, str3)[1]
    println(overlap)
    return score(overlap)
end

function solve_problem(data)
    tot = 0
    for datum in data
        println(datum)
        tot += get_overlap(datum[1], datum[2], datum[3])
    end
    return tot
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
end

main()