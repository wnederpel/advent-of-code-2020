using BenchmarkTools

function parse_data()
    data = []
    open(joinpath(dirname(@__FILE__), "data/day10.txt")) do f
        while !eof(f)
            line = readline(f)
            push!(data, line)
        end
    end
    return data
end

function solve_problem1(data)
    sol = 0
    tt_checked = false
    st_checked = false
    hd_checked = false
    hdft_checked = false
    hdet_checked = false
    thdtt_checked = false
    reg = 1
    cycle = 0
    for datum in data
        if datum[1] == 'a'
            value = parse(Int, split(datum, " ")[2])
            cycle += 2
        else
            value = 0
            cycle += 1
        end

        if cycle == 20 || cycle == 21
            if !tt_checked
                sol += reg * 20
#                 println(reg * 20)
                tt_checked = true
            end
        end
        if cycle == 60 || cycle == 61
            if !st_checked
                sol += reg * 60
#                 println(reg * 60)
                st_checked = true
            end
        end
        if cycle == 100 || cycle == 101
            if !hd_checked
                sol += reg * 100
#                 println(reg * 100)
                hd_checked = true
            end
        end
        if cycle == 140 || cycle == 141
            if !hdft_checked
                sol += reg * 140
#                 println(reg * 140)
                hdft_checked = true
            end
        end
        if cycle == 180 || cycle == 181
            if !hdet_checked
                sol += reg * 180
#                 println(reg * 180)
                hdet_checked = true
            end
        end
        if cycle == 220 || cycle == 221
            if !thdtt_checked
                sol += reg * 220
#                 println(reg * 220)
                thdtt_checked = true
            end
        end
        reg += value
    end
    return sol
end

function handlecycle(cycle, reg)
    if cycle == reg || cycle == reg - 1 || cycle == reg + 1
        return '#'
    else
        return '.'
    end
end

function solve_problem(data)
    screen = Vector([])
    for i = 1:240
        push!(screen, '.')
    end
    reg = 1
    cycle = 1
    for datum in data
        if datum[1] == 'a'
            value = parse(Int, split(datum, " ")[2])
            char = handlecycle((cycle - 1) % 40, reg)
            screen[cycle] = char
            println("reg = $reg")
            println("cycle = $cycle")
            println(screen[1:40])
            cycle += 1
            char = handlecycle((cycle - 1) % 40, reg)
            screen[cycle] = char
            println("reg = $reg")
            println("cycle = $cycle")
            println(screen[1:40])
            cycle += 1
        else
            value = 0
            char = handlecycle((cycle - 1) % 40, reg)
            screen[cycle] = char
            println("reg = $reg")
            println("cycle = $cycle")
            println(screen[1:40])
            cycle += 1
        end
        reg += value
#         if cycle == 21
#             break
#         end
    end
    return screen
end


function main()
    data = parse_data()
    screen = solve_problem(data)
    println(screen[1:40])
    println(screen[41:80])
    println(screen[81:120])
    println(screen[121:160])
    println(screen[161:200])
    println(screen[201:240])

end

main()