using BenchmarkTools
using DataStructures

function parse_data()
    workflows = Dict()
    parts = []
    do_parts = false
    open(joinpath(dirname(@__FILE__), "../data/day19.txt")) do f
        while !eof(f)
            line = readline(f)
            if line == ""
                do_parts = true
            elseif !do_parts
                line = line[begin:end-1]
                name, flow = split(line, "{")
                steps = split(flow, ",")
                steps = split.(steps, ":")
                workflows[name] = steps
            else
                line = line[begin+1:end-1]
                line = split(line, ",")
                line = split.(line, "=")
                part = Dict()
                for (key, value) in line
                    part[key] = parse(Int, value)
                end
                push!(parts, part)
            end
        end
    end
    return workflows, parts
end

function solve_problem(data)
    workflows, _ = data
    rejected = 0
    accepted = 0
    sol = 0

    # a state is a list of 4 ranges, its work flow, and the step in its work flow
    queue = Queue{Any}()
    enqueue!(queue, (Dict("x" => 1:4000, "m" => 1:4000, "a" => 1:4000, "s" => 1:4000), "in", 1))

    while !isempty(queue)
        cur_state = dequeue!(queue)
        part, workflow_name, workflow_pos = cur_state
        # Handle the current state
        if workflow_name == "R"
            rejected += 1
            continue
        elseif workflow_name == "A"
            accepted += 1
            to_add = 1
            for value in values(part)
                to_add *= length(value)
            end
            sol += to_add
            continue
        end
        workflow = workflows[workflow_name]
        instruction = workflow[workflow_pos]
        # instruction |> display
        if length(instruction) == 1
            new_workflow_name = instruction[1]
            enqueue!(queue, (part, new_workflow_name, 1))
        else
            check, potential_name = instruction
            check_variable = string(check[1])
            check_operator = check[2]
            check_value = parse(Int, check[3:end])

            continue_part = Dict()
            passes_part = Dict()
            for k in keys(part)
                # make voor elke key range die door gaat en een range die wordt ge teleporteert. 
                if k == check_variable
                    if check_operator == '>'
                        passes_part[k] = filter(x -> x > check_value, part[k])
                        continue_part[k] = filter(x -> x <= check_value, part[k])
                    else
                        passes_part[k] = filter(x -> x < check_value, part[k])
                        continue_part[k] = filter(x -> x >= check_value, part[k])
                    end
                else
                    # The check is on a different var, everything passes the check & continues
                    continue_part[k] = part[k]
                    passes_part[k] = part[k]
                end
            end
            enqueue!(queue, (passes_part, potential_name, 1))
            enqueue!(queue, (continue_part, workflow_name, workflow_pos + 1))
        end
    end
    return rejected, accepted, sol
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
end

main()
