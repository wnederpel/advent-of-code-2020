using DataStructures

function parse_data()
    compare = DefaultDict(() -> deepcopy([]))
    updates = []
    open(joinpath(dirname(@__FILE__), "data/day05.txt")) do f
        updates_active = false
        while !eof(f)
            line = readline(f)
            if line == ""
                updates_active = true
                continue
            end
            if updates_active
                push!(updates, parse.(Int, split(line, ",")))
            else
                before, after = parse.(Int, split(line, "|"))
                push!(compare[before], after)
            end
        end
    end
    return compare, updates
end

function solve_problem(data)
    compare, updates = data
    # println(compare)
    # println()
    # println(updates)
    sol = 0
    for update in updates
        # println(update)
        should_count = false
        while true
            correct = true
            for i in 1:length(update)
                for j in (i + 1):length(update)
                    el1 = update[i]
                    el2 = update[j]
                    # Check if el1 can come before el2
                    if !(el2 in compare[el1])
                        # println("not correct bc")
                        # println("$el2 not in $(compare[el1])")
                        # println()
                        update[j], update[i] = update[i], update[j]
                        correct = false
                        should_count = true
                        break
                    end
                end
                if !correct
                    break
                end
            end
            if correct
                break
            end
        end
        if should_count
            sol += update[Int((length(update) + 1) / 2)]
        end
    end
    return sol
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
    @btime solve_problem($data) seconds = 1
end

main()
