lines = open('data_day_16.txt').read().splitlines()

def adhere_to_any_rules(value, rule_lines):
    for rules in rule_lines:
        for rule in rules:
            if eval(rule.split('-')[0]) <= value <= eval(rule.split('-')[1]):
                return True

def is_valid_value(value, rules):
    return eval(rules[0].split('-')[0]) <= value <= eval(rules[0].split('-')[1]) or eval(rules[1].split('-')[0]) <= value <= eval(rules[1].split('-')[1])



region = 0
rule_lines = []
my_ticket_line = []
other_ticket_lines = []
for line in lines:
    if line == '':
        region += 1
    elif region == 0:
        rule_lines.append(line.split(': ')[1].split(' or '))
    elif region == 1:
        my_ticket_line = line.split(',')
    else:
        other_ticket_lines.append(line.split(','))

del other_ticket_lines[0]
valid_tickets = []
for other_ticket in other_ticket_lines:
    ticket_is_valid = True
    for value in other_ticket:
        value = eval(value)
        if not adhere_to_any_rules(value, rule_lines):
            ticket_is_valid = False
            break
    if ticket_is_valid:
        valid_tickets.append(other_ticket)

ticket_fields = list(range(len(rule_lines)))

possibilities = []
for field in ticket_fields:
    possibilities.append(list(range(len(rule_lines))))

for field in ticket_fields:
    for rule_index, rule in enumerate(rule_lines):
        for ticket in valid_tickets:
            if not is_valid_value(eval(ticket[field]), rule):
                possibilities[field][rule_index] = '_'
                break

tmp = [[] for _ in ticket_fields]
for field in ticket_fields:
    for rule_index, rule in enumerate(rule_lines):
        if possibilities[field][rule_index] != '_':
            tmp[field].append(possibilities[field][rule_index])
possibilities = tmp

removed = 0
field_index_is_rule_index = [-1] * len(ticket_fields)
while -1 in field_index_is_rule_index:
    i = 0
    while i < len(ticket_fields) - removed:
        num_possible_rules = len(possibilities[i])
        if num_possible_rules == 1:
            for other_field in range(len(ticket_fields) - removed):
                if other_field != i:
                    if possibilities[other_field] != []:
                        del possibilities[other_field][possibilities[other_field].index(possibilities[i][0])]
            field_index_is_rule_index[i + removed] = possibilities[i][0]
            if -1 not in field_index_is_rule_index:
                break
            possibilities[i] = []
        else:
            i += 1
print(field_index_is_rule_index)
prod = 1
for index in range(6):
    print(index)
    print(field_index_is_rule_index.index(index))
    prod *= eval(my_ticket_line[field_index_is_rule_index.index(index)])
    print()
print(prod)