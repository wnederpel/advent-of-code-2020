using BenchmarkTools
using ProfileView

function parse_data()
    data = []
    open(joinpath(dirname(@__FILE__), "data/day12.txt")) do f
        while !eof(f)
            line = readline(f)
            push!(data, line)
        end
    end
    return data
end

function dijkstra(data, dist, prev, que, source)
    target = [21, 1]
    while !isempty(que)
        min = argmin(i -> dist[que[i][1], que[i][2]], 1:length(que))
        u = popat!(que, min)
# #         For part 1
#         if u[1] == target[1] && u[2] == target[2]
#             seq = []
#             cur = target
#             while !isempty(prev[cur[1], cur[2]]) || cur == source
#                  pushfirst!(seq, cur)
#                  cur = prev[cur[1], cur[2]]
#
#             end
#             for i = seq
#                 println(i)
#             end
#             println(length(seq))
#             println(dist[u[1], u[2]])
#             break
#         end
        for (vi, vj) in [(u[1] +1 , u[2]), (u[1] - 1, u[2]), (u[1], u[2] + 1), (u[1], u[2] - 1)]
            if !(1 <= vi <= length(data)) || !(1 <= vj <= length(data[1]))
                continue
            end
#             println(data[vi][vj] == 'E')
#             println(data[u[1]][u[2]] >= 'y')
#             if (Char(Int(data[u[1]][u[2]]) + 1) >= data[vi][vj] && data[vi][vj] != 'E') || (data[u[1]][u[2]] == 'S' && data[vi][vj] <= 'b') || (data[vi][vj] == 'E' && data[u[1]][u[2]] >= 'y')
            if (Char(Int(data[vi][vj]) + 1) >= data[u[1]][u[2]] && data[u[1]][u[2]] != 'E') || (data[u[1]][u[2]] == 'E' && data[vi][vj] >= 'y')
                dist_u_to_v = 1
            else
                dist_u_to_v = Inf
            end
            alt = dist[u[1], u[2]] + dist_u_to_v
            if alt <= dist[vi, vj]
                dist[vi, vj] = alt
                prev[vi, vj] = [u[1], u[2]]
            end
        end
    end
    return dist
end

function solve_problem(data)
    dist = fill(Inf, (length(data), length(data[1])))
    prev = fill([], (length(data), length(data[1])))
    que = Vector{Vector{Int}}()
    start = (nothing, nothing)
    target = (nothing, nothing)
    for i = 1:length(data)
        for j = 1:length(data[1])
            push!(que, [i, j])
            if data[i][j] == 'S'
                start = (i, j)
            end
            if data[i][j] == 'E'
                target = (i, j)
                dist[i, j] = 0
            end
        end
    end
    dist = dijkstra(data, dist, prev, que, target)
    min = Inf
    for i = 1:length(data)
        for j = 1:length(data[1])
            if data[i][j] == 'a'
                if dist[i, j] < min
                    min = dist[i, j]
                 end
             end
        end
    end
    return min
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
end
main()
@time main()
# @profview main()