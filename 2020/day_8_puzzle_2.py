def get_reachable_indices(actions, values):
    next_index = 0
    reachable_indices = []
    accu = 0
    while True:
        current_index = next_index
        current_action = actions[current_index]
        current_value = values[current_index]
        if current_action == "acc":
            accu += eval(current_value)
            next_index += 1
        elif current_action == "jmp":
            next_index += eval(current_value)
        elif current_action == "nop":
            next_index += 1
        reachable_indices.append(current_index)
        if next_index in reachable_indices or next_index == len(actions):
            break
    return reachable_indices, accu

with open('data_day_8.txt') as f:
    lines = f.read().splitlines()
f.close()

actions = []
values = []
for i, line in enumerate(lines):
    action, value = line.split(' ')
    actions.append(action)
    values.append(value)

reachable_indices, accu = get_reachable_indices(actions, values)
for index in reachable_indices:
    new_actions = [0] * len(actions)
    if actions[index] == "jmp":
        for i in range(0, len(actions)):
            if i == index:
                new_actions[i] = "nop"
            else:
                new_actions[i] = actions[i]
        new_reachable_indices, accu = get_reachable_indices(new_actions, values)
    elif actions[index] == "nop":
        for i in range(0, len(actions)):
            if i == index:
                new_actions[i] = "jmp"
            else:
                new_actions[i] = actions[i]
        new_reachable_indices, accu = get_reachable_indices(new_actions, values)
    else:
        continue
    if len(actions) - 1 in new_reachable_indices:
        print(accu)


