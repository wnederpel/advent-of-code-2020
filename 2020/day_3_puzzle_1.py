with open('data_day_3_puzzle_1') as f:
    lines = f.read().splitlines()

height = len(lines)
width = len(lines[0])
steps_rights = [1, 3, 5, 7, 1]
steps_downs =  [1, 1, 1, 1, 2]
trees = 1
for steps_right, steps_down in zip(steps_rights, steps_downs):
    current_line = 0
    vertical_pos = 0
    tree = 0
    while True:
        if lines[current_line][vertical_pos] == '#':
            tree += 1

        current_line += steps_down
        vertical_pos += steps_right
        vertical_pos = vertical_pos % width

        if current_line >= height:
            break

    trees *= tree
print(trees)