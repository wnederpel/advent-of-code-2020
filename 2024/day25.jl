using DataStructures

function parse_data()
    locks = Vector{Int}[]
    keys = Vector{Int}[]
    start_block = true
    block = String[]
    open(joinpath(dirname(@__FILE__), "data/day25.txt")) do f
        while !eof(f)
            line = readline(f)
            if line == ""
                start_block = true
                continue
            end
            if start_block
                if !isempty(block)
                    key, info = parse_block(block)
                    if key
                        push!(keys, info)
                    else
                        push!(locks, info)
                    end
                end
                block = String[]
                start_block = false
            end
            push!(block, line)
        end
    end
    key, info = parse_block(block)
    if key
        push!(keys, info)
    else
        push!(locks, info)
    end
    return keys, locks
end

function parse_block(block)
    info = zeros(Int, length(block[begin])) .- 1
    for line in block
        for i in eachindex(info)
            if line[i] == '#'
                info[i] += 1
            end
        end
    end
    if block[begin][begin] == '#'
        key = false
    else
        key = true
    end
    return key, info
end

function solve_problem(data)
    keys, locks = data
    # println(keys)
    # println(locks)
    tot = 0
    for key in keys
        for lock in locks
            if all(lock .+ key .<= 5)
                tot += 1
            end
        end
    end
    return tot
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
end

main()