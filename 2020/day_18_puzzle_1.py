sums = open('data_day_18.txt').read().splitlines()

def evaluate_expression(sum):
    while(sum.count('(') != 0):
        latest_open_index = -1
        for index, char in enumerate(sum):
            if char == '(':
                latest_open_index = index
            if char == ')':
                new_value = str(evaluate(sum[latest_open_index + 1:index]))
                # new_value = '1'
                del sum[latest_open_index+1: index + 1]
                sum[latest_open_index] = new_value
                break
    return evaluate(sum)

def add_brackets(sum):
    sum.insert(0, '(')
    sum.append(')')
    print(sum)
    while(sum.count('*') + 1 != sum.count('(')):
        for i, item in enumerate(sum):
            if item == '*' and not sum[i - 1] == ')' and not sum[i + 1] == '(':
                sum.insert(i + 1, '(')
                sum.insert(i, ')')
                break
    return sum

def evaluate(sum):
    print(sum)
    if sum.count('+') != 0 and sum.count('*') != 0:
        sum = add_brackets(sum)
        print(sum)
        return evaluate_expression(sum)
    else:
        for i, pot_start in enumerate(sum):
            if pot_start != ' ' and pot_start != '(':
                result = eval(pot_start)
                start_index = i
                break
        for item in sum[start_index + 1:]:
            if item == ' ':
                continue
            elif item == '(' or item == ')':
                continue
            elif item == '*':
                multiply = True
                add = False
            elif item == '+':
                add = True
                multiply = False
            else:
                if multiply:
                    result *= eval(item)
                if add:
                    result += eval(item)
        return(result)


total_sum = 0
for sum in sums:
    print(sum)
    sum = list(sum)
    evaluate_expression(sum)
    final_value = evaluate_expression(sum)
    # final_value = evaluate(simplified_sum)
    total_sum += final_value
print(total_sum)
