using BenchmarkTools

function parse_data()
    data = []
    open(joinpath(dirname(@__FILE__), "../data/day18.txt")) do f
        while !eof(f)
            line = readline(f)
            push!(data, split(line, " "))
        end
    end
    return data
end

function solve_problem(data)
    x, y = 0, 0
    vol = 0
    boundary = 0
    for line in data
        _, _, hex = line
        len, dir = hex[begin+2:end-2], hex[end-1]

        len = parse(Int, len, base=16)
        dir = parse(Int, dir)
        boundary += len
        if dir == 3
            y += len
        elseif dir == 0
            x += len
        elseif dir == 1
            y -= len
        elseif dir == 2
            x -= len
        end
        if dir == 0
            vol += y * len
        elseif dir == 2
            vol -= y * len
        end
    end
    res = vol + boundary // 2 + 1

    return res
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
end

main()
