from math import *


def calc_angle(x, y):
    if x > 0 and y > 0:
        return atan(abs(y / x))
    elif x < 0 and y > 0:
        return pi - atan(abs(y / x))
    elif x < 0 and y < 0:
        return pi + atan(abs(y / x))
    elif x > 0 and y < 0:
        return - atan(abs(y / x))


with open('data_day_12.txt') as f:
    data = f.read().splitlines()
f.close()

print(calc_angle(-5, 42 ))
print()

way_point_y = 1
way_point_x = 10
curr_angle = atan(way_point_y / way_point_x)
curr_radians = sqrt(way_point_x * way_point_x + way_point_y * way_point_y)
curr_x = 0
curr_y = 0

print(sin(curr_angle))
print(cos(curr_angle))
print(sin(curr_angle) * curr_radians)
print(cos(curr_angle) * curr_radians)

for inst in data:
    action = inst[0]
    value = eval(inst[1:])
    if action == 'N':
        way_point_y += value
        curr_angle = calc_angle(way_point_x, way_point_y)
        curr_radians = sqrt(way_point_x * way_point_x + way_point_y * way_point_y)
    elif action == 'S':
        way_point_y -= value
        curr_angle = calc_angle(way_point_x, way_point_y)
        curr_radians = sqrt(way_point_x * way_point_x + way_point_y * way_point_y)
    elif action == 'E':
        way_point_x += value
        curr_angle = calc_angle(way_point_x, way_point_y)
        curr_radians = sqrt(way_point_x * way_point_x + way_point_y * way_point_y)
    elif action == 'W':
        way_point_x -= value
        curr_angle = calc_angle(way_point_x, way_point_y)
        curr_radians = sqrt(way_point_x * way_point_x + way_point_y * way_point_y)
    elif action == 'L':
        curr_angle += value * pi / 180
        way_point_y = sin(curr_angle) * curr_radians
        way_point_x = cos(curr_angle) * curr_radians
    elif action == 'R':
        curr_angle -= value * pi / 180
        way_point_y = sin(curr_angle) * curr_radians
        way_point_x = cos(curr_angle) * curr_radians
    elif action == 'F':
        curr_x += way_point_x * value
        curr_y += way_point_y * value
    print(action, value)
    print("x, y: ", curr_x, curr_y)
    print('way point x, y: ', way_point_x, way_point_y)
    print('way point angle: ', curr_angle)
    print('way point radius:', curr_radians)
print(abs(curr_x) + abs(curr_y))