using BenchmarkTools
using DataStructures
using Profile
using PProf

function parse_data()
    data = []
    open(joinpath(dirname(@__FILE__), "../data/day17.txt")) do f
        while !eof(f)
            line = parse.(Int, collect(readline(f)))
            push!(data, line)
        end
    end
    return data
end

function get_neighs(node)
    x, y, straights, dir = node
    if straights < 3
        if dir == :up
            return [
                (x - 1, y, straights + 1, :up),
            ]
        elseif dir == :right
            return [
                (x, y + 1, straights + 1, :right),
            ]
        elseif dir == :left
            return [
                (x, y - 1, straights + 1, :left),
            ]
        elseif dir == :down
            return [
                (x + 1, y, straights + 1, :down),
            ] 
        end
    end
    if dir == :up
        return [
            (x - 1, y, straights + 1, :up),
            (x, y + 1, 0, :right),
            (x, y - 1, 0, :left),
        ]
    elseif dir == :right
        return [
            (x - 1, y, 0, :up),
            (x, y + 1, straights + 1, :right),
            (x + 1, y, 0, :down),
        ]
    elseif dir == :left
        return [
            (x - 1, y, 0, :up),
            (x, y - 1, straights + 1, :left),
            (x + 1, y, 0, :down),
        ]
    elseif dir == :down
        return [
            (x, y - 1, 0, :left),
            (x, y + 1, 0, :right),
            (x + 1, y, straights + 1, :down),
        ] 
    end
end

function validate_neighs(neighs, known_heats, data)
    to_del = []
    len_neighs = length(neighs)
    for (i, node) in enumerate(reverse(neighs))
        x, y, straights, _ = node
        i = len_neighs + 1 - i

        if node in keys(known_heats)
            deleteat!(neighs, i)
        elseif straights == 10
            deleteat!(neighs, i)
        elseif !(1 <= x <= length(data) && 1 <= y <= length(data[1]))
            deleteat!(neighs, i)
        end
    end
    return neighs
end

function show(data, histories)
    print_data = []
    for line in data
        line = string.(line)
        push!(print_data, line)
    end
    for node in histories
        x, y, dir = node
        char = "#"
        if dir == :up
            char = "^"
        elseif dir == :down
            char = "v"
        elseif dir == :right
            char = ">"
        elseif dir == :left
            char = "<"
        end
        print_data[x][y] = char
    end
    println()
    for line in print_data
        # line |> display
        println(join(line, ""))
    end
    println()
end

function solve_problem(data)
    # A node is the x, y coordinates + how often you have gone straight + how you entered
    q = PriorityQueue{Tuple{Int, Int, Int, Symbol}, Int}()
    histories = Dict()
    q[(1, 2, 0, :right)] = data[1][2]
    q[(2, 1, 0, :down)] = data[2][1]
    histories[(2, 1, 0, :down)] = [(1, 1, :down), (2, 1, :down)]
    histories[(1, 2, 0, :right)] = [(1, 1, :right), (1, 2, :right)]
    known_heats = Dict()
    hits = 0
    while !isempty(q)
        node, heat = peek(q)
        known_heats[node] = heat
        dequeue!(q)
        x, y, straights, _ = node
        if (x, y) == (length(data), length(data[1])) && straights >= 3
            # show(data, histories[node])
            return heat
        end
        neighs = get_neighs(node)
        neighs = validate_neighs(neighs, known_heats, data)

        for new_node in neighs
            x, y, straights, dir = new_node

            if new_node in keys(q) && known_heats[node] + data[x][y] < q[new_node] 
                # histories[new_node] = push!(deepcopy(histories[node]), (x, y, dir))
                q[new_node] = known_heats[node] + data[x][y]
            elseif !(new_node in keys(q))
                # histories[new_node] = push!(deepcopy(histories[node]), (x, y, dir))
                q[new_node] = known_heats[node] + data[x][y]
            end
        end
    end
    return 
end

function main()
    data = parse_data()
    @time sol = solve_problem(data)
    println(sol)

    Profile.clear()
    @profile (
        for i = 1:100
            solve_problem(data)
        end
    )
    pprof()
end

main()
