using DataStructures

function parse_data()
    data = []
    open(joinpath(dirname(@__FILE__), "data/day22.txt")) do f
        while !eof(f)
            line = parse(Int, readline(f))
            push!(data, line)
        end
    end
    return data
end

function solve_problem(data)
    # println(data)
    ret = 0
    diff_2_nums = Dict{Vector{Int},Int}[]
    for secret in data
        last_4_diffs = Int[]
        diff_2_num = Dict{Vector{Int},Int}()
        price = secret % 10
        for _ in 1:4
            secret = evolve_secret(secret)
            new_price = secret % 10
            diff = new_price - price
            push!(last_4_diffs, diff)
            price = new_price
        end
        for _ in 4:2000
            secret = evolve_secret(secret)
            new_price = secret % 10
            diff = new_price - price
            if !(last_4_diffs in keys(diff_2_num))
                diff_2_num[copy(last_4_diffs)] = price
            end
            last_4_diffs[1:(end - 1)] = last_4_diffs[2:end]
            last_4_diffs[end] = diff
            price = new_price
        end
        push!(diff_2_nums, diff_2_num)
    end
    max_nanas = 0
    max_seq = Int[]
    for a in -9:9, b in -9:9, c in -9:9, d in -9:9
        bananas = 0
        seq_check = [a, b, c, d]
        for diff_2_num in diff_2_nums
            if seq_check in keys(diff_2_num)
                bananas += diff_2_num[seq_check]
            end
        end
        if bananas > max_nanas
            max_nanas = bananas
            max_seq = seq_check
        end
    end
    max_seq = [-2, 1, -1, 3]
    for diff_2_num in diff_2_nums
        if max_seq in keys(diff_2_num)
            diff_2_num[max_seq] |> println
        else
            0 |> println
        end
    end

    return max_nanas
end

function evolve_secret(secret)
    a = secret * 64
    secret = secret ⊻ a
    secret = secret % 16777216

    b = secret ÷ 32
    secret = secret ⊻ b
    secret = secret % 16777216

    c = secret * 2048
    secret = secret ⊻ c
    secret = secret % 16777216
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
end

main()