using DataStructures
using BenchmarkTools
using StaticArrays
using Profile
using PProf
using Bumper

function parse_data()
    data = Vector{Int}()
    open(joinpath(dirname(@__FILE__), "data/day11.txt")) do f
        while !eof(f)
            line = readline(f)
            data = parse.(Int, split(line, " "))
        end
    end
    return data
end

MAX_BLINKS = 75

function compute_new_number_after(stone, blinks_to_go)
    blinks_to_split = 0
    while blinks_to_split < blinks_to_go
        blinks_to_split += 1
        if stone == 0
            stone = 1
        elseif ndigits(stone) % 2 == 0
            half_digits = ndigits(stone) ÷ 2
            left = stone % (10^half_digits)
            right = stone ÷ (10^half_digits)
            return blinks_to_split, left, right
        else
            stone *= 2024
        end
    end
    return -1, -1, -1
end

function becomes_how_many_stones_at_end(stone, blinks_to_go, known_stones_at_end)
    (stone, blinks_to_go) in keys(known_stones_at_end) &&
        return known_stones_at_end[(stone, blinks_to_go)]

    blinks_to_split, left, right = compute_new_number_after(stone, blinks_to_go)

    blinks_to_split == -1 && return 1

    og_blinks_to_go = blinks_to_go
    blinks_to_go -= blinks_to_split

    left_val = becomes_how_many_stones_at_end(left, blinks_to_go, known_stones_at_end)
    right_val = becomes_how_many_stones_at_end(right, blinks_to_go, known_stones_at_end)

    ret = left_val + right_val
    known_stones_at_end[(stone, og_blinks_to_go)] = ret
    return ret
end

function solve_problem(stones)
    known_stones_at_end = Dict{Tuple{Int,Int},Int}()
    tot_stones = 0
    for stone in stones
        tot_stones += becomes_how_many_stones_at_end(stone, MAX_BLINKS, known_stones_at_end)
    end
    return tot_stones
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
    @btime solve_problem($data) seconds = 1

    # Profile.Allocs.clear()
    # Profile.Allocs.@profile sample_rate = 1 solve_problem(data)

    # PProf.Allocs.pprof()

    Profile.clear()
    Profile.@profile solve_problem(data)

    PProf.pprof()
end

main()