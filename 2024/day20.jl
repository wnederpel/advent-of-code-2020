using DataStructures
using BenchmarkTools
using PProf
using Profile
using StaticArrays
using Bumper

function parse_data()
    data = []
    start = (0, 0)
    finish = (0, 0)
    row, col = 0, 0
    open(joinpath(dirname(@__FILE__), "data/day20.txt")) do f
        while !eof(f)
            row += 1
            line = readline(f)
            col = 0
            for char in line
                col += 1
                if char == 'S'
                    start = (row, col)
                elseif char == 'E'
                    finish = (row, col)
                end
            end
            push!(data, line)
        end
    end
    return data, start, finish
end

function get(data, pos)
    if 1 <= pos[1] <= length(data) && 1 <= pos[2] <= length(data)
        return data[pos[1]][pos[2]]
    else
        return '#'
    end
end

function set(data, pos, val)
    data[pos[1]][pos[2]] = val
end

function solve_problem(data)
    data, start, finish = data
    start = start
    normal_length, known_dists = get_normal_length(data, start, finish)
    # gridprint(data, known_dists)

    # println("normal $normal_length")
    counts = count_shortcuts(data, known_dists)
    return counts
end

function get_cut_map(cut_time)
    cut_map = Vector{Tuple{Tuple{Int,Int},Int}}(undef, 841)
    # cut_map = SizedVector{3,Int}[]
    # cut_map = []
    k = 1
    for i in (-cut_time):1:cut_time
        remaining = cut_time - abs(i)
        for j in (-remaining):1:remaining
            used = abs(i) + abs(j)
            cut_map[k] = ((i, j), used)
            k += 1
            # push!(cut_map, Int[i, j, used])
            # push!(cut_map, [i, j, used])
        end
    end
    println(length(cut_map))
    return cut_map
end

@inline @inbounds function count_shortcuts(data, known_dists)
    totcuts = 0
    min_cut = 100
    cut_time = 20
    cut_map = get_cut_map(cut_time)
    println("computing")
    # for i in 1:(length(data))
    #     for j in 1:length(data[1])
    for from in keys(known_dists)
        for (offset, used) in cut_map
            len_cut1 = get_cuts(from, from .+ offset, known_dists, used)
            if len_cut1 >= min_cut
                totcuts += 1
            end
        end
    end
    return totcuts
end

@inline @inbounds function get_cuts(from, to, known_dists, used)
    if to in keys(known_dists)
        return (known_dists[to] - known_dists[from]) - used
    end
    return -1
end

function get_normal_length(data, start, finish)
    pq = PriorityQueue()
    dist = Dict()
    dist[start] = 0
    pq[start] = 0

    neighs = ((-1, 0), (1, 0), (0, -1), (0, 1))
    while !isempty(pq)
        loc = dequeue!(pq)
        d = dist[loc]
        if loc == finish
            return d, dist
        end
        for n in neighs
            nloc = loc .+ n
            if get(data, nloc) != '#'
                if !(nloc in keys(dist)) || dist[nloc] > d + 1
                    dist[nloc] = d + 1
                    pq[nloc] = d + 1
                end
            end
        end
    end
end

function gridprint(data, dists)
    for row in eachindex(data)
        for col in eachindex(data[1])
            if dists[[row, col]] != typemax(Int)
                print(dists[[row, col]] % 10)
            else
                # print(data[row][col])
                print(" ")
            end
        end
        println()
    end
    println()
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
    # @btime solve_problem($data) seconds = 1

    Profile.Allocs.clear()
    Profile.Allocs.@profile sample_rate = 0.0003 solve_problem(data)

    PProf.Allocs.pprof()
end

main()
