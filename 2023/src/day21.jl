using BenchmarkTools
using DataStructures

function parse_data()
    data = []
    start = nothing
    i = 1
    open(joinpath(dirname(@__FILE__), "../data/day21.txt")) do f
        while !eof(f)
            line = readline(f)
            if 'S' in line
                j = findfirst(x -> x == 'S', line)
                start = (i, j)
            end
            push!(data, line)
            i += 1
        end
    end
    return data, start
end

function access(data, x, y, stops)
    if check_is_dead(x, y, length(data), stops)
        # "at stop $stops, saying $(abs(x)), $(abs(y)) is blocked" |> display
        return '#'
    end
    return access_iterative(data, x, y, stops)
end

function access_iterative(data, x, y, stops)
    if 1 <= x <= length(data) && 1 <= y <= length(data[1])
        return data[x][y]
    else
        if 1 > x 
            return access_iterative(data, x + length(data), y, stops)
        elseif x > length(data)
            return access_iterative(data, x - length(data), y, stops)
        elseif 1 > y
            return access_iterative(data, x, y + length(data[1]), stops)
        elseif y > length(data)
            return access_iterative(data, x, y - length(data[1]), stops)
        end
    end
end

function access_old(data, x, y)
    if 1 <= x <= length(data) && 1 <= y <= length(data[1])
        return data[x][y]
    else
        return '#'
    end
end

function check_is_dead(x, y, len, stops)
    # First get the distance to the start
    rx, ry = abs(x - sx), abs(y - sy)
    tot = floor((rx + (len - 1) / 2) / len) + floor((ry + (len - 1) / 2) / len)
    return tot < stops

end

function show_blocks(blocks, data, queue, dist, stops)
    start = -length(data) * blocks
    finish = length(data) * (blocks + 1)
    for x in start:finish
        str = ""
        for y in start:finish
            if (x, y, dist) in queue
                str *= '0'
            else
                str *= access(data, x, y, stops)
            end
        end
        println(str)
    end
end

function solve_problem(data, max_dist)
    data, start = data
    global sx, sy = start
    visited = deepcopy(collect.(data))
    # join.(visited, "") |> display
    queue = Queue{Tuple{Int, Int, Int}}()
    enqueue!(queue, (sx, sy, 0))
    gardens = 0

    # We can also do the easier technique 
    # meaning that we pause at dist == k * length()
    # Check how many ended at even times, and how many ended at uneven times
    # then loop once more for the uneven ones.
    # 10 should give 121 = 4 * (10  + 8 + 6 + 4 + 2) + 1 = 4 * 12 * 2.5 + 1  = 121
    # 11 should give 4 + 12 + 20 + 28 + 36 + 44 = 48 * 3 = 144
    # From 12 it breaks.
    # 12 should give 121 + 4 * 12 = 121 + 48 = 169

    dist_found = -1

    stops = -1
    even_boards = 0
    odd_boards = 0
    odd_total = 0
    once = true
    while !isempty(queue)
        locx, locy, dist = dequeue!(queue)
        # If this loc is in a stopped thing, just remove it from the queue
        if check_is_dead(locx, locy, length(data), stops)
            if stops == 1
                odd_total += 1
            end
            # Avoid stopping at 1 to count uneven things
            # "at stop $stops, removing $((locx, locy)) with dist $dist" |> display
            continue
        end
        
        if dist == max_dist
            # show_blocks(stops + 1, data, enqueue!(deepcopy(queue), (locx, locy, dist)), dist, stops)
            if once
                "Queue length at start of dist $dist" |> display
                gardens = length(queue) + 1
                break
                once = false
            end
            gardens += 1
        elseif dist > max_dist
            continue
        end
        if dist_found != dist 
            if dist % length(data) == 0
                dist_found = dist
                stops += 1
                # "increasing stop to $stops for loc $((locx, locy)) at dist $dist" |> display
                # stop n stops -> 4 * n gardens stopped
                # 0 stops -> 1 garderns stops 
                if stops == 1
                    stopped = 1
                elseif stops > 1
                    stopped = (stops - 1) * 4
                else
                    stopped = 0
                end
                if dist % 2 == 0
                    even_boards += stopped
                else
                    odd_boards += stopped
                end
            end
        end
        if access(data, locx+1,locy, stops) != '#' 
            if !((locx+1,locy, dist+1) in queue)
                enqueue!(queue, (locx+1,locy, dist+1))
            end
        end
        if access(data, locx-1,locy, stops) != '#' 
            if !((locx-1,locy, dist+1) in queue)
                enqueue!(queue, (locx-1,locy, dist+1))
            end
        end
        if access(data, locx,locy+1, stops) != '#' 
            if !((locx,locy+1, dist+1) in queue)
                enqueue!(queue, (locx,locy+1, dist+1))
            end
        end
        if access(data, locx,locy-1, stops) != '#' 
            if !((locx,locy-1, dist+1) in queue)
                enqueue!(queue, (locx,locy-1, dist+1))
            end
        end
    end
    # join.(visited, "") |> display
    # (even_boards, odd_boards) |> display
    # odd_total |> display
    even_total = odd_total + 1
    removed = 0
    if max_dist % 2 == 0
        # The final step is even, so those that ended at odd need to be converted
        removed = even_boards * odd_total + odd_boards * even_total
    else
        removed = even_boards * even_total + odd_boards * odd_total
    end


    return gardens + removed
end

function main()
    data = parse_data()
    @time sol = solve_problem(data, 68)
    @time sol = solve_problem(data, 10000)
    println(sol)
end

main()
