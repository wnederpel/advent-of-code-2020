using BenchmarkTools

function parse_data()
    data = []
    open(joinpath(dirname(@__FILE__), "data/template.txt")) do f
        while !eof(f)
            line = readline(f)
            push!(data, line)
        end
    end
    return data
end

function solve_problem(data)
    println(data)
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
end

main()