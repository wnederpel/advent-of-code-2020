using DataStructures

function parse_data()
    data = DefaultDict(() -> [])

    row = 0
    col = 0
    open(joinpath(dirname(@__FILE__), "data/day08.txt")) do f
        while !eof(f)
            row += 1

            line = readline(f)
            col = 0
            for char in line
                col += 1
                if char != '.'
                    push!(data[char], [row, col])
                end
            end
        end
    end
    return data, row, col
end

function solve_problem(data)
    println(data)
    data, max_row, max_col = data
    interferencelocs = Set()
    for (freq, locs) in data
        println(freq)
        for i in 1:length(locs)
            for j in 1:(i - 1)
                dist = locs[i] - locs[j]
                new_loc1 = locs[j]
                while 1 <= new_loc1[1] <= max_row && 1 <= new_loc1[2] <= max_col
                    push!(interferencelocs, new_loc1)
                    new_loc1 -= dist
                end
                new_loc2 = locs[i]
                while 1 <= new_loc2[1] <= max_row && 1 <= new_loc2[2] <= max_col
                    push!(interferencelocs, new_loc2)
                    new_loc2 += dist
                end
            end
        end
    end
    return length(interferencelocs)
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
end

main()