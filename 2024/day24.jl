using DataStructures

function parse_data()
    known_values = Dict{String,Bool}()
    gates = Set{Tuple{String,String,String,String}}()
    connections = DefaultDict{String,Vector{Tuple{String,String,String,String}}}(() -> [])
    add_known_values = true
    open(joinpath(dirname(@__FILE__), "data/day24.txt")) do f
        while !eof(f)
            line = readline(f)
            if line == ""
                add_known_values = false
                continue
            elseif add_known_values
                name, val = split(line, ": ")
                known_values[name] = (val == "1")
            else
                name1, operation, name2, _, result_name = split(line, " ")
                gate = (name1, name2, operation, result_name)
                push!(gates, gate)
                push!(connections[name1], gate)
                push!(connections[name2], gate)
            end
        end
    end
    return known_values, gates, connections
end
function set_input(numx, numy, len)
    known_values = Dict{String,Bool}()
    for i in 0:44
        xval = (numx & (1 << i)) >> i
        yval = (numy & (1 << i)) >> i
        xstr = "x" * string(i; pad=2)
        ystr = "y" * string(i; pad=2)
        known_values[xstr] = xval
        known_values[ystr] = yval
    end
    return known_values
end

function add_pairs!(pair_set, out0s, out1s)
    for out0 in out0s
        for out1 in out1s
            push!(pair_set, (out0, out1))
        end
    end
end

function get_pairs(out0s, out1s)
    pairs = Set{Tuple{Tuple{String,String,String,String},Tuple{String,String,String,String}}}()
    for out0 in out0s
        for out1 in out1s
            push!(pairs, (out0, out1))
        end
    end
    return pairs
end

function val_to_bitsarray(num)
    return parse.(Int, collect(bitstring(num)[(end - 43):end]))
end

function solve_problem(data)
    known_values, gates, connections = data

    change_pairs = Set{
        Tuple{Tuple{String,String,String,String},Tuple{String,String,String,String}}
    }()

    xval = get_num_from_vars(known_values, "x")
    yval = get_num_from_vars(known_values, "y")
    val, out0s, out1s = compute_sol(known_values, gates)
    add_pairs!(change_pairs, out0s, out1s)

    dist = sum(abs.(val_to_bitsarray(val) .- val_to_bitsarray(xval + yval)))
    println(abs.(val_to_bitsarray(val) .- val_to_bitsarray(xval + yval)))
    println()
    # All gates should follow the following pattern
    # x xor y = a
    # a xor rem = z

    # x and y = b
    # a and rem = c
    # b or c = rem2
    bad_outs = []
    rem = "brj"
    for bit in 1:44
        println(bit)
        x = "x" * string(bit; pad=2)
        x_conn = connections[x]
        # same as conny
        a = filter(g -> g[3] == "XOR", x_conn)[begin][4]
        b = filter(g -> g[3] == "AND", x_conn)[begin][4]
        a_conn = connections[a]
        a_xor = filter(g -> g[3] == "XOR", a_conn)
        a_and = filter(g -> g[3] == "AND", a_conn)
        # both should be with rem
        a_xor = a_xor[begin]
        a_and = a_and[begin]
        if (a_xor[2] != rem && a_xor[1] != rem) || (a_and[2] != rem && a_and[1] != rem)
            println("a $a is bad bc the and or xor is not with $rem")
            push!(bad_outs, a)
            continue
        end
        z = a_xor[4]
        if !startswith(z, "z")
            println("z $z is bad bc it does not start with z")
            push!(bad_outs, z)
        end

        c = a_and[4]

        # b should be or with c
        b_conn = connections[b]
        b_or = filter(g -> g[3] == "OR", b_conn)
        if (b_or[begin][2] != c && b_or[begin][1] != c)
            println("b $b is bad bc the or is with not with $c")
            push!(bad_outs, b)
        end

        rem = b_or[begin][4]
    end
    println(bad_outs)
end
function compute_sol(known_values, gates)
    out1 = Set{Tuple{String,String,String,String}}()
    out0 = Set{Tuple{String,String,String,String}}()
    while true
        found_smth = false
        computed_gates = Set{Tuple{String,String,String,String}}()
        for (input1, input2, operation, result) in gates
            # input1, input2 = input
            # operation, result = output

            if input1 in keys(known_values) && input2 in keys(known_values)
                val1, val2 = known_values[input1], known_values[input2]
                res = -1
                if operation == "XOR"
                    res = val1 ⊻ val2
                elseif operation == "AND"
                    res = val1 && val2
                elseif operation == "OR"
                    res = val1 || val2
                else
                    error("should not happen")
                end
                known_values[result] = res
                found_smth = true
                push!(computed_gates, (input1, input2, operation, result))
                if res == 0
                    push!(out0, (input1, input2, operation, result))
                else
                    push!(out1, (input1, input2, operation, result))
                end
            end
        end
        for computed_gate in computed_gates
            delete!(gates, computed_gate)
        end
        if !found_smth
            ret = get_num_from_vars(known_values, "z")
            return ret, out0, out1
        end
    end
end

function get_num_from_vars(known_values, start)
    ret = 0
    for (name, val) in known_values
        if startswith(name, start)
            name_num = parse(Int, name[2:end])
            ret += val * 2^name_num
        end
    end
    return ret
end
function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
end

main()