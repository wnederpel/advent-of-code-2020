from math import *

with open('data_day_12.txt') as f:
    data = f.read().splitlines()
f.close()

curr_x = 0
curr_y = 0
curr_angle = 0

for inst in data:
    action = inst[0]
    value = eval(inst[1:])
    if action == 'N':
        curr_y += value
    elif action ==  'S':
        curr_y -= value
    elif action == 'E':
        curr_x += value
    elif action == 'W':
        curr_x -= value
    elif action == 'L':
        curr_angle += value
    elif action == 'R':
        curr_angle -= value
    elif action == 'F':
        curr_radians = curr_angle * pi / 180
        curr_x += cos(curr_radians) * value
        curr_y += sin(curr_radians) * value
    print(action, value)
    print("x, y: ", curr_x, curr_y)

print(abs(curr_x) + abs(curr_y))